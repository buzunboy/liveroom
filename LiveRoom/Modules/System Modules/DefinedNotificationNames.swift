//
//  DefinedNotificationNames.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 1.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

extension NSNotification.Name {
    
    static let LDBRMChanged = Notification.Name("LDBRMChanged")
    static let mainTableDisapperButtons = Notification.Name("mainTableDisapperButtons")
    static let mainTableAppearButtons = Notification.Name("mainTableAppearButtons")
    static let mainTableShowEditOptions = Notification.Name("mainTableShowEditOptions")
    static let mainTableHideEditOptions = Notification.Name("mainTableHideEditOptions")
    static let mainTableGetImageColor = Notification.Name("mainTableGetImageColor")
    static let mainTableDisappear = Notification.Name("mainTableDisappear")
    static let newMediaTutorialPageControlChanged = Notification.Name("newMediaTutorialPageControlChanged")
}
