//
//  Utilities.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 29.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

public class Utilities: NSObject {
    
    static let instance = Utilities()
    
    private override init() {
        
    }
    // MARK: - Static Properties
    public static var roomDetailScreenOffsetValue: CGFloat = 100
    public static var shouldStoreInCloud = true
    
    public static func getScreenRatio(for height: CGFloat, compare with: CGFloat) -> CGFloat {
        let ratio = height/with
        if ratio < 0 {
            return 0
        } else if ratio > 1 {
            return 1
        } else {
            return ratio
        }
    }
    
    public static func isValidEmail(_ email: String) -> Bool {
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: email, options: [], range: NSRange(location: 0, length: email.count)) != nil
    }
    
    public static func isValidUsername(_ username: String) -> Bool {
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: username, options: [], range: NSRange(location: 0, length: username.count)) != nil
    }
    
    public static func isValidPassword(_ password: String) -> Bool {
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: password, options: [], range: NSRange(location: 0, length: password.count)) != nil
    }
    
    public static func createNewid() -> String {
        let randomizer = arc4random_uniform(50) + 1
        let dateStr = Int(Date().timeIntervalSince1970)
        let dict = ["id": dateStr*Int(randomizer)]
        let encode = encodeDictionary(dict)
        return encode!
    }
    
    static func encodeDictionary(_ dict: [String:Any]) -> String? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            return jsonData.base64EncodedString()
        } catch {
            return nil
        }
    }
    
    // MARK: - Dynamic Properties
    
    public var deviceHeight: CGFloat {
        get {
            return UIScreen.main.bounds.height
        }
    }
    
    public var deviceWidth: CGFloat {
        get {
            return UIScreen.main.bounds.width
        }
    }

    class PropertyListHandler {
        
        private var listName: String!
        
        init(listName: String) {
            self.listName = listName
        }
        
        func setValue(_ value: Any, for key: String) {
            let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
            let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("\(listName!).plist")
            let fullDestPathString = fullDestPath!.path
            
            let dict = readPropertiesDictionary()
            if dict != nil {
                dict!.setObject(value, forKey: key as NSCopying)
                dict!.write(toFile: fullDestPathString, atomically: false)
            } else {
                LogError("Couldn't read \(self.listName!).plist")
            }
        }
        
        public func reloadConfigurations() -> NSDictionary? {
            return self.loadProperties()
        }
        
        public func removeAll() -> Bool {
            var retVal = false
            let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
            let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("\(listName!).plist")
            let fullDestPathString = fullDestPath!.path
            let dict = NSMutableDictionary()
            dict.write(toFile: fullDestPathString, atomically: false)
//            let dict = readPropertiesDictionary()
//            if dict != nil {
//                dict!.removeAllObjects()
//                dict!.write(toFile: fullDestPathString, atomically: true)
//                retVal = true
//            }
//
            return retVal
        }
        
        private func readPropertiesDictionary() -> NSMutableDictionary? {
            let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
            let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("\(listName!).plist")
            if fullDestPath != nil {
                let fullDestPathString = fullDestPath!.path
                return NSMutableDictionary(contentsOfFile: fullDestPathString)
            } else {
                return nil
            }
        }
        
        public func loadProperties() -> NSDictionary? {
            let path = openProperties()
            if path != nil {
                if let propertyDict = NSDictionary(contentsOfFile: path!) {
                    return propertyDict
                }
            } else {
                LogError("Couldn't load \(listName!).plist file")
            }
            return nil
        }
        
        private func openProperties() -> String? {
            let bundlePath = Bundle.main.path(forResource: listName, ofType: ".plist")
            let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
            let fileManager = FileManager.default
            let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("\(listName!).plist")
            let fullDestPathString = fullDestPath!.path
            if fileManager.fileExists(atPath: fullDestPathString) {
                //NSLog("\(listName!).plist file is already found in device directory")
            } else {
                do {
                    try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
                } catch {
                    LogError("\(listName!).plist couldn't copy to the device directory - Error: \(error.localizedDescription)")
                }
            }
            return fullDestPathString
        }
        
    }
    
}
