//
//  Enumerations.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 28.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import Foundation

enum MediaPickerType: Int {
    case photo = 0
    case video = 1
    case cameraPhoto = 2
    case cameraVideo = 3
}

enum ObjectType: Int {
    case unknown = 0
    case asset = 1
    case room = 2
    case media = 3
    case user = 4
}

enum AssetType: Int {
    case image = 0
    case video = 1
}

public enum LoginStatus: Int {
    case initial = 0
    case loggedOut = 1
    case loggedIn = 2
    case rejected = 3
    case connecting = 4
    case anonym = 5
}

enum ErrorDomains: String {
    case localError = "LocalError"
    case cloudDatabaseError = "cloudDatabaseError"
}

enum ErrorCodes: Int {
    case unspecified = 0
    case taskError = 100
    case jsonError = 101
    case urlConvertionError = 102
}

enum HTTPRequestType: String {
    case post = "POST"
    case get = "GET"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
}

enum imagePickerTag: Int {
    case photoLibrary = 1013
    case videoLibrary = 1014
    case cameraPhoto = 2011
    case cameraVideo = 2012
}

enum UserTypes: Int {
    case anonym = 0
    case standard = 1
    case godMode = 93
}
