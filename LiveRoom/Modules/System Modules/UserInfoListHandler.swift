//
//  UserInfoListHandler.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 2.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

extension Utilities {
    
    class UserInfoListHandler {
        
        static let sharedInstance = UserInfoListHandler()
        
        private var handler: Utilities.PropertyListHandler!
        
        private(set) var id: String?
        private(set) var email: String?
        private(set) var type: UserTypes = .standard
        private(set) var profilePicture: String?
        private(set) var isHomeKitEnabled = true
        
        private init() {
            self.handler = Utilities.PropertyListHandler(listName: "User")
            self.loadProperties()
        }
        
        public func setValue(_ value: Any,for key: String) {
            handler.setValue(value, for: key)
            self.reloadConfigurations()
        }
        
        public func getValue(for key: String) -> Any? {
            if let pDict = handler.loadProperties() {
                return pDict[key]
            } else {
                return nil
            }
        }
        
        public func resetData() -> Bool {
            _ = self.handler.removeAll()
            self.reloadConfigurations()
            return true
        }
        
        public func reloadConfigurations() {
            self.loadProperties()
        }
        
        private func loadProperties() {
            if let propertyDict = handler.loadProperties() {
                if let id = propertyDict["id"] as? String { self.id = id } else { self.id = nil }
                if let email = propertyDict["email"] as? String { self.email = email } else { self.email = nil }
                if let type = UserTypes(rawValue: propertyDict["type"] as? Int ?? 1) { self.type = type }
                if let profilePict = propertyDict["profile_picture"] as? String { self.profilePicture = profilePict } else { self.profilePicture = nil }
                if let homeKit = propertyDict["isHomeKitEnabled"] as? Bool { self.isHomeKitEnabled = homeKit }
            }
        }
    }
    
}
