//
//  NavigationBarController.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 11.08.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class LRNavigationController: UINavigationController, UIGestureRecognizerDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        interactivePopGestureRecognizer?.delegate = self
        interactivePopGestureRecognizer?.delaysTouchesBegan = false
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return viewControllers.count > 1
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return !viewDidAppeared
    }
    
    public var viewDidAppeared = false
}


class NavigationBarController: NSObject {
    
    static let sharedInstance = NavigationBarController()
    public var delegate: UIViewController? {
        didSet {
            if let navController = delegate?.navigationController {
                self.panGesture = UIPanGestureRecognizer(target: self, action: #selector(gestureRecognized(_:)))
                self.panGesture?.delegate = self
                navController.view.addGestureRecognizer(self.panGesture!)
            }
        }
    }
    
    private var backView: UIView?
    private var statusBarView: UIView?
    private var customNavBarView: UIView!
    private var panGesture: UIPanGestureRecognizer?
    private var viewdidAppeared = true
    private var viewdidDisappeared = false
    
    private override init() {
        super.init()
        self.customNavBarView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        self.prepareViews()
    }
    
    private func prepareViews() {
        let window = UIApplication.shared.value(forKey: "statusBarWindow")
        let view: UIView = (window as? NSObject)?.value(forKey: "statusBar") as! UIView
        self.statusBarView = view
        
        self.backView = self.delegate?.navigationController?.navigationBar
        self.customNavBarView.frame = CGRect(x: 0, y: -20, width: self.backView?.frame.width ?? Utilities.instance.deviceWidth, height: (self.backView?.frame.height ?? 44) + 20)
        self.customNavBarView.tag = 9454
        self.customNavBarView.isHidden = true
        self.delegate?.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.delegate?.navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    public func viewWillAppeared(currentPos: CGFloat? = nil) {
        self.customNavBarView.isHidden = true
        self.prepareViews()
        self.viewdidAppeared = false
        if let nav = delegate?.navigationController as? LRNavigationController {
            nav.viewDidAppeared = false
        }
        if let delegate = delegate as? NavigationBarProtocol {
            if self.delegate?.navigationController?.navigationBar.subviews != nil {
                for subview in (self.delegate?.navigationController?.navigationBar.subviews)! {
                    if subview.tag == 9454 {
                        subview.removeFromSuperview()
                    }
                }
            }
//            if delegate.isNavigationBarTransparent {
//                var shouldShow = false
//                if let pos = currentPos {
//                    if pos > delegate.navigationBarAppearanceLevel {
//                        shouldShow = true
//                    }
//                }
//
//                if shouldShow {
//                    UIView.animate(withDuration: 0.2, animations: {
//                        self.customNavBarView.frame.origin.y = -20
//                        self.customNavBarView.isHidden = false
//                    }) { (completed) in
//                        self.customNavBarView.frame.origin.y = -20
//                        self.customNavBarView.isHidden = false
//                    }
//                } else {
//                    self.customNavBarView.isHidden = true
//                }
//            } else {
//                self.customNavBarView.isHidden = false
//            }
            
            self.delegate?.navigationController?.navigationBar.addSubview(self.customNavBarView)
            self.delegate?.navigationController?.navigationBar.sendSubview(toBack: self.customNavBarView)
        }
        
    }
    
    public func viewDidAppeared(currentPos: CGFloat? = nil) {
        self.viewWillAppeared(currentPos: currentPos)
        self.viewdidAppeared = true
        if let nav = delegate?.navigationController as? LRNavigationController {
            nav.viewDidAppeared = true
        }
        if let delegate = delegate as? NavigationBarProtocol {
            
            if delegate.isNavigationBarTransparent {
                var shouldShow = false
                if let pos = currentPos {
                    if pos > delegate.navigationBarAppearanceLevel {
                        shouldShow = true
                    }
                }
                
                if shouldShow {
                    self.customNavBarView.frame.origin.y = -20 - self.customNavBarView.frame.size.height
                    self.customNavBarView.isHidden = false
                    UIView.animate(withDuration: 0.2, animations: {
                        self.customNavBarView.frame.origin.y = -20
                    }) { (completed) in
                    }
                } else {
                    if !self.customNavBarView.isHidden {
                        UIView.animate(withDuration: 0.2, animations: {
                            self.customNavBarView.frame.origin.y = -20 - self.customNavBarView.frame.size.height
                        }) { (completed) in
                            if completed {
                                self.customNavBarView.isHidden = true
                            }
                        }
                    }
                }
            } else {
                self.customNavBarView.isHidden = false
            }
        }
    }
    
    public func viewDidDisappeared(currentPos: CGFloat? = nil) {
    }
    
    public func scrollViewScrolled(offset: CGFloat) {
        if let delegate = delegate as? NavigationBarProtocol {
            self.backView = self.delegate?.navigationController?.navigationBar
            self.customNavBarView.frame = CGRect(x: 0, y: -20, width: self.backView?.frame.width ?? Utilities.instance.deviceWidth, height: (self.backView?.frame.height ?? 44) + 20)
            
            if delegate.isNavigationBarTransparent {
                self.customNavBarView.isHidden = true
            } else {
                self.customNavBarView.isHidden = false
            }
            
            if delegate.isNavigationBarTransparent {
                if offset >= delegate.navigationBarAppearanceLevel {
                    self.customNavBarView.isHidden = false
                    let pos = (offset - delegate.navigationBarAppearanceLevel) - self.customNavBarView.frame.height - 20
                    if pos <= -20 {
                        self.customNavBarView.frame.origin.y = pos
                    } else {
                        self.customNavBarView.frame.origin.y = -20
                    }
                }
            }
        }
    }
    
    static func setStatusBarBackgroundColor(to color: UIColor) {
        let window = UIApplication.shared.value(forKey: "statusBarWindow")
        let view: UIView = (window as? NSObject)?.value(forKey: "statusBar") as! UIView
        if view.responds(to: #selector(setter: UIView.backgroundColor)){
            view.backgroundColor = color
        }
    }
    
    static func setStatusBarBlackTranslucent() {
        let window = UIApplication.shared.value(forKey: "statusBarWindow")
        let view: UIView = (window as? NSObject)?.value(forKey: "statusBar") as! UIView
        
        let visualView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        visualView.frame = view.frame
        visualView.tag = 11230
        if view.responds(to: #selector(setter: UIView.backgroundColor)){
            view.insertSubview(visualView, at: 0)
        }
    }
    
    static func removeStatusBarBlackTranslucent() {
        let window = UIApplication.shared.value(forKey: "statusBarWindow")
        let view: UIView = (window as? NSObject)?.value(forKey: "statusBar") as! UIView
        
        for subview in view.subviews {
            if subview.tag == 11230 {
                subview.removeFromSuperview()
            }
        }
    }
    
    @objc private func gestureRecognized(_ gesture: UIPanGestureRecognizer) {
//        if gesture.state == .ended {
//            self.customNavBarView.frame.origin.x = 0
//            return
//        }
        
        if self.viewdidAppeared {
            return
        }
//
//        let location = gesture.translation(in: delegate?.view)
//        self.customNavBarView.frame.origin.x = location.x - self.customNavBarView.frame.width
//        delegate?.navigationController?.navigationBar.sendSubview(toBack: self.customNavBarView)
    }
    
}

extension NavigationBarController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}
