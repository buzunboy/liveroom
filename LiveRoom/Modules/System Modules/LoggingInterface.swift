//
//  LoggingInterface.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 28.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

func LogInfo(_ format: String, _ args: CVarArg..., file: String = #file, function: String = #function, line: Int = #line, queue: String = OperationQueue.current?.name ?? "") {
    LoggingInterface.sharedInstance.writeLogToFile(file: file, function: function, line: line, queue: queue, type: .Info, format, args)
}

func LogError(_ format: String, _ args: CVarArg..., file: String = #file, function: String = #function, line: Int = #line, queue: String = OperationQueue.current?.name ?? "") {
    LoggingInterface.sharedInstance.writeLogToFile(file: file, function: function, line: line, queue: queue, type: .Error, format, args)
}

func LogWarning(_ format: String, _ args: CVarArg... , file: String = #file, function: String = #function, line: Int = #line, queue: String = OperationQueue.current?.name ?? "") {
    LoggingInterface.sharedInstance.writeLogToFile(file: file, function: function, line: line, queue: queue, type: .Warning, format, args)
}

func LogMemoryWarning(_ format: String, _ args: CVarArg..., file: String = #file, function: String = #function, line: Int = #line, queue: String = OperationQueue.current?.name ?? "") {
    LoggingInterface.sharedInstance.writeLogToFile(file: file, function: function, line: line, queue: queue, type: .MemoryWarning, format, args)
}

func LogVerbose(_ format: String, _ args: CVarArg..., file: String = #file, function: String = #function, line: Int = #line, queue: String = OperationQueue.current?.name ?? "") {
    LoggingInterface.sharedInstance.writeLogToFile(file: file, function: function, line: line, queue: queue, type: .Verbose, format, args)
}

fileprivate enum LogType: Int {
    case Verbose = 0
    case Info = 1
    case Warning = 2
    case MemoryWarning = 3
    case Error = 4
}

public class LoggingInterface {
    
    public static let sharedInstance = LoggingInterface()
    private var handler: FileHandle?
    private var logLevel: LogType = .Verbose
    
    private init() {
        initLogFile()
    }
    
    private func initLogFile() {
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("LiveRoom.log")
        let fullDestPathString = fullDestPath!.path
        if !FileManager.default.fileExists(atPath: fullDestPathString) {
            do {
                try "".write(toFile: fullDestPathString, atomically: true, encoding: String.Encoding.utf8)
            } catch {
                NSLog("Can't write to file to device directory - Error: \(error.localizedDescription)")
            }
        }
        handler = FileHandle.init(forUpdatingAtPath: fullDestPathString)
    }
    
    fileprivate func writeLogToFile(file: String, function: String, line: Int, queue: String, type: LogType,_ format: String, _ args: CVarArg...) {
        var typeText = ""
        let fileStrArray = file.split(separator: "/")
        let fileStr = fileStrArray.last!
        switch type {
        case .Verbose:
            typeText = "VERBOSE >>"
        case .Info:
            typeText = "INFO >>"
        case .Warning:
            typeText = "WARNING >>"
        case .MemoryWarning:
            typeText = "MEMORY WARNING >>"
        case .Error:
            typeText = "ERROR >>"
        }
        let dateStr = DateFormatter.localizedString(from: Date(), dateStyle: .short, timeStyle: .medium)
        let strWithoutDate = String(format: "[\(typeText)] [FUNCTION: \(function) AT \(fileStr).\(line) QUEUE: \(queue)] \(format) \n", args)
        let logStr = String(format: "[\(dateStr)]: \(strWithoutDate)")
        if type.rawValue >= self.logLevel.rawValue {
            NSLog(strWithoutDate)
            handler?.seekToEndOfFile()
            if let logData = logStr.data(using: String.Encoding.utf8) {
                handler?.write(logData)
            }
        }
    }
    
    public func createZipFile() -> String {
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("LiveRoom.log")
        let fullDestPathString = fullDestPath!.path
        return fullDestPathString
    }
    
    public func clearLogs() {
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("LiveRoom.log")
        let fullDestPathString = fullDestPath!.path
        do {
            try "".write(toFile: fullDestPathString, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            NSLog("Can't write to file to device directory - Error: \(error.localizedDescription)")
        }
        initLogFile()
        LogInfo("Log file is cleaned")
    }
}

