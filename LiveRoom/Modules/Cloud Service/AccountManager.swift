//
//  AccountManager.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 2.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth
import Lottie

extension CloudService {
    
    class AccountManager {
        
        static let instance = AccountManager()
        public var delegates = [AccountManagerDelegate]()
        private var id: String?
        private var backgroundTaskID: UIBackgroundTaskIdentifier?
        
        public var loginStatus: LoginStatus = .initial {
            didSet {
                if oldValue == self.loginStatus {
                    return
                }
                for delegate in self.delegates {
                    delegate.loginStatusChanged(status: self.loginStatus)
                }
            }
        }
        
        private var db: Firestore {
            let dbs = Firestore.firestore()
            let settings = dbs.settings
            settings.areTimestampsInSnapshotsEnabled = true
            dbs.settings = settings
            return dbs
        }
        
        private init() {
        }
        
        func start(withDelegate: AccountManagerDelegate? = nil) {
            if let delegate = withDelegate {
                if !self.delegates.contains(where: { delegate.isEqual($0) }) {
                    self.delegates.append(delegate)
                }
            }
            
            Utilities.UserInfoListHandler.sharedInstance.reloadConfigurations()
            if Utilities.UserInfoListHandler.sharedInstance.type == .anonym {
                self.loginStatus = .anonym
            }
            
            self.checkAuth()
            self.getUserInfo()
        }
        
        // MARK: - Anonym Flow
        
        public func registerAnonym() {
            let userId = (UIDevice.current.identifierForVendor?.uuidString)!
            let type = UserTypes(rawValue: 0)!
            Utilities.UserInfoListHandler.sharedInstance.setValue(type.rawValue, for: "type")
            Utilities.UserInfoListHandler.sharedInstance.setValue(userId, for: "id")
            AccountManager.redirectToMain()
            self.loginStatus = .anonym
        }
        
        
        // MARK: - Login Flow
        
        public func login(credential: String, password: String, completion: @escaping (_ error: NSError?)->()) {
            self.loginToAuth(email: credential, password: password) { (error) in
                completion(error)
            }
        }
        
        public func logOut(completion: ((_ error: NSError?)->())?) {
            do {
                try Auth.auth().signOut()
                self.loginStatus = .loggedOut
                if Utilities.UserInfoListHandler.sharedInstance.resetData() {
                    if LDBService.resetLDB() {
                        completion?(nil)
                    } else {
                        let err = NSError(domain: ErrorDomains.cloudDatabaseError.rawValue, code: ErrorCodes.taskError.rawValue, description: "Couldn't remove data from LDB")
                        completion?(err)
                    }
                } else {
                    let err = NSError(domain: ErrorDomains.cloudDatabaseError.rawValue, code: ErrorCodes.taskError.rawValue, description: "Couldn't remove user data from device")
                    completion?(err)
                }
            } catch {
                let err = NSError(domain: ErrorDomains.cloudDatabaseError.rawValue, code: ErrorCodes.taskError.rawValue, description: "Couldn't signed out - Reason: \(error.localizedDescription)")
                completion?(err)
            }
        }
        
        private func loginToAuth(email: String, password: String, completion: @escaping (_ error: NSError?)->()) {
            //            if self.loginStatus == .connecting {
            //                return
            //            }
            
            if self.loginStatus == .loggedIn {
                completion(nil)
                return
            }
            
            Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
                if let error = error {
                    completion(error as NSError)
                    return
                }
                if let result = result {
                    completion(nil)
                    self.saveUserInfo(result.user.uid, email: email, password: password, with: .standard)
                } else {
                    let err = NSError(domain: ErrorDomains.cloudDatabaseError.rawValue, code: ErrorCodes.taskError.rawValue, description: "Couldn't find any user info")
                    completion(err)
                }
            }
        }
        
        public func trySilentLogin(completion: ((_ error: NSError?)->())?) {
            guard let encodedPass = Utilities.UserInfoListHandler.sharedInstance.getValue(for: "password") as? String,
                let email = Utilities.UserInfoListHandler.sharedInstance.getValue(for: "email") as? String,
                let decodedPass = self.decodePassword(password: encodedPass) else { return }
            self.loginToAuth(email: email, password: decodedPass) { (error) in
                if let error = error {
                    completion?(error)
                    return
                }
                //self.loginStatus = .loggedIn
                completion?(nil)
            }
        }
        
        private func checkAuth() {
            if self.loginStatus == .connecting { return }
            if self.loginStatus == .anonym { return }
            self.loginStatus = .connecting
            Auth.auth().addStateDidChangeListener { (auth, user) in
                if user != nil {
                    self.loginStatus = .loggedIn
                    if let email = user!.email {
                        self.saveUserInfo(user!.uid, email: email, password: nil, with: .standard)
                    } else {
                        // No email record found for user - try with FB token
                        FacebookLoginManager.getEmail(completion: { (email) in
                            if let email = email {
                                if let token = FacebookLoginManager.getToken() {
                                    let credential = FacebookAuthProvider.credential(withAccessToken: token)
                                    self.retrieveData(with: credential, completion: { (result) in
                                        user?.updateEmail(to: email, completion: { (error) in
                                            if let error = error {
                                                LogError("Couldn't update user email - Error: \(error.localizedDescription)")
                                                return
                                            }
                                            self.saveUserInfo(user!.uid, email: email, password: nil, with: .standard)
                                        })
                                    })
                                }
                            }
                        })
                        

                    }
                    self.id = user!.uid
                    self.updateLastLogin(userId: user!.uid)
                } else {
                    if Utilities.UserInfoListHandler.sharedInstance.type != .anonym {
                        self.loginStatus = .loggedOut
                    }
                }
            }
        }
        
        private func updateLastLogin(userId: String) {
            let dataDict = [
                "lastLogin": Date()
            ]
            db.collection("Users").document(userId).setData(dataDict, merge: true) { (error) in
                if let error = error {
                    LogError("Couldn't update last login for user Id: \(userId) - Error: \(error.localizedDescription)")
                    return
                }
                
                LogInfo("Last Login is updated for user Id: \(userId)")
            }
        }
        
        public func updateLastSeen() {
            DispatchQueue.global().async {
                self.backgroundTaskID = UIApplication.shared.beginBackgroundTask(withName: "updateLastSeen", expirationHandler: {
                    UIApplication.shared.endBackgroundTask(self.backgroundTaskID!)
                    self.backgroundTaskID = UIBackgroundTaskInvalid
                })
                
                guard let userId = self.id else { return }
                let dataDict = [
                    "lastSeen": Date()
                ]
                self.db.collection("Users").document(userId).setData(dataDict, merge: true) { (error) in
                    if let error = error {
                        LogError("Couldn't update last seen for user Id: \(userId) - Error: \(error.localizedDescription)")
                    } else {
                        LogInfo("Last Seen is updated for user Id: \(userId)")
                    }
                    UIApplication.shared.endBackgroundTask(self.backgroundTaskID!)
                }
            }
        }
        
        // MARK: - FB Login Flow
        
        public func fbLogin(_ token: String) {
            let credential = FacebookAuthProvider.credential(withAccessToken: token)
            self.retrieveData(with: credential) { (result) in
                
            }
        }
        
        public func saveProfilePic(url: String) {
            if let id = id {
                CloudService.Database.instance.saveProfilePic(id: id, url: url)
            } else {
                LogError("Couldn't find user id for function: \(#function)")
            }
        }
        
        private func retrieveData(with credential: AuthCredential, completion: @escaping (_ result: AuthDataResult?)->()) {
            Auth.auth().signInAndRetrieveData(with: credential) { (result, error) in
                if let error = error {
                    LogError("Couldn't get Auth credentials - Error: \(error.localizedDescription) - Credential: \(credential.provider)")
                    completion(nil)
                    return
                }
                
                LogInfo("Auth credentials are retrieved successfuly - User: \(result!.user.displayName ?? "")")
                completion(result)
            }
        }
        
        // MARK: - Registration Flow
        
        private func canRegister(email: String, completion: @escaping (_ error: NSError?, _ canRegister: Bool)->()) {
            db.collection("Users").whereField("email", isEqualTo: email).getDocuments { (snapshot, error) in
                if let error = error {
                    LogError("Couldn't check user for registration - Error: \(error.localizedDescription)")
                    completion(error as NSError, false)
                    return
                }
                if let snapshot = snapshot {
                    if snapshot.documents.count > 0 {
                        completion(nil, false)
                    } else {
                        completion(nil, false)
                    }
                } else {
                    completion(nil, true)
                }
            }
        }
        
        public func register(email: String, password: String, completion: @escaping (_ error: NSError?)->()) {
            self.canRegister(email: email) { (error, canRegister) in
                if let error = error {
                    let err = NSError(domain: ErrorDomains.cloudDatabaseError.rawValue, code: ErrorCodes.taskError.rawValue, description: error.description)
                    completion(err)
                } else {
                    if let passwordencoded = self.encodePassword(password: password) {
                        let userDict = [
                            "email": email,
                            "password": passwordencoded,
                            "type": UserTypes.standard.rawValue,
                            ] as [String : Any]
                        self.createUserAuthentication(email: email, password: password, completion: { (error, userId) in
                            if let error = error {
                                completion(error)
                                return
                            }
                            self.createUser(id: userId!, dict: userDict, completion: completion)
                            self.saveDeviceInfoToUser(userId: userId!)
                        })
                        
                    } else {
                        let err = NSError(domain: ErrorDomains.localError.rawValue, code: ErrorCodes.taskError.rawValue, description: "Couldn't encode password")
                        completion(err)
                    }
                    
                }
            }
        }
        
        public func createUserAuthentication(email:String, password: String, completion: @escaping (_ error: NSError?, _ userId: String?)->()) {
            Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
                if let error = error {
                    completion(error as NSError, nil)
                    return
                }
                
                self.loginToAuth(email: email, password: password, completion: { (error) in
                    completion(error, result?.user.uid)
                })
            }
        }
        
        public func saveUserInfo(_ id: String, email: String, password: String?, with type: UserTypes) {
            Utilities.UserInfoListHandler.sharedInstance.setValue(id, for: "id")
            Utilities.UserInfoListHandler.sharedInstance.setValue(type.rawValue, for: "type")
            Utilities.UserInfoListHandler.sharedInstance.setValue(email, for: "email")
            if password != nil {
                Utilities.UserInfoListHandler.sharedInstance.setValue(self.encodePassword(password: password!)!, for: "password")
            }
        }
        
        private func createUser(id: String, dict: [String:Any], completion: @escaping (_ error: NSError?)->()) {
            var newDict = dict
            newDict["id"] = id
            db.collection("Users").document(id).setData(newDict) { (error) in
                if let error = error {
                    completion(error as NSError)
                } else {
                    completion(nil)
                }
            }
            
            
        }
        
        private func saveDeviceInfoToUser(userId: String) {
            let dataDict = [
                "registrationDate": Date(),
                "model": UIDevice.modelName,
                "UUID": UIDevice.current.identifierForVendor!.uuidString,
                "OSVersion": UIDevice.current.systemVersion
                ] as [String:Any]
            db.collection("Users").document(userId).collection("Devices").document().setData(dataDict, merge: true) { (error) in
                if let error = error {
                    LogError("Couldn't save device info for user Id: \(userId) - Error: \(error.localizedDescription) - Data: \(dataDict.description)")
                }
            }
        }
        
        
        private func getUserInfo() {
            if let userId = Utilities.UserInfoListHandler.sharedInstance.id, Utilities.UserInfoListHandler.sharedInstance.type != .anonym {
                db.collection("Users").whereField("id", isEqualTo: userId).addSnapshotListener { (snapshot, error) in
                    if let error = error {
                        LogError("Couldn't get user info - Error: \(error.localizedDescription)")
                        return
                    }
                    
                    if snapshot != nil {
                        if snapshot!.documents.count > 0 {
                            snapshot!.documentChanges.forEach { diff in
                                if (diff.type == .modified || diff.type == .added) {
                                    self.parseDocument(data: diff.document.data())
                                }
                            }
                        } else {
                            self.loginStatus = .rejected
                        }
                    } else {
                        self.loginStatus = .rejected
                    }
                }
            }
        }
        
        private func parseDocument(data: [String:Any]) {
            if let email = data["email"] as? String { Utilities.UserInfoListHandler.sharedInstance.setValue(email, for: "email") }
            if let id = data["id"] as? String { Utilities.UserInfoListHandler.sharedInstance.setValue(id, for: "id") }
            if let password = data["password"] as? String { Utilities.UserInfoListHandler.sharedInstance.setValue(password, for: "password") }
            if let type = data["type"] as? Int { Utilities.UserInfoListHandler.sharedInstance.setValue(type, for: "type") }
            if let profilePicture = data["profile_picture"] as? String { Utilities.UserInfoListHandler.sharedInstance.setValue(profilePicture, for: "profile_picture") }
        }
        
        private func encodePassword(password: String) -> String? {
            let dict = ["original": password]
            do {
                let data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
                return data.base64EncodedString()
            } catch {
                return nil
            }
            
        }
        
        private func decodePassword(password: String) -> String? {
            guard let data = Data(base64Encoded: password) else { return nil }
            do {
                guard let obj = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:String], let pass = obj["original"] else { return nil }
                return pass
            } catch {
                return nil
            }
            
        }
        
        // MARK: - UI Functions
        
        static func redirectToMain() {
            MainService.sync {
                let vc = UIViewController.ViewControllers.MainStoryboard.tabBarVC()
                AccountManager.redirectVC(vc)
            }
        }
        
        static func redirectToRegistration() {
            let vc = UIViewController.ViewControllers.RegistrationStoryboard.initialVC()
            AccountManager.redirectVC(vc)
        }
        
        private static func redirectVC(_ vc: UIViewController) {
//            vc.view.frame.origin.y = Utilities.instance.deviceHeight
//            guard let rootVC = UIApplication.shared.keyWindow?.rootViewController else { return }
//            let snapshot = UIImage(view: rootVC.view)
//            let snapView = UIImageView(frame: rootVC.view.frame)
//            snapView.image = snapshot //UIImage(named: "default_background")
//            snapView.contentMode = .scaleAspectFill
            
            UIApplication.shared.keyWindow?.rootViewController = vc
            let animationView = LOTAnimationView(name: "welcomeAnimation")
            animationView.frame = vc.view.frame
            animationView.animationSpeed = 0.5
            animationView.contentMode = .scaleAspectFill
            vc.view.addSubview(animationView)
            animationView.play { (completed) in
                if completed {
                    animationView.removeFromSuperview()
                    for subview in vc.view.subviews {
                        if let subview = subview as? LOTAnimationView {
                            subview.removeFromSuperview()
                        }
                    }
                }
            }
        }
//            let secondSnapView = UIImageView(frame: CGRect(x: vc.view.frame.width/2, y: vc.view.frame.height/2, width: 0, height: 0))
//            let snapshot2 = UIImage(view: vc.view)
//            secondSnapView.image = snapshot2
//            secondSnapView.clipsToBounds = true
//            secondSnapView.contentMode = .center
//
//            vc.view.addSubview(snapView)
//            vc.view.addSubview(secondSnapView)
//            vc.view.bringSubview(toFront: snapView)
//            vc.view.bringSubview(toFront: secondSnapView)
//            
//            UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0, options: .curveEaseIn, animations: {
//                let w = vc.view.frame.width/2
//                let h = vc.view.frame.height/2
//                let corRadi =  sqrt((w*w)+(h*h))
//                let midX = vc.view.frame.width/2
//                let midY = vc.view.frame.height/2
//                secondSnapView.layer.cornerRadius = corRadi
//                secondSnapView.frame = CGRect(x: midX-corRadi, y: midY-corRadi, width: corRadi*2, height: corRadi*2)
//            }) { (completed) in
//                snapView.removeFromSuperview()
//                secondSnapView.removeFromSuperview()
//            }
//        }
//
        
    }
    
    
}

class AccountDelegate: NSObject, AccountManagerDelegate {
    
    static let instance = AccountDelegate()
    private var isMain = false
    
    private override init() {
        super.init()
        CloudService.AccountManager.instance.delegates.append(self)
    }
    
    func loginStatusChanged(status: LoginStatus) {
        LogInfo("Connection status is changed to: \(status)")
        if status == .loggedIn {
            if !isMain { CloudService.AccountManager.redirectToMain(); isMain = true }
            return
        }
        
        if status == .anonym {
            if !isMain { CloudService.AccountManager.redirectToMain(); isMain = true }
            return
        }
        
        if status == .connecting {
            return
        }
        
        if status == .loggedOut {
            if isMain { CloudService.AccountManager.redirectToRegistration(); isMain = false }
        }
    }
}

