//
//  Database.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 2.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import FirebaseFirestore

extension CloudService {
    
    class Database {
        
        static let instance = Database()
        private static var db: Firestore {
            return Firestore.firestore()
        }
        
        private init() {
            
        }
        
        public func connectRoom(id: String, connection: @escaping (_ room: RoomObject?)->()) {
            CloudService.Database.db.collection("Rooms").document(id).addSnapshotListener { (snapshot, error) in
                if let error = error {
                    LogError("Couldn't connect room - Error: \(error.localizedDescription)")
                    return
                }
                connection(self.parseRoomObject(data: snapshot?.data()))
            }
        }
        
        public func saveRoom(_ room: RoomObject, completion: ((_ stored: Bool)->())? = nil) {
            CloudService.Database.db.collection("Rooms").document(room.id).setData(room.dictionary(), merge: true) { (error) in
                if let error = error {
                    LogError("Couldn't save the room info - Error: \(error.localizedDescription) - Room: \(room.dictionary())")
                    completion?(false)
                    return
                }
                
                LogInfo("Room info is saved to cloud successfuly - Room: \(room.dictionary().description)")
                completion?(true)
            }
        }
        
        public func hideRoom(_ id: String) {
            CloudService.Database.db.collection("Rooms").document(id).setData(["isHidden": true], merge: true) { (error) in
                if let error = error {
                    LogError("Couldn't hide the room info - Error: \(error.localizedDescription) - Room ID: \(id)")
                    return
                }
            }
        }
        
        public func saveMedia(_ media: MediaObject) {
            CloudService.Database.db.collection("Rooms").document(media.roomId).collection("Media").document(media.id).setData(media.dictionary(), merge: true) { (error) in
                if let error = error {
                    LogError("Couldn't save the media info - Error: \(error.localizedDescription) - Media: \(media.dictionary())")
                    return
                }
                
                LogInfo("Media info is saved to cloud successfuly - Media: \(media.dictionary())")
            }
        }
        
        public func hideMedia(_ media: MediaObject) {
            getRoom(roomId: media.roomId) { (error, room) in
                if let error = error {
                    LogError("Couldn't get room properties for media deletion - Error: \(error.localizedDescription) - Media: \(media.dictionary())")
                    return
                }
                
                guard let room = room else { return }
                    if room.isOwner() {
                        CloudService.Database.db.collection("Rooms").document(media.roomId).collection("Media").document(media.id).setData(["isHidden": true], merge: true) { (error) in
                            if let error = error {
                                LogError("Couldn't hide the media info - Error: \(error.localizedDescription) - Media ID: \(media.dictionary())")
                                return
                            }
                            
                            LogInfo("Media is hidden from cloud successfuly - Media: \(media.dictionary())")
                        }
                    }
            }
            
        }
        
        public func saveVideo(_ video: VideoAsset, completion: ((_ error: NSError?)->())?) {
            StorageService.instance.saveVideo(video) { (error) in
                if let error = error {
                    LogError("Couldn't save video - Error: \(error.localizedDescription)")
                    completion?(error)
                    return
                }
                let tempMedia = MediaObject(id: video.mediaId, roomId: video.roomId, videoAssetId: nil, imageAssetId: nil)
                self.updateMedia(value: video.id, key: "videoId", media: tempMedia, completion: completion)
                
            }
        }
        
        public func saveImage(_ image: ImageAsset, completion: ((_ error: NSError?)->())?) {
            StorageService.instance.saveImage(image) { (error) in
                if let error = error {
                    LogError("Couldn't save image - Error: \(error.localizedDescription)")
                    completion?(error)
                    return
                }
                
                let tempMedia = MediaObject(id: image.mediaId, roomId: image.roomId, videoAssetId: nil, imageAssetId: nil)
                self.updateMedia(value: image.id, key: "imageId", media: tempMedia, completion: completion)
            }
        }
        
        public func updateMedia(value: Any, key: String, media: MediaObject, completion: ((_ error: NSError?)->())?) {
            CloudService.Database.db.collection("Rooms").document(media.roomId).collection("Media").document(media.id).setData([key:value]) { (error) in
                if let error = error {
                    LogError("Couldn't update the media - Error: \(error.localizedDescription)")
                    completion?(error as NSError)
                    return
                }
                completion?(nil)
            }
        }
        
        public func getRoom(roomId: String, completion: @escaping (_ error: NSError?, _ room: RoomObject?)->()) {
            CloudService.Database.db.collection("Rooms").document(roomId).getDocument { (snapshot, error) in
                if let error = error {
                    completion(error as NSError, nil)
                    return
                }
                
                let room = self.parseRoomObject(data: snapshot!.data())
                completion(nil, room)
            }
        }
        
        public func getUsername(id: String, completion: @escaping (_ error: NSError?, _ username: String?)->()) {
            CloudService.Database.db.collection("Users").whereField("id", isEqualTo: id).getDocuments { (snapshot, error) in
                if let error = error {
                    completion(error as NSError, nil)
                    return
                }
                let error = NSError(domain: ErrorDomains.cloudDatabaseError.rawValue, code: ErrorCodes.unspecified.rawValue, description: "Couldn't find any user with specified id: \(id)")
                guard let snapshot = snapshot, snapshot.count > 0 else { completion(error, nil);return }
                guard let username = snapshot.documents.first?.data()["username"] as? String else { completion(error, nil);return }
                completion(nil, username)
            }
        }
        
        public func saveProfilePic(id: String, url: String) {
            MainService.HTTPEngine.downloadFrom(url) { (error, data) in
                if let error = error {
                    LogError("Couldn't downloaded image from url: \(url) - Error: \(error.localizedDescription)")
                    return
                }
                guard let data = data else { return }
                StorageService.instance.saveProfilePicture(data: data, completion: { (uid) in
                    guard let uid = uid else { return }
                    CloudService.Database.db.collection("Users").document(id).setData(["profile_picture": uid], merge: true) { (error) in
                        if let error = error {
                            LogError("Couldn't save profile picture - Error: \(error.localizedDescription) - ID: \(id) - Url: \(url)")
                            return
                        }
                        
                        LogInfo("Profile picture is saved to Cloud successfuly - ID: \(id) - Url: \(url)")
                        Utilities.UserInfoListHandler.sharedInstance.setValue(uid, for: "profile_picture")
                    }
                })
                
            }
            
        }
        
        private func parseRoomObject(data: [String:Any]?) -> RoomObject? {
            if data != nil {
                let isHidden = data!["isHidden"] as? Bool ?? false
                if isHidden { return nil }
                let coverImage = data!["coverImage"] as? String
                let creationDate = (data!["creationDate"] as? Timestamp)?.dateValue() ?? Date()
                guard let id = data!["id"] as? String,
                    let name = data!["name"] as? String,
                    let owner = data!["owner"] as? String else { return nil }
                let obj = RoomObject(id: id, name: name, owner: owner, creationDate: creationDate, coverImage: coverImage)
                return obj
            } else { return nil }
        }
        
        
    }
    
}
