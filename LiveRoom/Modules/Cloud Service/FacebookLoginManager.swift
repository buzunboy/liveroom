//
//  FacebookLoginManager.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 15.08.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import FBSDKLoginKit

extension CloudService {
    
    class FacebookLoginManager {
        
        private var viewController: UIViewController!
        
        init(viewController: UIViewController) {
            self.viewController = viewController
        }
        
        func login() {
            LogInfo("Facebook Login manager is called")
            let manager = FBSDKLoginManager()
            manager.logIn(withReadPermissions: ["public_profile", "email", "user_photos"], from: self.viewController) { (result, error) in
                if let error = error {
                    LogError("Couldn't login with facebook - Error: \(error.localizedDescription)")
                    return
                }
                
                if result!.isCancelled {
                    LogWarning("Facebook Login is cancelled by user")
                    return
                }
                LogInfo("Facebook Login is successful - Auth Operation is being started")
                FacebookLoginManager.getProfilePic()
                AccountManager.instance.fbLogin(FBSDKAccessToken.current().tokenString)
            }
        }
        
        public static func getToken() -> String? {
            return (FBSDKAccessToken.current().tokenString != nil) ? FBSDKAccessToken.current().tokenString : nil
        }
        
        public static func getProfilePic() {
            FBSDKGraphRequest.init(graphPath: "/me", parameters: ["fields": "id, name, first_name, last_name, email, picture.type(large)"], httpMethod: "GET").start { (connection, result, error) in
                if let error = error {
                    LogError("Couldn't get user email from Facebook - Error: \(error.localizedDescription)")
                    return
                }
                
                guard let userInfo = result as? [String: Any],
                let picDict = userInfo["picture"] as? [String:Any],
                    let dataDict = picDict["data"] as? [String:Any],
                    let url = dataDict["url"] as? String else { return }
                
                CloudService.AccountManager.instance.saveProfilePic(url: url)
                
            }
        }
        
        public static func getEmail(completion: @escaping (_ email: String?)->()) {
            FBSDKGraphRequest.init(graphPath: "/me", parameters: ["fields": "id, name, first_name, last_name, email, picture.type(large)"], httpMethod: "GET").start { (connection, result, error) in
                if let error = error {
                    LogError("Couldn't get user email from Facebook - Error: \(error.localizedDescription)")
                    completion(nil)
                    return
                }
                
                guard let userInfo = result as? [String: Any] else {
                    completion(nil)
                    return
                    
                }
                completion(userInfo["email"] as? String)
            }
        }
        
    }
}
