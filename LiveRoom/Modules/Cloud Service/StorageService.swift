//
//  StorageService.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 2.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import FirebaseStorage

extension CloudService {
    
    class StorageService {
        
        static let instance = StorageService()
        private var ref = Storage.storage().reference()
        
        private init() {
            
        }
        
        public func saveVideo(_ video: VideoAsset, completion: ((_ error: NSError?)->Void)?) {
            let videoRef = ref.child("Videos").child(video.id)
            guard let videoData = MainService.VideoStorage(folderName: "videos").getVideoDataFromStorage(name: video.name) else { return }
            videoRef.putData(videoData, metadata: nil) { (metadata, error) in
                if let error = error {
                    LogError("Couldn't upload the video - Error: \(error.localizedDescription) - Video: \(video.dictionary())")
                    completion?(error as NSError)
                } else {
                    completion?(nil)
                }
            }
        }
        
        public func saveImage(_ image: ImageAsset, completion: ((_ error: NSError?)->Void)?) {
            let imageRef = ref.child("Images").child(image.id)
            guard let imgData = MainService.ImageStore(folderName: "images").getImageData(name: image.name) else { return }
            imageRef.putData(imgData, metadata: nil) { (metadata, error) in
                if let error = error {
                    LogError("Couldn't upload the image - Error: \(error.localizedDescription) - Image: \(image.dictionary())")
                    completion?(error as NSError)
                } else {
                    completion?(nil)
                }
            }
        }
        
        public func saveCoverImage(_ image: UIImage, completion: ((_ error: NSError?,_ imageId: String?)->Void)?) {
            let imageId = Utilities.createNewid()
            let imageRef = ref.child("Images").child(imageId)
            let data = UIImageJPEGRepresentation(image, 1.0)
            imageRef.putData(data!, metadata: nil) { (metadata, error) in
                if let error = error {
                    completion?(error as NSError, nil)
                } else {
                    completion?(nil, imageId)
                }
            }
        }
        
        public func getImage(imageId: String, completion: @escaping (_ error: NSError?, _ image: UIImage?)->()) {
            let imageRef = ref.child("Images").child(imageId)
            imageRef.downloadURL { (url, error) in
                if let error = error {
                    completion(error as NSError, nil)
                    return
                }
                do {
                    let data = try Data(contentsOf: url!)
                    completion(nil, UIImage(data: data))
                } catch {
                    completion(error as NSError, nil)
                }
            }
        }
        
        public func saveProfilePicture(data: Data, completion: @escaping (_ id: String?)->()) {
            let uid = UUID().uuidString
            let imageRef = ref.child("Images").child(uid)
            imageRef.putData(data, metadata: nil) { (metadata, error) in
                if let error = error {
                    LogError("Error occured during profile picture upload - Error: \(error.localizedDescription)")
                    completion(nil)
                    return
                }
                
                completion(uid)
            }
        }
        
//        public func getCoverImage(roomId: String, completion: @escaping ((_ error: NSError?, _ image: UIImage?)->Void)) {
//            let imageRef = ref.child("Covers").child(roomId)
//            imageRef.downloadURL { (url, error) in
//                if let error = error {
//                    completion(error as NSError, nil)
//                    return
//                }
//                do {
//                    let data = try Data(contentsOf: url!)
//                    completion(nil, UIImage(data: data))
//                } catch {
//                    completion(error as NSError, nil)
//                }
//            }
//        }
        
    }
    
}
