//
//  ImageStorage.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 29.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

extension MainService {
    
    public class ImageStore: NSObject {
        
        private var imageStorageName = "images"
        
        /**
         * Initializes Image storage object in the specified folder path.
         * - important: If you need more specified path you can use **folderpath1/folderpath2** as a folderName.
         */
        init(folderName: String) {
            self.imageStorageName = folderName
        }
        
        /**
         * Loads image from the storage.
         *
         * If Image isn't saved before to device directory, saves image to device directory from the imageURL, and returns the newly created image.
         * - parameter imageURL: URL path for the image in case cannot be found in the device directory.
         * - parameter name: Name of the saved image. If image is downloaded from url, it will also saved with that name.
         * - parameter splashImage: If image cannot be found in the device directory and also couldn't be downloaded, handler will return splash image.
         * - parameter quality: Compression quality for the image. It will only effect newly downloaded images.
         * - parameter handler: Will be called when operations are completed or failed.
         * - parameter image: The result of the operation.
         */
        public func getImageFromStorage(imageURL: String, splashImage: UIImage?, with quality: CGFloat = 1.0, handler: @escaping (_ image: UIImage?)->()){
            DispatchQueue.global().async {
                if imageURL != "" {
                    let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                    let filePath = documentsURL.appendingPathComponent("\(self.imageStorageName)/\(imageURL)").path
                    if FileManager.default.fileExists(atPath: filePath) {
                        DispatchQueue.main.async {
                            handler(UIImage(contentsOfFile: filePath))
                        }
                    } else {
                        self.saveImageToStorage(imageURL: imageURL, name: imageURL, quality: quality, handler: { (error, loaded) in
                            if let image = loaded {
                                DispatchQueue.main.async {
                                    handler(image)
                                }
                            }
                        })
                    }
                } else {
                    DispatchQueue.main.async {
                        handler(splashImage)
                    }
                }
            }
        }
        
        public func getImageImmediately(imageURL: String, with quality: CGFloat = 1.0) -> UIImage? {
            var retVal: UIImage?
            if imageURL != "" {
                let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                let filePath = documentsURL.appendingPathComponent("\(self.imageStorageName)/\(imageURL)").path
                if FileManager.default.fileExists(atPath: filePath) {
                    retVal = UIImage(contentsOfFile: filePath)
                }
            }
            return retVal
        }
        
        /**
         * Loads image from the storage.
         *
         * - parameter name: Name of the image.
         * - parameter splashImage: If image cannot be found in the device directory, handler will return splash image.
         * - parameter handler: Will be called when operations are completed or failed.
         * - parameter image: The result of the operation.
         */
        public func getImageFromStorage(name: String, splashImage: UIImage?, handler: @escaping (_ image: UIImage?)->()){
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let filePath = documentsURL.appendingPathComponent("\(self.imageStorageName)/\(name)").path
            if FileManager.default.fileExists(atPath: filePath) {
                handler(UIImage(contentsOfFile: filePath))
            } else {
                handler(splashImage)
            }
        }
        
        public func getImage(name: String) -> UIImage? {
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let filePath = documentsURL.appendingPathComponent("\(self.imageStorageName)/\(name)").path
            if FileManager.default.fileExists(atPath: filePath) {
                return UIImage(contentsOfFile: filePath)
            } else {
                return nil
            }
        }
        
        public func getImageData(name: String) -> Data? {
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let filePath = documentsURL.appendingPathComponent("\(self.imageStorageName)/\(name)").path
            if FileManager.default.fileExists(atPath: filePath) {
                do {
                    return try Data(contentsOf: URL(fileURLWithPath: filePath))
                } catch {
                    return nil
                }
            } else {
                return nil
            }
        }
        
        /**
         * Downloads image from the url and saves to the directory.
         *
         * - parameter imageURL: URL path for the image.
         * - parameter name: Name for the saved image in the device directory.
         * - parameter quality: Compression quality for the saved image.
         * - parameter handler: Will be called when operations are completed or failed.
         * - parameter image: The result of the operation.
         */
        public func saveImageToStorage(imageURL: String, name: String, quality: CGFloat = 1.0, handler: ((_ error: NSError?, _ image: UIImage?)->())?) {
            DispatchQueue.global().async {
                ImageCacheLoader.sharedInstance.loadImage(imageURL, to: nil, withSplashImage: nil) { (loaded, image) in
                    if let image = image {
                        do {
                            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                            let folderURL = documentsURL.appendingPathComponent(self.imageStorageName)
                            if !FileManager.default.fileExists(atPath: folderURL.absoluteString) {
                                self.createDirectory(name: self.imageStorageName)
                            }
                            let fileURL = folderURL.appendingPathComponent(name)
                            try UIImageJPEGRepresentation(image, quality)?.write(to: fileURL, options: .atomic)
                            //try data(quality)?.write(to: fileURL, options: .atomic)
                            DispatchQueue.main.async {
                                handler?(nil, image)
                            }
                        } catch {
                            LogError("Couldn't save the image - Error: \(error.localizedDescription)")
                            DispatchQueue.main.async {
                                handler?(error as NSError, nil)
                            }
                        }
                    }
                }
            }
            
        }
        
        public func saveImageToStorage(with image: UIImage, name: String, quality: CGFloat = 1.0, handler: ((_ error: NSError?, _ url: URL?)->())?) {
            do {
                let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                let folderURL = documentsURL.appendingPathComponent(self.imageStorageName)
                if !FileManager.default.fileExists(atPath: folderURL.absoluteString) {
                    self.createDirectory(name: self.imageStorageName)
                }
                let fileURL = folderURL.appendingPathComponent(name)
                try UIImageJPEGRepresentation(image, quality)?.write(to: fileURL, options: .atomic)
//                let data = UIImage.jpegData(image)
//                try data(quality)?.write(to: fileURL, options: .atomic)
                
                handler?(nil, fileURL)
            } catch {
                LogError("Couldn't save the image - Error: \(error.localizedDescription)")
                handler?(error as NSError, nil)
            }
        }
        
        public func deleteImage(imageURL: String) {
            DispatchQueue.global().async {
                let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                let filePath = documentsURL.appendingPathComponent("\(self.imageStorageName)/\(imageURL)").path
                if FileManager.default.fileExists(atPath: filePath) {
                    do {
                        try FileManager.default.removeItem(atPath: filePath)
                    } catch {
                        LogError("Couldn't remove image from directory - Error: \(error.localizedDescription) - imageURL: \(imageURL)")
                    }
                }
            }
        }
        
        /**
         * Creates directory in documents with the specified name.
         * - parameter name: Name of the directory.
         */
        private func createDirectory(name: String) {
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let directoryPath = documentsURL.appendingPathComponent(name)
            do {
                try FileManager.default.createDirectory(atPath: directoryPath.path, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                LogError("Unable to create directory - Error: \(error.localizedDescription)")
            }
        }
        
    }
    
    private class ImageCacheLoader {
        
        var task: URLSessionDownloadTask!
        var session: URLSession!
        typealias ImageCacheLoaderCompletionHandler = ((UIImage?) -> ())
        var imageCache: NSCache<NSString, UIImage>!
        
        static let sharedInstance = ImageCacheLoader()
        
        private init() {
            session = URLSession.shared
            task = URLSessionDownloadTask()
            imageCache = NSCache()
        }
        
        func loadImage(_ image: String?, to: UIImageView?, withSplashImage: UIImage?, handler: @escaping (_ result: Bool, _ loaded: UIImage?) -> Void){
            if image != nil {
                if image != "" {
                    self.obtainImageWithPath(imagePath: image!, splashImage: withSplashImage, completionHandler: { (imageLoaded) in
                        to?.image = imageLoaded
                        to?.clipsToBounds = true
                        to?.contentMode = .scaleAspectFill
                        handler(true, imageLoaded)
                    })
                } else {
                    if withSplashImage == .none {
                        handler(false, nil)
                        return
                    }
                    handler(false, withSplashImage)
                    LogError("Image string is empty!, splash image is used for \(to?.description ?? "")")
                    to?.image = withSplashImage
                    to?.clipsToBounds = true
                    to?.contentMode = .scaleAspectFill
                }
            } else {
                if withSplashImage == .none {
                    handler(false, nil)
                    return
                }
                handler(false, withSplashImage)
                LogError("Image string is nil!, splash image is used for \(to?.description ?? "")")
                to?.image = withSplashImage
                to?.clipsToBounds = true
                to?.contentMode = .scaleAspectFill
            }
        }
        
        func obtainImageWithPath(imagePath: String, splashImage: UIImage?, completionHandler: @escaping ImageCacheLoaderCompletionHandler) {
            if let image = imageCache.object(forKey: imagePath as NSString) {
                completionHandler(image)
                LogInfo("Image is loaded from the cache: \(imagePath)")
            } else {
                CloudService.StorageService.instance.getImage(imageId: imagePath) { (error, image) in
                    if let error = error {
                        LogError("Couldn't download the image - image: \(imagePath) - Error: \(error.localizedDescription)")
                        completionHandler(splashImage)
                        return
                    }
                    if image != nil {
                        self.imageCache.setObject(image!, forKey: imagePath as NSString)
                    }
                    completionHandler(image)
                    LogInfo("Image is uploaded to the cache: %@", imagePath)
                    
                }
            }
        }
    }
    
}
