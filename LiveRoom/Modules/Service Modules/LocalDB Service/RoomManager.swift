//
//  RoomsManager.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 30.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import CoreData

extension LDBService {
    
    class RoomsManager: LDBSubClass {
        
        static let sharedInstance = AssetsManager()
        
        private var request: NSFetchRequest<NSFetchRequestResult>
        
        private override init() {
            self.request = LDBSubClass.getEntityRequest(with: "Assets")
            super.init()
        }
        
        func getAsset(id: String) -> AssetObject? {
            var retVal: AssetObject?
            do {
                let results = try context.fetch(self.request)
                if results.count > 0 {
                    for result in results as! [NSManagedObject] {
                        if let resultID = result.value(forKey: "id") as? String {
                            if resultID == id {
                                retVal = self.createAsset(from: result)
                            }
                        }
                    }
                }
            } catch {
                NSLog("Couldn't fetch results for \(#function) - Id: \(id) - Error: \(error.localizedDescription)")
            }
            return retVal
        }
        
        private func createAsset(from entity: NSManagedObject) -> AssetObject? {
            guard let id = entity.value(forKey: "id") as? String,
                let name = entity.value(forKey: "name") as? String,
                let type = entity.value(forKey: "type") as? Int,
                let owner = entity.value(forKey: "owner") as? String,
                let location = entity.value(forKey: "location") as? String else { return nil }
            let obj = AssetObject(id: id, name: name, location: location, owner: owner, assetType: AssetType(rawValue: type)!)
            return obj
        }
        
    }
    
}

