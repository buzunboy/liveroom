//
//  LDBSubClass.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 29.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import CoreData

protocol LDBSub {
    
    func resetEntity() -> Bool
    var request: NSFetchRequest<NSFetchRequestResult>! { get set }
    
}

extension LDBSub {
    
    func context() -> NSManagedObjectContext {
        return appDelegateEntity.persistentContainer.viewContext
    }
    
    func getEntityRequest(with name: String) -> NSFetchRequest<NSFetchRequestResult> {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: name)
        request.returnsObjectsAsFaults = false
        return request
    }
    
    func resetEntity() -> Bool {
        var retVal = false
        let batchReq = NSBatchDeleteRequest(fetchRequest: request)
        do {
            _ = try context().execute(batchReq)
            retVal = true
        } catch {
            LogError("Couldn't reset entity - Error: \(error.localizedDescription)")
            retVal = false
        }
        do {
            try context().save()
        } catch {
            LogError("Entity couldn't save - Error: \(error.localizedDescription)")
        }
        return retVal
    }
    
}
