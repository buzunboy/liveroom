//
//  RoomsManager.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 30.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import CoreData

extension LDBService {
    
    class RoomsManager: NSObject, LDBSub {
        var request: NSFetchRequest<NSFetchRequestResult>!
        static let sharedInstance = RoomsManager()
        
        private override init() {
            super.init()
            self.request = getEntityRequest(with: "Rooms")
        }

        func getRoom(id: String) -> RoomObject? {
            var retVal: RoomObject?
            do {
                let getContext = context()
                let results = try getContext.fetch(self.request)
                if results.count > 0 {
                    for result in results as! [NSManagedObject] {
                        if let resultID = result.value(forKey: "id") as? String {
                            if resultID == id {
                                retVal = self.createRoom(from: result)
                            }
                        }
                    }
                }
            } catch {
                LogError("Couldn't fetch results for \(#function) - Id: \(id) - Error: \(error.localizedDescription)")
            }
            return retVal
        }
        
        func getAllRooms() -> [RoomObject]? {
            var retVal = [RoomObject]()
            do {
                let getContext = context()
                let results = try getContext.fetch(self.request)
                if results.count > 0 {
                    for result in results as! [NSManagedObject] {
                        if let room = self.createRoom(from: result) {
                            retVal.append(room)
                        }
                    }
                }
            } catch {
                LogError("Couldn't fetch all room results - Error: \(error.localizedDescription)")
            }
            return retVal
        }
        
        func getRoomsFromUser() -> [RoomObject]? {
            var retVal = [RoomObject]()
            do {
                let getContext = context()
                let results = try getContext.fetch(self.request)
                if results.count > 0 {
                    for result in results as! [NSManagedObject] {
                        if let room = self.createRoom(from: result) {
                            if room.isOwner() {
                                retVal.append(room)
                            }
                        }
                    }
                }
            } catch {
                LogError("Couldn't fetch user room results - Error: \(error.localizedDescription)")
            }
            return retVal
        }
        
        func saveRoom(_ room: RoomObject) -> Bool {
            var retVal = false
            var isSavedBefore = false
            let saveContext = context()

            do {
                let results = try saveContext.fetch(self.request)
                if results.count > 0 {
                    for result in results as! [NSManagedObject] {
                        if let resultID = result.value(forKey: "id") as? String {
                            if resultID == room.id {
                                result.setValue(room.name, forKey: "name")
                                result.setValue(room.coverImage, forKey: "coverImage")
                                result.setValue(room.creationDate, forKey: "creationDate")
                                result.setValue(room.owner, forKey: "owner")
                                result.setValue(room.shouldStoreInCloud, forKey: "storeInCloud")
                                isSavedBefore = true
                            }
                        }
                    }
                }
            } catch {
                retVal = false
                LogError("Couldn't fetch results for \(#function) - Id: \(room.id ?? "nil") - Error: \(error.localizedDescription)")
            }
            
            if !isSavedBefore {
                let obj = NSEntityDescription.insertNewObject(forEntityName: "Rooms", into: context())
                obj.setValue(room.id, forKey: "id")
                obj.setValue(room.name, forKey: "name")
                obj.setValue(room.coverImage, forKey: "coverImage")
                obj.setValue(room.creationDate, forKey: "creationDate")
                obj.setValue(room.owner, forKey: "owner")
                obj.setValue(room.shouldStoreInCloud, forKey: "storeInCloud")
            }
            
            if saveContext.hasChanges {
                do {
                    try saveContext.save()
                    NotificationCenter.default.post(name: .LDBRMChanged, object: nil)
                    //room.saveToCloud()
                    retVal = true
                } catch {
                    retVal = false
                    LogError("Couldn't save results for \(#function) - Id: \(room.id ?? "nil") - Error: \(error.localizedDescription)")
                }
            }
            
            return retVal
        }
        
        func setStoreInCloud(to shouldStore: Bool, id: String) -> Bool {
            var retVal = false
            let saveContext = context()
            
            do {
                let results = try saveContext.fetch(self.request)
                if results.count > 0 {
                    for result in results as! [NSManagedObject] {
                        if let resultID = result.value(forKey: "id") as? String {
                            if resultID == id {
                                result.setValue(shouldStore, forKey: "storeInCloud")
                            }
                        }
                    }
                }
            } catch {
                retVal = false
                LogError("Couldn't fetch results for \(#function) - Id: \(id) - Error: \(error.localizedDescription)")
            }

            if saveContext.hasChanges {
                do {
                    try saveContext.save()
                    retVal = true
                } catch {
                    retVal = false
                    LogError("Couldn't save results for \(#function) - Id: \(id) - Error: \(error.localizedDescription)")
                }
            }
            
            return retVal
        }
        
        func deleteRoom(room: RoomObject) -> Bool {
            var retVal = false
            let deleteContext = context()

            do {
                let results = try deleteContext.fetch(self.request)
                if results.count > 0 {
                    for result in results as! [NSManagedObject] {
                        if let resultID = result.value(forKey: "id") as? String {
                            if resultID == room.id {
                                LogInfo("Room: \(room.dictionary()) is added to deletion queue")
                                deleteContext.delete(result)
                                retVal = true
                            }
                        }
                    }
                }
            } catch {
                LogError("Couldn't fetch results for \(#function) - Id: \(room.id) - Error: \(error.localizedDescription)")
            }
            if room.isOwner() { CloudService.Database.instance.hideRoom(room.id) }
            
            do {
                if (deleteContext.hasChanges) {
                    try deleteContext.save()
                    NotificationCenter.default.post(name: .LDBRMChanged, object: nil, userInfo: ["deleted" : room])
                    LogInfo("Deletion operation from LDB is completed successfuly")
                }
            } catch {
                LogError("Couldn't save deletion operation to LDB - Error: \(error.localizedDescription)")
            }
            return retVal
        }
        
        private func createRoom(from entity: NSManagedObject) -> RoomObject? {
            let coverImage = entity.value(forKey: "coverImage") as? String
            let storeInCloud = entity.value(forKey: "storeInCloud") as? Bool ?? false
            guard let id = entity.value(forKey: "id") as? String,
                let name = entity.value(forKey: "name") as? String,
                let creationDate = entity.value(forKey: "creationDate") as? Date,
                let owner = entity.value(forKey: "owner") as? String else { return nil }
            let obj = RoomObject(id: id, name: name, owner: owner, creationDate: creationDate, coverImage: coverImage, storeInCloud: storeInCloud)
            return obj
        }
        
    }
    
}

