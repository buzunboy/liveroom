//
//  AssetsManager.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 29.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import CoreData

extension LDBService {
    
    class AssetsManager: NSObject, LDBSub {
        
        static let sharedInstance = AssetsManager()
        var request: NSFetchRequest<NSFetchRequestResult>!
        
        private override init() {
            super.init()
            self.request = getEntityRequest(with: "Assets")
        }
        
        func getAsset(id: String) -> AssetObject? {
            var retVal: AssetObject?
            do {
                let results = try context().fetch(self.request)
                if results.count > 0 {
                    for result in results as! [NSManagedObject] {
                        if let resultID = result.value(forKey: "id") as? String {
                            if resultID == id {
                                retVal = self.createAsset(from: result)
                            }
                        }
                    }
                }
            } catch {
                LogError("Couldn't fetch results for \(#function) - Id: \(id) - Error: \(error.localizedDescription)")
            }
            return retVal
        }
        
        func saveAsset(_ asset: AssetObject) -> Bool {
            var retVal = false
            var isSavedBefore = false
            do {
                let results = try context().fetch(self.request)
                if results.count > 0 {
                    for result in results as! [NSManagedObject] {
                        if let resultID = result.value(forKey: "id") as? String {
                            if resultID == asset.id {
                                result.setValue(asset.name, forKey: "name")
                                result.setValue(asset.roomId, forKey: "roomId")
                                result.setValue(asset.creationDate, forKey: "creationDate")
                                result.setValue(asset.mediaId, forKey: "mediaId")
                                result.setValue(asset.type.rawValue, forKey: "type")
                                result.setValue(asset.location, forKey: "location")
                                isSavedBefore = true
                            }
                        }
                    }
                }
            } catch {
                retVal = false
                LogError("Couldn't fetch results for \(#function) - Id: \(asset.id ?? "nil") - Error: \(error.localizedDescription)")
            }
            
            if !isSavedBefore {
                let obj = NSEntityDescription.insertNewObject(forEntityName: "Assets", into: context())
                obj.setValue(asset.id, forKey: "id")
                obj.setValue(asset.name, forKey: "name")
                obj.setValue(asset.roomId, forKey: "roomId")
                obj.setValue(asset.mediaId, forKey: "mediaId")
                obj.setValue(asset.creationDate, forKey: "creationDate")
                obj.setValue(asset.type.rawValue, forKey: "type")
                obj.setValue(asset.location, forKey: "location")
            }
            
            if context().hasChanges {
                do {
                    try context().save()
                    if Utilities.shouldStoreInCloud {
                        if asset.assetType == .image {
                            if (asset.convertToImageAsset() != nil) { CloudService.Database.instance.saveImage((asset.convertToImageAsset())!, completion: nil) }
                        } else if asset.assetType == .video {
                            if (asset.convertToVideoAsset() != nil) { CloudService.Database.instance.saveVideo((asset.convertToVideoAsset())!, completion: nil) }
                        }
                    }
                    retVal = true
                } catch {
                    retVal = false
                    LogError("Couldn't save results for \(#function) - Id: \(asset.id ?? "nil") - Error: \(error.localizedDescription)")
                }
            }
            
            return retVal
        }
        
        private func createAsset(from entity: NSManagedObject) -> AssetObject? {
            guard let id = entity.value(forKey: "id") as? String,
                let name = entity.value(forKey: "name") as? String,
                let type = entity.value(forKey: "type") as? Int,
                let date = entity.value(forKey: "creationDate") as? Date,
                let roomId = entity.value(forKey: "roomId") as? String,
                let mediaId = entity.value(forKey: "mediaId") as? String,
                let location = entity.value(forKey: "location") as? String else { return nil }
            let obj = AssetObject(id: id, mediaId: mediaId, name: name, location: location, roomId: roomId, assetType: AssetType(rawValue: type)!, creationDate: date)
        
            return obj
        }
        
    }
    
}
