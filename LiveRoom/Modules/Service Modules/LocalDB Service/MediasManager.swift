//
//  MediasManager.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 30.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import CoreData

extension LDBService {
    
    class MediaManager: NSObject, LDBSub {
        
        static let sharedInstance = MediaManager()
        var request: NSFetchRequest<NSFetchRequestResult>!

        private override init() {
            super.init()
            self.request = getEntityRequest(with: "Medias")
        }
        
        func getMedia(id: String) -> MediaObject? {
            var retVal: MediaObject?
            do {
                let results = try context().fetch(self.request)
                if results.count > 0 {
                    for result in results as! [NSManagedObject] {
                        if let resultID = result.value(forKey: "id") as? String {
                            if resultID == id {
                                retVal = self.createMedia(from: result)
                            }
                        }
                    }
                }
            } catch {
                LogError("Couldn't fetch results for \(#function) - Id: \(id) - Error: \(error.localizedDescription)")
            }
            return retVal
        }
        
        func getMedias(forRoomId: String) -> [MediaObject]? {
            var retVal = [MediaObject]()
            do {
                let results = try context().fetch(self.request)
                if results.count > 0 {
                    for result in results as! [NSManagedObject] {
                        if let resultID = result.value(forKey: "roomId") as? String {
                            if resultID == forRoomId {
                                if let media = self.createMedia(from: result) {
                                    retVal.append(media)
                                }
                            }
                        }
                    }
                }
            } catch {
                LogError("Couldn't fetch results for \(#function) - Id: \(forRoomId) - Error: \(error.localizedDescription)")
            }
            return retVal
        }
        
        func saveMedia(_ media: MediaObject) -> Bool {
            var retVal = false
            var isSavedBefore = false
            do {
                let results = try context().fetch(self.request)
                if results.count > 0 {
                    for result in results as! [NSManagedObject] {
                        if let resultID = result.value(forKey: "id") as? String {
                            if resultID == media.id {
                                result.setValue(media.roomId, forKey: "roomId")
                                result.setValue(media.videoAssetId, forKey: "videoId")
                                result.setValue(media.imageAssetId, forKey: "imageId")
                                isSavedBefore = true
                            }
                        }
                    }
                }
            } catch {
                retVal = false
                LogError("Couldn't fetch results for \(#function) - Id: \(media.id ?? "nil") - Error: \(error.localizedDescription)")
            }
            
            if !isSavedBefore {
                let obj = NSEntityDescription.insertNewObject(forEntityName: "Medias", into: context())
                obj.setValue(media.id, forKey: "id")
                obj.setValue(media.roomId, forKey: "roomId")
                obj.setValue(media.videoAssetId, forKey: "videoId")
                obj.setValue(media.imageAssetId, forKey: "imageId")
            }
            
            if context().hasChanges {
                do {
                    try context().save()
                    if Utilities.shouldStoreInCloud {
                        CloudService.Database.instance.saveMedia(media)
                    }
                    retVal = true
                } catch {
                    retVal = false
                    LogError("Couldn't save results for \(#function) - Id: \(media.id ?? "nil") - Error: \(error.localizedDescription)")
                }
            }
            
            return retVal
        }
        
        func deleteMedia(_ media: MediaObject) -> Bool {
            var retVal = false
            do {
                let results = try context().fetch(self.request)
                if results.count > 0 {
                    for result in results as! [NSManagedObject] {
                        if let resultID = result.value(forKey: "id") as? String {
                            if resultID == media.id {
                                LogInfo("Media: \(media.dictionary()) is deleted from LDB")
                                context().delete(result)
                                retVal = true
                            }
                        }
                    }
                }
            } catch {
                LogError("Couldn't fetch results for \(#function) - Id: \(media.id) - Error: \(error.localizedDescription)")
            }
            CloudService.Database.instance.hideMedia(media)
            
            return retVal
        }
        
        private func createMedia(from entity: NSManagedObject) -> MediaObject? {
            let videoId = entity.value(forKey: "videoId") as? String
            let imageId = entity.value(forKey: "imageId") as? String
            guard let id = entity.value(forKey: "id") as? String,
                let roomId = entity.value(forKey: "roomId") as? String
                else { return nil }
            let obj = MediaObject(id: id, roomId: roomId, videoAssetId: videoId, imageAssetId: imageId)
            
            return obj
        }
    }
    
}

