//
//  LocalDBService.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 29.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import CoreData

class LDBService: NSObject {
    
    class func resetLDB() -> Bool {
        let assetResult = AssetsManager.sharedInstance.resetEntity()
        let mediaResult = MediaManager.sharedInstance.resetEntity()
        let roomResult = RoomsManager.sharedInstance.resetEntity()
        
        if (assetResult && mediaResult && roomResult) {
            LogInfo("Local DB is reset successfuly")
            return true
        } else if (!assetResult && !mediaResult && !roomResult) {
            LogError("Local DB entities not resetted")
            return false
        } else {
            LogError("Local DB entities are partially resetted")
            return false
        }
    }

}
