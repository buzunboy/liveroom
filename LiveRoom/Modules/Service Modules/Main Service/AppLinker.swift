//
//  AppLinker.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 24.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

extension MainService {
    
    public class AppLinker {
        
        private var url: URL!
        private var hostType: ObjectType!
        private var window: UIWindow?
        
        init(url: URL, mainWindow: UIWindow?) {
            self.url = url
            self.hostType = .unknown
            self.window = mainWindow
            LogInfo("Application Linker started - URL: \(self.url.path) - Host: \(self.hostType!) - ")
        }
        
        public func redirect() {
            self.createPage()
        }
        
        private func getHostVC() -> UIViewController? {
            if let host = self.url.host {
                if host == "rooms" {
                    self.hostType = .room
                    return UIViewController.ViewControllers.MainStoryboard.roomDetailVC()
                }
            }
            return nil
        }
        
        private func createPage() {
            if let vc = self.getHostVC() {
                if self.url.path.count > 1 {
                    if self.hostType == .room {
                        CloudService.Database.instance.getRoom(roomId: self.url.path) { (error, room) in
                            if let error = error {
                                LogError("Couldn't get room with id: \(self.url.path) - Error: \(error.localizedDescription)")
                                self.redirectToHome()
                            }
                            else if vc.initialize(with: room) {
                                self.redirectToVC(vc)
                            }
                        }
                    }
                }
            }
            LogInfo("Application Linker parsed - URL: \(self.url.path) - Host: \(self.hostType) - Path: \(self.url.path)")
        }
        
        private func redirectToHome() {
            
        }
        
        private func redirectToVC(_ vc: UIViewController) {
            LogInfo("Application will be redirected to detected VC")
            let nav = UINavigationController(rootViewController: vc)
            nav.navigationBar.tintColor = UIColor.orange
            UIViewController.topViewController()?.presentWithLink(navigationController: nav, viewControllerToPresent: vc, animated: true)
        }
        
    }
}
