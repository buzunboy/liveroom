//
//  VideoStorage.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 29.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

extension MainService {
    
    public class VideoStorage: NSObject {
        
        private var videoStorageName = "videos"
        
        init(folderName: String) {
            self.videoStorageName = folderName
        }
        
        public func getVideoFromStorage(name: String, handler: @escaping (_ video: Data?)->()){
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let filePath = documentsURL.appendingPathComponent("\(self.videoStorageName)/\(name)").path
            if FileManager.default.fileExists(atPath: filePath) {
                do {
                    let data = try Data.init(contentsOf: URL(fileURLWithPath: filePath))
                    handler(data)
                } catch {
                    LogError("Couldn't read data from the file URL: \(filePath) - Error: \(error.localizedDescription)")
                    handler(nil)
                }
            } else {
                LogError("Couldn't find file with file URL: \(filePath)")
                handler(nil)
            }
        }
        
        public func getVideoURLFromStorage(name: String) -> URL? {
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let filePath = documentsURL.appendingPathComponent("\(self.videoStorageName)/\(name)").appendingPathExtension("mp4").path
            
            if FileManager.default.fileExists(atPath: filePath) {
                return URL(fileURLWithPath: filePath)
            } else {
                return nil
            }
        }
        
        public func getVideoDataFromStorage(name: String) -> Data? {
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let filePath = documentsURL.appendingPathComponent("\(self.videoStorageName)/\(name)").path
            if FileManager.default.fileExists(atPath: filePath) {
                do {
                    return try Data(contentsOf: URL(fileURLWithPath: filePath))
                } catch {
                    return nil
                }
            } else {
                return nil
            }
        }
        
        public func saveVideoToStorage(videoURL: URL, name: String) -> URL {
            let videoData = NSData(contentsOf: videoURL)
            
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let folderURL = documentsURL.appendingPathComponent(self.videoStorageName)
            if !FileManager.default.fileExists(atPath: folderURL.absoluteString) {
                self.createDirectory(name: self.videoStorageName)
            }
            let fileURL = folderURL.appendingPathComponent(name).appendingPathExtension("mp4")
            videoData?.write(to: fileURL, atomically: true)
            return fileURL
        }
        
        private func createDirectory(name: String) {
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let directoryPath = documentsURL.appendingPathComponent(name)
            do {
                try FileManager.default.createDirectory(atPath: directoryPath.path, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                LogError("Unable to create directory - Error: \(error.localizedDescription)")
            }
        }
        
    }
}
