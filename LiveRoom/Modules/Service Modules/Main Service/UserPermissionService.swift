//
//  UserPermissionService.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 29.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import Photos

extension MainService {
    
    class UserPermissionService: NSObject {
        
        class func checkForPhotoLibrary(completion: @escaping (_ isAuthorized: Bool)->()) {
            let status = PHPhotoLibrary.authorizationStatus()
            switch status {
            case .authorized:
                completion(true)
            case .denied:
                completion(false)
            case .restricted:
                completion(false)
            case .notDetermined:
                requestPhotoAuth(completion: completion)
            }
            
        }
        
        class func requestPhotoAuth(completion: @escaping (_ isAuthorized: Bool)->()) {
            PHPhotoLibrary.requestAuthorization { (status) in
                switch status {
                case .authorized:
                    completion(true)
                case .notDetermined:
                    completion(false)
                case .restricted:
                    completion(false)
                case .denied:
                    completion(false)
                }
            }
        }
        
    }
    
}
