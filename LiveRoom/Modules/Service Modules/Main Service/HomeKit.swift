//
//  HomeKit.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 22.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import HomeKit

extension MainService {
    
    public class HomeKit: NSObject, HMHomeManagerDelegate {
        
        public static let instance = HomeKit()
        
        private var homeManager: HMHomeManager!
        public var accessories = Set<HMAccessory>()
        private var maxBrightness = 1.0
        
        private override init() {
            super.init()
            self.homeManager = HMHomeManager()
            self.homeManager.delegate = self
        }
        
        public func prepareLamps(with image: UIImage) {
            let colors = image.getColors()
            self.changeLampColors(with: colors.background)
        }
        
        private func getAllLamps() {
            guard let home = self.homeManager.primaryHome else { return }
            for room in home.rooms {
                for accessory in room.accessories {
                    for service in accessory.services {
                        for characteristic in service.characteristics {
                            if characteristic.characteristicType == HMCharacteristicTypeBrightness {
                                self.accessories.insert(accessory)
                            }
                        }
                    }
                }
            }
        }
        
        private func changeLampColors(with color: UIColor) {
            let hsbaColor = hsba(from: color)
            for accessory in self.accessories {
                for service in accessory.services {
                    for characteristic in service.characteristics {
                        if characteristic.characteristicType == HMCharacteristicTypeBrightness {
                            let max = characteristic.metadata?.maximumValue?.floatValue ?? 1.0
                            let value = NSNumber(value: Float(hsbaColor[2]) * max)
                            characteristic.writeValue(Int(max)) { (error) in
                                if let error = error {
                                    LogError("Couldn't set brightness to characteristic - Error: \(error.localizedDescription) - Value: \(max) - Max: \(max)")
                                    return
                                }
                                
                                LogInfo("Brightness value is changed successfully - Value: \(value) - Max: \(max)")
                            }
                        } else if characteristic.characteristicType == HMCharacteristicTypeHue {
                            let max = characteristic.metadata?.maximumValue?.floatValue ?? 1.0
                            let value = NSNumber(value: Float(hsbaColor[0]) * max)
                            characteristic.writeValue(value) { (error) in
                                if let error = error {
                                    LogError("Couldn't set hue to characteristic - Error: \(error.localizedDescription) - Value: \(value) - Max: \(max)")
                                    return
                                }
                                
                                LogInfo("Hue value is changed successfully - Value: \(value) - Max: \(max)")
                            }
                        } else if characteristic.characteristicType == HMCharacteristicTypeSaturation {
                            let max = characteristic.metadata?.maximumValue?.floatValue ?? 1.0
                            let value = NSNumber(value: Float(hsbaColor[1]) * max)
                            characteristic.writeValue(value) { (error) in
                                if let error = error {
                                    LogError("Couldn't set saturation to characteristic - Error: \(error.localizedDescription) - Value: \(value) - Max: \(max)")
                                    return
                                }
                                
                                LogInfo("Saturation value is changed successfully - Value: \(value) - Max: \(max)")
                            }
                        } else if characteristic.characteristicType == HMCharacteristicTypePowerState {
                            characteristic.writeValue(true) { (error) in
                                if let error = error {
                                    LogError("Couldn't set power to characteristic - Error: \(error.localizedDescription)")
                                    return
                                }
                                
                                LogInfo("Power value is changed successfully")
                            }
                        } else if characteristic.characteristicType == HMCharacteristicTypeCurrentLightLevel {
                            let max = characteristic.metadata?.maximumValue?.floatValue ?? 1.0
                            let value = NSNumber(value: 1 * max)
                            characteristic.writeValue(value) { (error) in
                                if let error = error {
                                    LogError("Couldn't set light level to characteristic - Error: \(error.localizedDescription) - Value: \(value) - Max: \(max)")
                                    return
                                }
                                
                                LogInfo("Light level value is changed successfully - Value: \(value) - Max: \(max)")
                            }
                        }
                    }
                }
            }
        }
        
        public func homeManagerDidUpdateHomes(_ manager: HMHomeManager) {
            self.getAllLamps()
        }
        
        public func homeManagerDidUpdatePrimaryHome(_ manager: HMHomeManager) {
            self.getAllLamps()
        }
        
        public func homeManager(_ manager: HMHomeManager, didAdd home: HMHome) {
            self.getAllLamps()
        }
        
        public func homeManager(_ manager: HMHomeManager, didRemove home: HMHome) {
            self.getAllLamps()
        }
        
        private func hsba(from color: UIColor) -> [CGFloat] {
            var HSBA = [CGFloat](repeating: 0.0, count: 4)
            var hue: CGFloat = 0.0
            var saturation: CGFloat = 0.0
            var brightness: CGFloat = 0.0
            var alpha: CGFloat = 0.0
            color.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)
        
            HSBA = [hue, saturation, brightness, alpha]
            return HSBA
        }
        
    }

}

