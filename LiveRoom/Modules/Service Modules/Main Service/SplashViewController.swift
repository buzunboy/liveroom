//
//  SplashViewController.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 29.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

extension MainService {
    
    class SplashViewController: NSObject {
        
        static let sharedInstance = SplashViewController()
        
        private var splashView: UIImageView!
        
        private override init() {
            super.init()
            self.splashView = UIImageView(frame: (UIApplication.shared.keyWindow?.rootViewController?.view.frame)!)
            self.splashView.image = UIImage(named: "default_background")
        }
        
        public func show() {
            UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(self.splashView)
        }
        
        public func hide() {
            UIView.animate(withDuration: 2, animations: {
                self.splashView.frame.origin.y = Utilities.instance.deviceHeight
            }) { (completed) in
                
            }
            
        }
    }
    
}
