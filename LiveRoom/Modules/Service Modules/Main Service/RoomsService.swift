//
//  RoomsService.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 5.08.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

extension MainService {
    
    
    class RoomsService: NSObject {
        
        static let sharedInstance = RoomsService()
        
        public var rooms = [RoomObject]() {
            didSet {
                if self.rooms != oldValue {
                    delegate?.didRoomsUpdated()
                }
            }
        }
        public var delegate: RoomsServiceProtocol?
        
        private override init() {
            super.init()
            NotificationCenter.default.addObserver(self, selector: #selector(roomsChanged(notification:)), name: .LDBRMChanged, object: nil)
            if let localRooms = LDBService.RoomsManager.sharedInstance.getAllRooms() {
                for room in localRooms {
                    if !self.rooms.contains(where: { $0.id == room.id }) {
                        self.rooms.append(room)
                    }
                }
            }
            self.connectRooms()
        }
        
        @objc private func roomsChanged(notification: Notification) {
            var copyRooms = self.rooms
            
            if let localRooms = LDBService.RoomsManager.sharedInstance.getAllRooms() {
                if let deletedRoom = notification.userInfo?["deleted"] as? RoomObject {
                    _ = self.rooms.contains(where: { (container) -> Bool in
                        if container.id == deletedRoom.id {
                            if let index = self.rooms.index(of: container) {
                                self.rooms.remove(at: index)
                                return true
                            }
                        }
                        return false
                    })
                    return
                }
                
                for room in localRooms {
                    if !copyRooms.contains(room) {
                        _ = copyRooms.contains { (container) -> Bool in
                            if container.id == room.id {
                                if let index = copyRooms.index(of: container) {
                                    copyRooms.remove(at: index)
                                    return true
                                }
                            }
                            return false
                        }
                        RoomsService.roomImage(roomId: room.id, completion: nil)
                        copyRooms.append(room)
                        self.connectRoom(room)
                    }
                }
            }
            self.rooms = copyRooms
        }
        
        private func connectRooms() {
            var copyRooms = self.rooms
            for room in copyRooms {
                CloudService.Database.instance.connectRoom(id: room.id) { (connectedRoom) in
                    if let connectedRoom = connectedRoom {
                        if !copyRooms.contains(room) {
                            _ = copyRooms.contains { (container) -> Bool in
                                if container.id == connectedRoom.id {
                                    if let index = copyRooms.index(of: container) {
                                        copyRooms.remove(at: index)
                                        return true
                                    }
                                }
                                return false
                            }
                            RoomsService.roomImage(roomId: connectedRoom.id, completion: nil)
                            copyRooms.append(connectedRoom)
                        } else {
                            return
                        }
                    }
                }
            }
            self.rooms = copyRooms
        }
        
        private func connectRoom(_ room: RoomObject) {
            var copyRooms = self.rooms
            CloudService.Database.instance.connectRoom(id: room.id) { (connectedRoom) in
                if let connectedRoom = connectedRoom {
                    if !copyRooms.contains(room) {
                        _ = copyRooms.contains { (container) -> Bool in
                            if container.id == connectedRoom.id {
                                if let index = copyRooms.index(of: container) {
                                    copyRooms.remove(at: index)
                                    return true
                                }
                            }
                            return false
                        }
                        RoomsService.roomImage(roomId: connectedRoom.id, completion: nil)
                        copyRooms.append(connectedRoom)
                    } else {
                        return
                    }
                }
            }
            self.rooms = copyRooms
        }
        
        class func room(id: String) {
            //CloudService.Database.instance.getRoom(roomId: <#T##String#>, completion: <#T##(NSError?, RoomObject?) -> ()#>)
        }
        
        class func saveRoomImage(image: UIImage, room: RoomObject) {
            MainService.ImageStore(folderName: "images").saveImageToStorage(with: image, name: room.id) { (error, url) in
                if let error = error {
                    LogError("Image couldn't be saved to storage with name: \(room.id) - Error: \(error.localizedDescription)")
                    return
                }
                CloudService.StorageService.instance.saveCoverImage(image, completion: { (error, imageId) in
                    if let error = error {
                        LogError("Image Couldn't be saved to cloud storage - Error: \(error.localizedDescription) - Room ID: \(room.id)")
                        return
                    }
                    room.coverImage = imageId
                    CloudService.Database.instance.saveRoom(room, completion: { (completed) in
                        if completed {
                            LogInfo("Image saved to the cloud storage with room id : \(room.id!)")
                            if LDBService.RoomsManager.sharedInstance.saveRoom(room) {
                            }
                        } else {
                            LogError("Image Id couldn't be saved to Cloud Storage - Room: \(room.dictionary())")
                        }
                    })
                    
                    
                })
            }
        }
        
        class func roomImage(roomId id: String) -> UIImage? {
            var retVal: UIImage?
            let localRoom = LDBService.RoomsManager.sharedInstance.getRoom(id: id)
            let imageStorage = MainService.ImageStore.init(folderName: "images")
            
            if let localRoom = localRoom, let localCoverURL = localRoom.coverImage {
                retVal = imageStorage.getImageImmediately(imageURL: localCoverURL)
            }
            
            return retVal
        }
        
        class func roomImage(roomId id: String, completion:  ((_ image: UIImage?)->())?) {
            DispatchQueue.global().async {
                let localRoom = LDBService.RoomsManager.sharedInstance.getRoom(id: id)
                let imageStorage = MainService.ImageStore.init(folderName: "images")
                
                if let localRoom = localRoom, let localCoverURL = localRoom.coverImage {
                    completion?(imageStorage.getImageImmediately(imageURL: localCoverURL))
                    imageStorage.getImageFromStorage(imageURL: localCoverURL, splashImage: nil, handler: { (loadedImage) in
                        DispatchQueue.main.async {
                            completion?(loadedImage)
                        }
                    })
                }
                
                CloudService.Database.instance.connectRoom(id: id, connection: { (cloudRoom) in
                    if let cloudRoom = cloudRoom {
                        if let localRoom = localRoom, localRoom.coverImage == cloudRoom.coverImage {
                            if let localURL = localRoom.coverImage {
                                imageStorage.getImageFromStorage(imageURL: localURL, splashImage: nil, handler: { (loadedImage) in
                                    DispatchQueue.main.async {
                                        completion?(loadedImage)
                                    }
                                })
                            }
                        } else {
                            
                            if let localRoom = localRoom, let coverURL = localRoom.coverImage {
                                imageStorage.deleteImage(imageURL: coverURL)
                            }
                            if let cloudImageURL = cloudRoom.coverImage {
                                imageStorage.getImageFromStorage(imageURL: cloudImageURL, splashImage: nil, handler: { (loadedImage) in
                                    DispatchQueue.main.async {
                                        completion?(loadedImage)
                                    }
                                })
                            } else {
                                DispatchQueue.main.async {
                                    completion?(nil)
                                }
                            }
                        }
                    } else {
//                        if let localRoom = localRoom, let localCoverURL = localRoom.coverImage {
//                            imageStorage.getImageFromStorage(imageURL: localCoverURL, splashImage: nil, handler: { (loadedImage) in
//                                DispatchQueue.main.async {
//                                    completion?(loadedImage)
//                                }
//                            })
//                        } else {
//                            DispatchQueue.main.async {
//                                completion?(nil)
//                            }
//                        }
                    }
                })
            }
        }
        
    }
    
    
}


