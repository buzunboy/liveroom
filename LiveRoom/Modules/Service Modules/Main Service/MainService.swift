//
//  MainService.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 29.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import Foundation

class MainService: NSObject {
    
    class func sync(completion: @escaping ()->()) {
        MainService.syncRooms {
            completion()
        }
    }
    
    private class func syncRooms(completion: @escaping ()->()) {
        guard let rooms = LDBService.RoomsManager.sharedInstance.getAllRooms() else { completion(); return }
        for localRoom in rooms {
            CloudService.Database.instance.getRoom(roomId: localRoom.id) { (error, cloudRoom) in
                if let cloudRoom = cloudRoom {
                    let imageStorage = MainService.ImageStore.init(folderName: "images")

                    if localRoom.coverImage == cloudRoom.coverImage {
                        if let localURL = localRoom.coverImage {
                            imageStorage.getImageFromStorage(imageURL: localURL, splashImage: nil, handler: { (loadedImage) in })
                        }
                    } else {
                        
                        if let coverURL = localRoom.coverImage {
                            imageStorage.deleteImage(imageURL: coverURL)
                        }
                        if let cloudImageURL = cloudRoom.coverImage {
                            imageStorage.getImageFromStorage(imageURL: cloudImageURL, splashImage: nil, handler: { (loadedImage) in })
                        }
                    }
                } else {
                    
                }
            }
        }
        completion()
        
    }

    
}
