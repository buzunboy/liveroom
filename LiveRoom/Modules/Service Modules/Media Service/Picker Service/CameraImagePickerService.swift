//
//  CameraImagePickerService.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 1.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

extension MediaService {
    
    class CameraImagePickerService: GenericPickerService {
        
        init(delegate: UIViewController) {
            if (delegate as? ImageServiceDelegate) != nil {
                super.init(delegate: delegate, with: .cameraPhoto)
            } else {
                fatalError("Delegate should conform protocol for ImageServiceDelegate")
            }
        }
        
    }
    
}
