//
//  ImagePickerService.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 28.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import MobileCoreServices

extension MediaService {
    
    internal class GenericPickerService: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
        
        public private(set) var delegate: UIViewController!
        private var type: MediaPickerType!
        private var pickerType: UIImagePickerControllerSourceType!
        private var pickerController: UIImagePickerController!
        
        init(delegate: UIViewController!, with type: MediaPickerType) {
            self.delegate = delegate
            self.type = type
            if (type == .photo || type == .video) {
                self.pickerType = .photoLibrary
            } else {
                self.pickerType = .camera
            }
            super.init()
            
            pickerController = UIImagePickerController()
            pickerController.delegate = self
            pickerController.sourceType = pickerType
            pickerController.allowsEditing = false
            if self.type == .video {
                pickerController.mediaTypes = [kUTTypeMovie as String, kUTTypeVideo as String]
            }
        }

        func show() {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.delegate.present(pickerController, animated: true) {
                    LogInfo("UIImagePickerController is presented with type: \(String(describing: self.type)) and picker type: \(String(describing: self.pickerType))")
                }
            }
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
            if (self.type == .photo || self.type == .cameraPhoto) {
                if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                    if let delegate = self.delegate as? ImageServiceDelegate {
                        delegate.didImageChosen(image)
                    }
                } else{
                    if let delegate = self.delegate as? ImageServiceDelegate {
                        delegate.didImageChosen(nil)
                    }
                }
            } else if self.type == .video {
                if let videoUrl = info[UIImagePickerControllerMediaURL] as? NSURL{
                    let data = NSData(contentsOf: videoUrl as URL)!
                    if let delegate = self.delegate as? VideoServiceDelegate {
                        delegate.didVideoChosen(videoUrl)
                    }
                    print("File size before compression: \(Double(data.length / 1048576)) mb")
                }
                else{
                    if let delegate = self.delegate as? VideoServiceDelegate {
                        delegate.didVideoChosen(nil)
                    }
                }
            }
            
            picker.dismiss(animated: true, completion: nil)
        }
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            if self.type == .photo || self.type == .cameraPhoto {
                if let delegate = self.delegate as? ImageServiceDelegate {
                    delegate.didImageChosen(nil)
                }
            } else if self.type == .video {
                if let delegate = self.delegate as? VideoServiceDelegate {
                    delegate.didVideoChosen(nil)
                }
            }
            
            picker.dismiss(animated: true, completion: nil)
        }
        
    }
    
}


@objc protocol ImageServiceDelegate: NSObjectProtocol {
    func didImageChosen(_ image: UIImage?)
}

@objc protocol VideoServiceDelegate: NSObjectProtocol {
    func didVideoChosen(_ video: NSURL?)
}
