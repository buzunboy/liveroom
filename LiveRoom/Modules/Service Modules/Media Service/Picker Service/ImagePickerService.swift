//
//  ImagePickerService.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 28.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

extension MediaService {
    
    class ImagePickerService: GenericPickerService {
        
        init(delegate: UIViewController) {
            if (delegate as? ImageServiceDelegate) != nil {
                super.init(delegate: delegate, with: .photo)
            } else {
                fatalError("Delegate should conform protocol for ImageServiceDelegate")
            }
        }
        
    }
    
}
