//
//  VideoPickerService.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 29.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

extension MediaService {
    
    class VideoPickerService: GenericPickerService {
        
        init(delegate: UIViewController) {
            if (delegate as? VideoServiceDelegate) != nil {
                super.init(delegate: delegate, with: .video)
            } else {
                fatalError("Delegate should conform protocol for VideoServiceDelegate")
            }
        }
        
    }
    
}
