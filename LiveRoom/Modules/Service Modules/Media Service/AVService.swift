//
//  AVService.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 29.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import AVKit

extension MediaService {
    
    class AVService: NSObject {
        
        static func getPlayerController(for name: String) -> AVPlayerViewController? {
            if let url = MainService.VideoStorage(folderName: "videos").getVideoURLFromStorage(name: name) {
                let player = AVPlayer(url: url)
                
                let playerController = AVPlayerViewController()
                playerController.player = player
                return playerController
            } else {
                return nil
            }
        }
        
        static func getImageFromVideo(player: AVPlayer) -> UIImage? {
            if let asset = player.currentItem?.asset {
                let generator = AVAssetImageGenerator(asset: asset)
                let timer = CMTime(seconds: 1, preferredTimescale: 1)
                let imageRef = try! generator.copyCGImage(at: timer, actualTime: nil)
                let thumbnail = UIImage(cgImage: imageRef)
                return thumbnail
            } else {
                return nil
            }
        }
        
    }
    
}
