//
//  AccountManagerDelegate.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 15.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

public protocol AccountManagerDelegate: NSObjectProtocol {
    
    func loginStatusChanged(status: LoginStatus)
    
}
