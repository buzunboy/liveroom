//
//  StartScrollerViewController.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 29.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

protocol StartScrollerDelegate: NSObjectProtocol {
    func didRatioUpdated(ratio: CGFloat)
    func didAccepted()
}

class StartScrollerViewController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var scrollableView: RadialUIView!
    @IBOutlet weak var backVisualEffectView: RadialVisualEffectView!
    @IBOutlet weak var centerLabel: UILabel!
    @IBOutlet weak var switchLabel: UILabel!
    @IBOutlet weak var switchBtn: UISwitch!
    
    var panRecognizer: UIPanGestureRecognizer!
    var minimumPointX: CGFloat!
    var maximumPointX: CGFloat!
    var delegate: Any?
    var selectedRoom: RoomObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(swiped(gesture:)))
        panRecognizer.delegate = self
        self.view.addGestureRecognizer(panRecognizer)
        self.maximumPointX = self.backVisualEffectView.frame.width-40
        self.minimumPointX = 46
        self.scrollableView.center = CGPoint(x: minimumPointX, y: self.scrollableView.center.y)
        self.switchLabel.text = "room_store_cloud".localized()
        self.switchLabel.textColor = UIColor.white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let delegate = (delegate as? RoomDetailViewController) else { return }
        switchBtn.setOn(delegate.selectedRoom.shouldStoreInCloud, animated: true)
    }
    
    @objc func swiped(gesture: UIPanGestureRecognizer) {
        let location = gesture.location(in: self.backVisualEffectView).x
        // let isUpwardDirection = (gesture.velocity(in: self.view).y < 0) ? true : false
        if let delegate = self.delegate as? StartScrollerDelegate {
            var ratio = (location-minimumPointX)/(maximumPointX-minimumPointX)
            ratio = (ratio < 0 ) ? 0 : ratio
            self.centerLabel.alpha = 1-2*ratio
            delegate.didRatioUpdated(ratio: ratio)
        }
        UIView.animate(withDuration: 0.1) {
            if location < self.backVisualEffectView.frame.width-20 && location > self.minimumPointX {
                self.scrollableView.center = CGPoint(x: location, y: self.scrollableView.center.y)
            }
        }
        
        if gesture.state == .ended {
            if location > self.backVisualEffectView.frame.width - 50 {
                if let delegate = self.delegate as? StartScrollerDelegate {
                    delegate.didAccepted()
                    delegate.didRatioUpdated(ratio: 1)
                    self.centerLabel.alpha = 0
                }
                UIView.animate(withDuration: 0.1) {
                    self.scrollableView.center = CGPoint(x: self.backVisualEffectView.frame.width-36, y: self.scrollableView.center.y)
                }
            } else {
                UIView.animate(withDuration: 0.5) {
                    if let delegate = self.delegate as? StartScrollerDelegate {
                        delegate.didRatioUpdated(ratio: 0)
                        self.centerLabel.alpha = 1
                    }
                    self.scrollableView.center = CGPoint(x: self.minimumPointX, y: self.scrollableView.center.y)
                }
            }
        }
    }

    
    @IBAction func switchBtnClicked(_ sender: Any) {
        guard let delegate = (delegate as? RoomDetailViewController) else { return }
        delegate.selectedRoom.shouldStoreInCloud = switchBtn.isOn
        switchBtn.isOn = delegate.selectedRoom.shouldStoreInCloud
    }
    
    func changeBaseColor(_ color: UIColor) {
        self.switchLabel.textColor = color
        self.switchBtn.onTintColor = color
    }
    
}
