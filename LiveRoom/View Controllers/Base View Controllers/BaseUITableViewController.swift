//
//  BaseUITableViewController.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 28.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class BaseUITableViewController: UITableViewController, NavigationBarProtocol {
    
    var navigationBarAppearanceLevel: CGFloat = 0.0
    var isNavigationBarTransparent: Bool = false
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    public private(set) var baseTintColor: UIColor = UIColor.orange {
        didSet {
            self.didBaseTintColorChange()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        NavigationBarController.sharedInstance.delegate = self
        LogVerbose("\(className) view did load")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NavigationBarController.sharedInstance.delegate = self
        NavigationBarController.sharedInstance.viewWillAppeared()
        self.changeBaseTintColor(self.baseTintColor)
        UIDevice.lockOrientation(.portrait, andRotateTo: .portrait)
        LogVerbose("\(className) view will appear")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NavigationBarController.sharedInstance.viewDidAppeared()
        LogVerbose("\(className) view did appear")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        LogVerbose("\(className) view did disappear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        LogVerbose("\(className) view will disappear")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        LogMemoryWarning("\(className) MEMORY WARNING")
    }
    
    public final func changeBaseTintColor(_ color: UIColor) {
        self.baseTintColor = color
        self.navigationController?.navigationBar.tintColor = self.baseTintColor
        UIApplication.shared.statusBarStyle = (self.baseTintColor.isBlackish()) ? .default : .lightContent
        if self.baseTintColor != color {
            self.didBaseTintColorChange()
            LogVerbose("Base tint color changed to: \(color.description) in \(className) ")
        }
    }
    
    public func didBaseTintColorChange() {
        
    }

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.navigationBarAppearanceLevel == 0.0 {
            NavigationBarController.sharedInstance.delegate = self
            NavigationBarController.sharedInstance.scrollViewScrolled(offset: 0)
            NavigationBarController.sharedInstance.viewDidAppeared()
        } else {
            NavigationBarController.sharedInstance.delegate = self
            NavigationBarController.sharedInstance.scrollViewScrolled(offset: self.tableView.contentOffset.y)
        }
    }
}
