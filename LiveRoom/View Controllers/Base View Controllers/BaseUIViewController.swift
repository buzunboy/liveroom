//
//  BaseUIViewController.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 28.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class BaseUIViewController: UIViewController, NavigationBarProtocol {
    
    var isNavigationBarTransparent: Bool = true
    var navigationBarAppearanceLevel: CGFloat = 0.0
    
    public private(set) var isLinked = false {
        didSet {
            self.linkageChanged()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    public private(set) var baseTintColor: UIColor = UIColor.orange {
        didSet {
            if self.baseTintColor != oldValue {
                self.didBaseTintColorChange()
                LogVerbose("Base tint color changed to: \(self.baseTintColor.description) in \(className) ")
            }
        }
    }

    override func viewDidLoad() {
        NavigationBarController.sharedInstance.delegate = self
        super.viewDidLoad()
        LogVerbose("\(className) view did load")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NavigationBarController.sharedInstance.delegate = self
        if self.responds(to: #selector(getter: UITableViewController.tableView)) {
            let tableView = self.value(forKey: "tableView") as? UITableView
            NavigationBarController.sharedInstance.viewWillAppeared(currentPos: tableView?.contentOffset.y)
        } else {
            NavigationBarController.sharedInstance.viewWillAppeared()
        }
        self.changeBaseTintColor(self.baseTintColor)
        LogVerbose("\(className) view will appear")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.linkChange()
        if self.responds(to: #selector(getter: UITableViewController.tableView)) {
            let tableView = self.value(forKey: "tableView") as? UITableView
            NavigationBarController.sharedInstance.viewDidAppeared(currentPos: tableView?.contentOffset.y)
        } else {
            NavigationBarController.sharedInstance.viewDidAppeared()
        }
        UIDevice.lockOrientation(.portrait, andRotateTo: .portrait)
        LogVerbose("\(className) view did appear")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        LogVerbose("\(className) view did disappear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        LogVerbose("\(className) view will disappear")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        LogMemoryWarning("\(className) MEMORY WARNING")
    }
    
    override func linked() {
        self.isLinked = true
        self.linkChange()
    }
    
    private func linkChange() {
        if isLinked {
            let cancelBtn = UIBarButtonItem(image: UIImage(named: "icon_cancel"), style: .done, target: self, action: #selector(cancelSelected))
            self.navigationItem.setLeftBarButton(cancelBtn, animated: true)
        }
    }
    
    @objc func cancelSelected() {
        self.dismiss(animated: true) {
            LogVerbose("\(self.className) dismissed")
        }
    }
    
    public func linkageChanged() {
        
    }
    
    public final func changeBaseTintColor(_ color: UIColor) {
        self.navigationController?.navigationBar.tintColor = color
        //UIApplication.shared.statusBarStyle = (color.isBlackish()) ? .default : .lightContent
        self.baseTintColor = color
    }
    
    public func didBaseTintColorChange() {
        
    }


}

