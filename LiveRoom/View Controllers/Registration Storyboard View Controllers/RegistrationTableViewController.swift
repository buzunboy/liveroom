//
//  RegistrationTableViewController.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 3.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

@objc protocol RegistrationProtocol: NSObjectProtocol {
    @objc func pop()
}

class RegistrationTableViewController: BaseUITableViewController {

    @IBOutlet weak var emailTF: LoginTextField!
    @IBOutlet weak var passwordTF: LoginTextField!
    @IBOutlet weak var rePasswordTF: LoginTextField!
    @IBOutlet weak var aggreementSwitch: UISwitch!
    @IBOutlet weak var aggreementLabel: UILabel!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var registerBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.editLocalizables()
    }
    @IBAction func registerClicked(_ sender: Any) {
        self.startRegistration()
    }
    
    override func editLocalizables() {
        self.titleLabel.textColor = UIColor.white
        self.titleLabel.text = "register_createAccount".localized()
        self.emailTF.placeholder = "register_email".localized()
        self.passwordTF.placeholder = "register_password".localized()
        self.rePasswordTF.placeholder = "register_rePassword".localized()
        self.aggreementLabel.text = "register_acceptance".localized()
        self.registerBtn.setTitle("register_register".localized(), for: .normal)
    }
    
    func startRegistration() {
        if aggreementSwitch.isOn {
            self.aggreementLabel.textColor = UIColor.white
            self.registerUser { (error) in
                if let error = error {
                    LogError("Couldn't register - Error: \(error.localizedDescription)")
                    self.errorLabel.text = error.localizedDescription
                    return
                }
                self.goStart()
            }
        } else {
            self.aggreementLabel.textColor = UIColor.red
            self.errorLabel.text = "registration_error_aggreement".localized()
        }
        
    }
    
    func goStart() {
        if let superVC = self.parent as? RegistrationProtocol {
            superVC.pop()
        }
    }
    
    func registerUser(completion: @escaping (_ error: NSError?)->()) {
        let err = NSError(domain: ErrorDomains.localError.rawValue, code: ErrorCodes.taskError.rawValue, description: "All fields should be filled correctly")
        let result = self.controlCredentials()
        guard let email = result.0,
            let password = result.1 else {
                completion(err)
                return
                
        }
        
        CloudService.AccountManager.instance.register(email: email, password: password) { (error) in
            if let error = error {
                completion(error)
                return
            }
            
            completion(nil)
        }
        
    }
    
    private func controlCredentials() -> (String?, String?) {
        if let password = self.checkPassword() {
            if let email = self.checkEmail() {
                return (email, password)
            }
        }
        
        return (nil, nil)
    }
    
    private func checkEmail() -> String? {
        guard let email = emailTF.text else {
            self.emailTF.showError()
            self.errorLabel.text = "registration_error_email_notValid".localized()
            return nil
        }
        
        self.emailTF.showNormal()
        return email
        
//        if Utilities.isValidEmail(email) {
//            self.emailTF.showNormal()
//            return email
//        } else {
//            self.emailTF.showError()
//            return nil
//        }
    }
    
    private func checkPassword() -> String? {
        guard let password = passwordTF.text, let rePassword = rePasswordTF.text else {
            self.passwordTF.showError()
            self.rePasswordTF.showError()
            self.errorLabel.text = "registration_error_password_match".localized()
            return nil
        }
        if password != rePassword {
            self.passwordTF.showError()
            self.rePasswordTF.showError()
            self.errorLabel.text = "registration_error_password_match".localized()
            return nil
        }
        
        self.passwordTF.showNormal()
        self.rePasswordTF.showNormal()
        self.errorLabel.text = nil
        return password
//
//        if Utilities.isValidPassword(password) {
//            self.passwordTF.showNormal()
//            self.rePasswordTF.showNormal()
//            return password
//        } else {
//            self.passwordTF.showError()
//            self.rePasswordTF.showError()
//            return nil
//        }
    }



}
