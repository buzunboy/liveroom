//
//  LoginViewController.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 3.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import AVKit

class LoginViewController: BaseUIViewController, RegistrationProtocol {

    func pop() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func goBackClicked(_ sender: Any) {
        self.pop()
    }
    
    var player: AVPlayer?
    var playerLayer: AVPlayerLayer!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setVideoPlayerLayer()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        player = nil
    }
    
    func setVideoPlayerLayer() {
        let videoPath = Bundle.main.url(forResource: "startVideo", withExtension: "mp4")
        player = AVPlayer.init(url: videoPath!)
        playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer.frame = self.view.frame
        playerLayer.opacity = 0.4
        player?.actionAtItemEnd = .none
        player?.isMuted = true
        player?.play()
        self.view.layer.insertSublayer(playerLayer, at: 0)
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem, queue: nil, using: { (_) in
            DispatchQueue.main.async {
                self.player?.seek(to: kCMTimeZero)
                self.player?.play()
            }
        })
    }
    
}
