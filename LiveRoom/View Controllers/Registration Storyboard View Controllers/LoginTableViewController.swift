//
//  LoginTableViewController.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 3.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class LoginTableViewController: BaseUITableViewController {

    @IBOutlet weak var credentialTF: LoginTextField!
    @IBOutlet weak var passwordTF: LoginTextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var loginBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.editLocalizables()
    }
    
    @IBAction func loginClicked(_ sender: Any) {
        self.tryLogin()
    }
    
    func tryLogin() {
        guard let credential = self.checkCredential(),
            let password = self.checkPassword() else { return }
        
        CloudService.AccountManager.instance.login(credential: credential, password: password) { (error) in
            if let error = error {
                LogError("Couldn't login - Error: \(error.localizedDescription)")
                self.errorLabel.text = error.localizedDescription
                return
            }
            
            self.loginSucceeded()
        }
    }
    
    func loginSucceeded() {
        CloudService.AccountManager.redirectToMain()
    }
    
    private func checkCredential() -> String? {
        guard let credential = credentialTF.text else {
            self.credentialTF.showError()
            self.errorLabel.text = "login_error_email".localized()
            return nil
        }
        if Utilities.isValidEmail(credential) {
            self.credentialTF.showNormal()
            self.errorLabel.text = nil
            return credential
        } else {
            self.credentialTF.showError()
            self.errorLabel.text = "login_error_email".localized()
            return nil
        }
    }
    
    private func checkPassword() -> String? {
        guard let password = passwordTF.text else {
            self.passwordTF.showError()
            self.errorLabel.text = "login_error_password".localized()
            return nil
        }
        
        self.passwordTF.showNormal()
        self.errorLabel.text = nil
        return password
//        if Utilities.isValidPassword(password) {
//            self.passwordTF.showNormal()
//            return password
//        } else {
//            self.passwordTF.showError()
//            return nil
//        }
    }
    
    override func editLocalizables() {
        self.titleLabel.textColor = UIColor.white
        self.titleLabel.text = "login_loginToAccount".localized()
        self.credentialTF.placeholder = "login_emailUsername".localized()
        self.passwordTF.placeholder = "login_password".localized()
        self.loginBtn.setTitle("login_login".localized(), for: .normal)
    }
  
}
