//
//  StartViewController.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 3.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import AVKit
import Lottie

class StartViewController: BaseUIViewController, AccountManagerDelegate {

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var registrationButtonsView: UIView!
    @IBOutlet var loadingIndicator: LRLoadingIndicator!
    
    @IBOutlet weak var fbLoginBtn: UIButton!
    @IBOutlet weak var signupBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var goAnonymBtn: UIButton!
    
    private var animationView: LOTAnimationView?
    
    var shouldShowButtons = false
    var isViewShown = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        animationView = LOTAnimationView(name: "welcomeAnimation")
        animationView?.frame = self.view.frame
        animationView?.animationSpeed = 0.5
        animationView?.contentMode = .scaleAspectFill
        self.view.addSubview(animationView!)
        animationView!.pause()
        
        self.setVideoPlayerLayer()
        self.editLocalizables()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        player = nil
        self.isViewShown = false
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Timer.scheduledTimer(withTimeInterval: 3, repeats: false) { (timer) in
            self.loginStatusChanged(status: .initial)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //self.loadingIndicator = LRLoadingIndicator(maxRadii: 50)
        self.loadingIndicator.startAnimating()
        self.registrationButtonsView.isHidden = true
        self.checkAuthentication()
        self.isViewShown = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    func checkAuthentication() {
        let hasLaunchBefore = UserDefaults.standard.hasLaunchBefore
        CloudService.AccountManager.instance.start(withDelegate: self)
        if !hasLaunchBefore {
            let vc = UIViewController.ViewControllers.FirstLaunchStoryboard.mainVC()
            self.present(vc, animated: true, completion: nil)
        } else {
            if self.shouldShowButtons {
                self.showButtons()
                //self.loadingIndicator.stopAnimating()
            }
        }
    }
    
    func showButtons() {
        self.registrationButtonsView.isHidden = false
    }

    @IBAction func goAnonym(_ sender: Any) {
        CloudService.AccountManager.instance.registerAnonym()
    }
    
    @IBAction func fbLoginClicked(_ sender: Any) {
        CloudService.FacebookLoginManager(viewController: self).login()
    }
    
    @IBAction func signupClicked(_ sender: Any) {
        let vc = UIViewController.ViewControllers.RegistrationStoryboard.registrationVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func loginClicked(_ sender: Any) {
        let vc = UIViewController.ViewControllers.RegistrationStoryboard.loginVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func editLocalizables() {
        self.goAnonymBtn.setTitle("start_skipNow".localized(), for: UIControlState.normal)
        self.fbLoginBtn.setTitle("start_fbLogin".localized(), for: .normal)
        self.signupBtn.setTitle("start_signUp".localized(), for: .normal)
        self.loginBtn.setTitle("start_login".localized(), for: .normal)
    }
    
    // MARK: - Account Manager Delegate
    
    func loginStatusChanged(status: LoginStatus) {
        self.shouldShowButtons = true
        //self.loadingIndicator.stopAnimating()
        self.showButtons()
        self.animationView?.play(completion: { (completed) in
            if completed { self.animationView?.removeFromSuperview() }
        })
    }
    
    var player: AVPlayer?
    var playerLayer: AVPlayerLayer!
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setVideoPlayerLayer() {
        let videoPath = Bundle.main.url(forResource: "startVideo", withExtension: "mp4")
        player = AVPlayer.init(url: videoPath!)
        playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer.frame = self.view.frame
        playerLayer.opacity = 1.0
        player?.actionAtItemEnd = .none
        player?.isMuted = true
        player?.play()
        self.view.layer.insertSublayer(playerLayer, at: 0)
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem, queue: nil, using: { (_) in
            DispatchQueue.main.async {
                self.player?.seek(to: kCMTimeZero)
                self.player?.play()
            }
        })
    }
    
}
