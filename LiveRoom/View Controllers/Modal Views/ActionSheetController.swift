//
//  ActionSheetController.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 30.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class ActionSheetController: NSObject {
    
    static func actionSheet(title: String, message: String? = nil, actions: LRAlertAction...) {
        smallBackVC(blurBackView: false)
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        for action in actions {
            let newAction = UIAlertAction(title: action.title, style: action.style) { (actionHandler) in
                enlargeVC(shouldDoImmediate: false)
                action.completionHandler(actionHandler)
            }
            alertVC.addAction(newAction)
        }
        let cancelAct = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            LogInfo("Cancel Action is clicked for: \(#function)")
            enlargeVC()
        }
        alertVC.addAction(cancelAct)
        UIViewController.topViewController()?.present(alertVC, animated: true, completion: {
            LogInfo("Action sheet is opened - Title: \(title) - Message: \(message ?? "nil")")
        })
    }
    
    static func activityController(activityItems: [Any], applicationActivities: [UIActivity]? = nil, completion: (()->())? = nil) {
        smallBackVC(blurBackView: false)
        let actionSheetVC = UIActivityViewController(activityItems: activityItems, applicationActivities: applicationActivities)
        actionSheetVC.completionWithItemsHandler = { (type, completed, activities, error) in
            LogInfo("Activity Sheet is closed - Items: \(activityItems.description) - Activities: \(applicationActivities?.description ?? "nil")")
            enlargeVC()
        }
        UIViewController.topViewController()?.present(actionSheetVC, animated: true, completion: {
            LogInfo("Activity Sheet is opened - Items: \(activityItems.description) - Activities: \(applicationActivities?.description ?? "nil")")
            completion?()
        })
    }
    
    static func alert(title: String, message: String? = nil, actions: LRAlertAction...) {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for action in actions {
            let newAction = UIAlertAction(title: action.title, style: action.style) { (actionHandler) in
                action.completionHandler(actionHandler)
            }
            alertVC.addAction(newAction)
        }
        UIViewController.topViewController()?.present(alertVC, animated: true, completion: {
            LogInfo("Alert is opened - Title: \(title) - Message: \(message ?? "nil")")
        })
    }
    
    static func anonymAlert() {
        let registerAct = LRAlertAction(title: "room_alert_action_anonym_goRegister".localized(), style: .default) { (action) in
            CloudService.AccountManager.redirectToRegistration()
        }
        let cancelAct = LRAlertAction(title: "action_cancel".localized(), style: .cancel) { (action) in
        }
        ActionSheetController.alert(title: "room_alert_anonym_title".localized(), message: "room_alert_anonym_subtitle".localized(), actions: cancelAct, registerAct)
    }
    
    private static func smallBackVC(blurBackView: Bool = true) {
        UIView.animate(withDuration: 0.5, animations: {
            if UIApplication.shared.keyWindow?.rootViewController != nil && blurBackView {
                let blurView = UIVisualEffectView(frame: (UIApplication.shared.keyWindow?.rootViewController!.view.frame)!)
                blurView.effect = UIBlurEffect(style: .light)
                blurView.tag = 3638
                UIViewController.topViewController()?.view.addSubview(blurView)
            }
            
            UIApplication.shared.keyWindow?.rootViewController?.view.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            UIApplication.shared.keyWindow?.rootViewController?.view.layer.cornerRadius = 20
            UIApplication.shared.keyWindow?.rootViewController?.view.clipsToBounds = true
            
        }) { (completed) in
            if completed { LogInfo("View Controller scaled to 90%%")}
        }
    }
    
    private static func enlargeVC(shouldDoImmediate: Bool = false) {
        if shouldDoImmediate {
            UIApplication.shared.keyWindow?.rootViewController?.view.layer.cornerRadius = 0
            UIApplication.shared.keyWindow?.rootViewController?.view.transform = CGAffineTransform(scaleX: 1, y: 1)
            for view in (UIApplication.shared.keyWindow?.rootViewController!.view.subviews)! {
                if view.tag == 3638 { view.removeFromSuperview() }
            }
            LogInfo("View Controller scaled to 100%%")
        } else {
            UIView.animate(withDuration: 0.5, animations: {
                UIApplication.shared.keyWindow?.rootViewController?.view.layer.cornerRadius = 0
                UIApplication.shared.keyWindow?.rootViewController?.view.transform = CGAffineTransform(scaleX: 1, y: 1)
                for view in (UIApplication.shared.keyWindow?.rootViewController!.view.subviews)! {
                    if view.tag == 3638 { view.alpha = 0.0 }
                }
            }) { (completed) in
                for view in (UIApplication.shared.keyWindow?.rootViewController!.view.subviews)! {
                    if view.tag == 3638 { view.removeFromSuperview() }
                }
                if completed { LogInfo("View Controller scaled to 100%% with animation")}
            }
        }
        
    }
    
    
}
