//
//  ImagePreviewViewController.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 19.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class ImagePreviewViewController: BaseUIViewController {
    
    @IBOutlet weak var imageScrollerView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    
    private var loadedImage: UIImage!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareUI()
    }
    
    private func prepareUI() {
        self.imageView.image = self.loadedImage
        self.imageScrollerView.delegate = self
        self.imageScrollerView.minimumZoomScale = 1.0
        self.imageScrollerView.maximumZoomScale = 6.0
        self.imageView.contentMode = .scaleAspectFit
        let editBtn = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(showEditVC))
        self.navigationItem.rightBarButtonItem = editBtn
    }
    
    override func initialize(with object: NSObject?) -> Bool {
        if let image = object as? UIImage {
            self.loadedImage = image
            return true
        } else {
            return false
        }
    }
    
    @objc func showEditVC() {
        let vc = CropViewController(image: self.loadedImage)
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

extension ImagePreviewViewController: UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
    
}

extension ImagePreviewViewController: CropViewControllerDelegate {
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.imageView.image = image
    }
    
}
