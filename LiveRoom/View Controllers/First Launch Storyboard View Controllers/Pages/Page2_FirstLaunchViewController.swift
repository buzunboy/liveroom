//
//  Page2_FirstLaunchViewController.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 7.08.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import Lottie

class Page2_FirstLaunchViewController: BaseUIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var fullView: UIView!
    @IBOutlet weak var imageBackView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var subLabel: UILabel!
    
    // MARK: - View Functions
    
    override func viewDidLoad() {
        self.fullView.backgroundColor = .clear
        super.viewDidLoad()
        self.setLabelTexts()
        self.changeBaseTintColor(UIColor.white)
    }
    
    override func viewDidLayoutSubviews() {
        
    }
    
    override func viewWillLayoutSubviews() {
        self.animateUI()
        self.appendSmallIcons()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.animateUI()
        self.appendSmallIcons()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.setAnimationLayer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.updateUIPositions()
        self.removeSmallIcons()
    }
    
    // MARK: - UI Functions
    
    func updateUIPositions() {
        
    }
    
    func animateUI() {
        updateUIPositions()
        //        let originalTransform = tutorialImageView.transform
        //        let translatedAnimation = originalTransform.translatedBy(x: 0.0, y: -150)
        //        UIView.animate(withDuration: 2, delay: 1, options: .curveEaseInOut, animations: {
        //            UIView.setAnimationDelegate(self)
        //            self.tutorialImageView.transform = translatedAnimation
        //        }) { (completed) in
        //            if completed {
        //                LogInfo("First Tutorial scene animation is completed.")
        //            }
        //        }
    }
    
    func setLabelTexts() {
        self.mainLabel.text = "firstLaunch_page2_title".localized()
        self.subLabel.text = "firstLaunch_page2_subtitle".localized()
    }
    
    // MARK: - Small Icon Creators
    
    func appendSmallIcons() {
        let numberOfItems = Int(arc4random_uniform(10) + 3)
        for _ in 0...numberOfItems {
            let timer = Timer.scheduledTimer(timeInterval: TimeInterval((Int(arc4random_uniform(10))+1)*5), target: self, selector: #selector(setIcons), userInfo: nil, repeats: true)
            timer.fire()
        }
    }
    
    @objc func setIcons() {
        let itemImageView = UIImageView(frame: CGRect(x: -40, y: Int(arc4random_uniform(32))*10, width: 40, height: 40 ))
        itemImageView.tag = 13
        itemImageView.image = UIImage(named: smallIcons[Int(arc4random_uniform(4))])
        self.fullView.insertSubview(itemImageView, at: Int(arc4random_uniform(UInt32(self.fullView.subviews.count))))
        self.animateSmallIcon(icon: itemImageView)
    }
    
    func animateSmallIcon(icon: UIImageView) {
        let originalPosition = icon.transform
        let translatedPosition = originalPosition.translatedBy(x: 700, y: 0)
        UIView.animate(withDuration: TimeInterval(arc4random_uniform(20)+10), delay: TimeInterval(arc4random_uniform(2)), options: .curveEaseInOut, animations: {
            UIView.setAnimationDelegate(self)
            icon.transform = translatedPosition
        }, completion: { (completed) in
            if completed {
                self.iconObjectCompleted(icon: icon)
            }
        })
    }
    
    func iconObjectCompleted(icon: UIImageView) {
        icon.removeFromSuperview()
    }
    
    func removeSmallIcons() {
        for subview in self.fullView.subviews {
            if subview.tag == 13 {
                subview.removeFromSuperview()
            }
        }
    }
    
    // MARK: - Images Array
    private(set) lazy var smallIcons: [String] = {
        return [
            "small_icon_tutorial_page1",
            "small_icon2_tutorial_page1",
            "small_icon3_tutorial_page1",
            "small_icon4_tutorial_page1"
        ]
    }()
    
    func setAnimationLayer() {
        for subview in self.imageBackView.subviews {
            if let sub = subview as? LOTAnimationView {
                sub.removeFromSuperview()
            }
        }
        self.imageView.isHidden = true
        let animationView = LOTAnimationView(name: "framesAnimation")
        animationView.frame = CGRect(x: 0, y: 0, width: self.imageBackView.frame.width, height: self.imageBackView.frame.height)
        self.imageBackView.addSubview(animationView)
        animationView.play{ (finished) in
        }
    }
    
}
