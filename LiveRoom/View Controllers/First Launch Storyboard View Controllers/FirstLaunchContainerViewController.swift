//
//  FirstLaunchContainerViewController.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 7.08.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class FirstLaunchContainerViewController: BaseUIViewController {
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var skipBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageControl.numberOfPages = 4
        addObservers()
        self.skipBtn.alpha = 0.0
        self.skipBtn.isHidden = true
        self.skipBtn.setTitle("firstLaunch_skip".localized(), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.changeBaseTintColor(UIColor.white)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func initialize(with object: NSObject?) -> Bool {
        return true
    }
    
    override func initialize(with firstObj: GenericObject?, secondObj: GenericObject?) -> Bool {
        return true
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(setPageControlTo0), name: .newMediaTutorialPageControlChanged, object: 0)
        NotificationCenter.default.addObserver(self, selector: #selector(setPageControlTo1), name: .newMediaTutorialPageControlChanged, object: 1)
        NotificationCenter.default.addObserver(self, selector: #selector(setPageControlTo2), name: .newMediaTutorialPageControlChanged, object: 2)
        NotificationCenter.default.addObserver(self, selector: #selector(setPageControlTo3), name: .newMediaTutorialPageControlChanged, object: 3)
        NotificationCenter.default.addObserver(self, selector: #selector(setPageControlTo4), name: .newMediaTutorialPageControlChanged, object: 4)
    }
    
    @objc func setPageControlTo0() {
        UIView.animate(withDuration: 0.5, animations: {
            self.pageControl.currentPage = 0
            self.skipBtn.alpha = 0.0
        }) { (completed) in
            if completed {
                self.skipBtn.isHidden = true
            }
        }
    }
    
    @objc func setPageControlTo1() {
        self.skipBtn.isHidden = false
        UIView.animate(withDuration: 0.5, animations: {
            self.pageControl.currentPage = 1
            self.skipBtn.alpha = 1.0
        }) { (completed) in
        }
    }
    
    @objc func setPageControlTo2() {
        UIView.animate(withDuration: 0.5) {
            self.pageControl.currentPage = 2
            self.skipBtn.setTitle("firstLaunch_skip".localized(), for: .normal)
        }
    }
    
    @objc func setPageControlTo3() {
        UIView.animate(withDuration: 0.5) {
            self.pageControl.currentPage = 3
            self.skipBtn.setTitle("firstLaunch_start".localized(), for: .normal)
        }
    }
    
    @objc func setPageControlTo4() {
        pageControl.currentPage = 4
    }
    
    @IBAction func skpBtnClicked(_ sender: Any) {
        self.dismiss(animated: true) {
            UserDefaults.standard.hasLaunchBefore = true
            LogInfo("\(self.className) is dismissed")
            CloudService.AccountManager.instance.start()
        }
    }
    
}

