//
//  FirstLaunchMainViewController.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 7.08.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class FirstLaunchMainViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    var currentIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        delegate = self
        showFirstViewController()
        // Do any additional setup after loading the view.
    }
    
    func showFirstViewController() {
        if let firstViewController = tutorialViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            NotificationCenter.default.post(name: .newMediaTutorialPageControlChanged, object: currentIndex)
        }
    }
    
    // MARK: - Page View Data Source
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        currentIndex = tutorialViewControllers.index(of: pendingViewControllers[0])!
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = tutorialViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        // User is on the first view controller and swiped left to loop to
        // the last view controller.
        guard previousIndex >= 0 else {
            return nil // tutorialViewControllers.last or nil
        }
        
        guard tutorialViewControllers.count > previousIndex else {
            return nil
        }
        
        return tutorialViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = tutorialViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let viewControllersCount = tutorialViewControllers.count
        
        // User is on the last view controller and swiped right to loop to
        // the first view controller.
        guard viewControllersCount != nextIndex else {
            return nil // nil or tutorialViewControllers.first
        }
        
        guard viewControllersCount > nextIndex else {
            return nil
        }
        
        return tutorialViewControllers[nextIndex]
    }
    
    // MARK: - Dots Source
    //
    //    func presentationCount(for pageViewController: UIPageViewController) -> Int {
    //        return tutorialViewControllers.count
    //    }
    //
    //    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
    //        guard let firstViewController = viewControllers?.first,
    //            let firstViewControllerIndex = tutorialViewControllers.index(of: firstViewController) else {
    //                return 0
    //        }
    //
    //        return firstViewControllerIndex
    //    }
    
    // MARK: - Array Controllers
    private(set) lazy var tutorialViewControllers: [UIViewController] = {
        return [
            self.nextViewController(name: page1),
            self.nextViewController(name: page2),
            self.nextViewController(name: page3),
            self.nextViewController(name: page4)
        ]
    }()
    
    private func nextViewController(name: String) -> UIViewController {
        let vc = UIStoryboard(name: "FirstLaunch", bundle: nil).instantiateViewController(withIdentifier: name)
        return vc
    }
    
    private var page1: String {
        get {
            return "Page1_FirstLaunchViewController"
        }
    }
    
    private var page2: String {
        get {
            return "Page2_FirstLaunchViewController"
        }
    }
    
    private var page3: String {
        get {
            return "Page3_FirstLaunchViewController"
        }
    }
    
    private var page4: String {
        get {
            return "Page4_FirstLaunchViewController"
        }
    }
    
}
