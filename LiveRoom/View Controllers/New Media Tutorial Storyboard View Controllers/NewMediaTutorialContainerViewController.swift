//
//  NewMediaTutorialContainerViewController.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 5.08.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class NewMediaTutorialContainerViewController: BaseUIViewController, NewMediaTutorialProtocol {
    var selectedMedia: MediaObject! {
        didSet {
            for child in self.childViewControllers {
                if let prt = child as? NewMediaTutorialProtocol {
                    prt.selectedMedia = self.selectedMedia
                }
            }
        }
    }
    var selectedRoom: RoomObject! {
        didSet {
            for child in self.childViewControllers {
                if let prt = child as? NewMediaTutorialProtocol {
                    prt.selectedRoom = self.selectedRoom
                }
            }
        }
    }

    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundView.backgroundColor = UIColor(red: 255/255, green: 127/255, blue: 0/255, alpha: 1.0)
        
        pageControl.numberOfPages = 3
        addObservers()
        for child in self.childViewControllers {
            if let prt = child as? NewMediaTutorialProtocol {
                prt.selectedMedia = self.selectedMedia
                prt.selectedRoom = self.selectedRoom
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let backBtn = UIBarButtonItem(image: UIImage(named: "icon_cancel"), style: .done, target: self, action: #selector(cancelClicked))
        self.navigationItem.setLeftBarButton(backBtn, animated: true)
        self.changeBaseTintColor(UIColor.white)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func initialize(with object: NSObject?) -> Bool {
        return true
    }
    
    override func initialize(with firstObj: GenericObject?, secondObj: GenericObject?) -> Bool {
        if let obj = firstObj as? RoomObject, let mediaObj = secondObj as? MediaObject {
            self.selectedRoom = obj
            self.selectedMedia = mediaObj
            return true
        } else {
            return false
        }
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(setPageControlTo0), name: .newMediaTutorialPageControlChanged, object: 0)
        NotificationCenter.default.addObserver(self, selector: #selector(setPageControlTo1), name: .newMediaTutorialPageControlChanged, object: 1)
        NotificationCenter.default.addObserver(self, selector: #selector(setPageControlTo2), name: .newMediaTutorialPageControlChanged, object: 2)
        NotificationCenter.default.addObserver(self, selector: #selector(setPageControlTo3), name: .newMediaTutorialPageControlChanged, object: 3)
        NotificationCenter.default.addObserver(self, selector: #selector(setPageControlTo4), name: .newMediaTutorialPageControlChanged, object: 4)
    }
    
    @objc func setPageControlTo0() {
        pageControl.currentPage = 0
        backgroundView.backgroundColor = UIColor(red: 255/255, green: 127/255, blue: 0/255, alpha: 1.0)
    }
    
    @objc func setPageControlTo1() {
        pageControl.currentPage = 1
    }
    
    @objc func setPageControlTo2() {
        pageControl.currentPage = 2
        backgroundView.backgroundColor = UIColor(red: 85/255, green: 85/255, blue: 85/255, alpha: 1)
    }
    
    @objc func setPageControlTo3() {
        pageControl.currentPage = 3
    }
    
    @objc func setPageControlTo4() {
        pageControl.currentPage = 4
        backgroundView.backgroundColor = UIColor(red: 34/255, green: 34/255, blue: 34/255, alpha: 1)
    }
    
    @objc func cancelClicked() {
        let okAct = LRAlertAction(title: "action_ok".localized(), style: .destructive) { (action) in
            _ = LDBService.MediaManager.sharedInstance.deleteMedia(self.selectedMedia)
            
            var rDetail: UIViewController?
            if self.navigationController?.viewControllers != nil {
                for vc in (self.navigationController?.viewControllers)! {
                    if let vc = vc as? RoomDetailViewController {
                        rDetail = vc
                    }
                }
            }
            if rDetail != nil {
                self.navigationController?.popToViewController(rDetail!, animated: true)
            }
        }
        let cancelAct = LRAlertAction(title: "action_cancel".localized(), style: .default) { (action) in
            
        }
        ActionSheetController.alert(title: "newMedia_sure_cancel_title".localized(), message: "newMedia_sure_cancel_subtitle".localized(), actions: cancelAct, okAct)
        
    }
}
