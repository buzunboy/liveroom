//
//  Page3_NewMediaViewController.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 5.08.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class Page3_NewMediaViewController: BaseUIViewController, NewMediaTutorialProtocol {
    var selectedMedia: MediaObject!
    var selectedRoom: RoomObject!
    // MARK: - IBOutlets
    
    @IBOutlet weak var fullView: UIView!
    @IBOutlet weak var backgroundRadialView: RadialTutorialView!
    @IBOutlet weak var tutorialImageView: UIImageView!
    @IBOutlet weak var tutorialLabel: UILabel!
    @IBOutlet weak var mainBackView: UIView!
    @IBOutlet weak var doneBtn: UIButton!
    
    // MARK: - View Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fullView.backgroundColor = UIColor(red: 85/255, green: 85/255, blue: 85/255, alpha: 1.0)
        self.changeBaseTintColor(UIColor.white)
    }
    
    override func viewDidLayoutSubviews() {
        
    }
    
    override func viewWillLayoutSubviews() {
        self.appendSmallIcons()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.updateUIPositions()
        self.appendSmallIcons()
        self.setLabelTexts()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.updateUIPositions()
        self.removeSmallIcons()
    }
    
    @IBAction func doneBtnClicked(_ sender: Any) {
        
        if self.selectedMedia.imageAssetId != nil && self.selectedMedia.videoAssetId != nil {
            self.navigationController?.popViewController(animated: true)
        } else {
            let okAct = LRAlertAction(title: "action_ok".localized(), style: .default) { (action) in
                
            }
            ActionSheetController.alert(title: "newMedia_no_image_title".localized(), message: "newMedia_no_image_message".localized(), actions: okAct)
        }
    }
    // MARK: - UI Functions
    
    func updateUIPositions() {
        tutorialImageView.frame = CGRect(x: 25, y: 300, width: 250, height: 250)
    }
    
    func animateUI() {
        self.backgroundRadialView.isHidden = false
        self.backgroundRadialView.alpha = 0.0
        UIView.animate(withDuration: 1.5) {
           self.backgroundRadialView.alpha = 1.0
        }
        self.backgroundRadialView.isHidden = false
        updateUIPositions()
        let originalTransform = tutorialImageView.transform
        let translatedAnimation = originalTransform.translatedBy(x: 0.0, y: -275)
        UIView.animate(withDuration: 2, delay: 1, options: .curveEaseInOut, animations: {
            UIView.setAnimationDelegate(self)
            self.tutorialImageView.transform = translatedAnimation
        }) { (completed) in
            if completed {
                LogInfo("First Tutorial scene animation is completed.")
            }
        }
    }
    
    func setLabelTexts() {
        self.doneBtn.setTitle("action_continue".localized(), for: .normal)
        
        if self.selectedMedia.imageAssetId != nil && self.selectedMedia.videoAssetId != nil {
            let labelText = NSMutableAttributedString(string: "newMedia_page3_title_done".localized(), attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 22)])
            let boldText = NSMutableAttributedString(string: "newMedia_page3_subtitle_done".localized(), attributes: [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 22)])
            labelText.append(boldText)
            self.tutorialLabel.attributedText = labelText
            self.doneBtn.backgroundColor = .blue
            
            self.animateUI()
        } else {
            let labelText = NSMutableAttributedString(string: "newMedia_page3_title_undone".localized(), attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 22)])
            let boldText = NSMutableAttributedString(string: "newMedia_page3_subtitle_undone".localized(), attributes: [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 22)])
            labelText.append(boldText)
            tutorialLabel.attributedText = labelText
            self.doneBtn.backgroundColor = .lightGray
            self.backgroundRadialView.isHidden = true
        }
        
    }
    
    // MARK: - Small Icon Creators
    
    func appendSmallIcons() {
        let numberOfItems = Int(arc4random_uniform(10) + 3)
        for _ in 0...numberOfItems {
            let timer = Timer.scheduledTimer(timeInterval: TimeInterval((Int(arc4random_uniform(10))+1)*5), target: self, selector: #selector(setIcons), userInfo: nil, repeats: true)
            timer.fire()
        }
    }
    
    @objc func setIcons() {
        let itemImageView = UIImageView(frame: CGRect(x: -40, y: Int(arc4random_uniform(32))*10, width: 40, height: 40 ))
        itemImageView.tag = 13
        itemImageView.image = UIImage(named: smallIcons[Int(arc4random_uniform(4))])
        self.backgroundRadialView.insertSubview(itemImageView, at: Int(arc4random_uniform(UInt32(self.backgroundRadialView.subviews.count))))
        self.animateSmallIcon(icon: itemImageView)
    }
    
    func animateSmallIcon(icon: UIImageView) {
        let originalPosition = icon.transform
        let translatedPosition = originalPosition.translatedBy(x: 700, y: 0)
        UIView.animate(withDuration: TimeInterval(arc4random_uniform(20)+10), delay: TimeInterval(arc4random_uniform(2)), options: .curveEaseInOut, animations: {
            UIView.setAnimationDelegate(self)
            icon.transform = translatedPosition
        }, completion: { (completed) in
            if completed {
                self.iconObjectCompleted(icon: icon)
            }
        })
    }
    
    func iconObjectCompleted(icon: UIImageView) {
        icon.removeFromSuperview()
    }
    
    func removeSmallIcons() {
        for subview in backgroundRadialView.subviews {
            if subview.tag == 13 {
                subview.removeFromSuperview()
            }
        }
    }
    
    // MARK: - Images Array
    private(set) lazy var smallIcons: [String] = {
        return [
            "small_icon_tutorial_page1",
            "small_icon2_tutorial_page1",
            "small_icon3_tutorial_page1",
            "small_icon4_tutorial_page1"
        ]
    }()

}
