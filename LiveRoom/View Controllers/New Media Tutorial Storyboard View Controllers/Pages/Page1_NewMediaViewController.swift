//
//  Page1_NewMediaViewController.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 5.08.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class Page1_NewMediaViewController: BaseUIViewController, NewMediaTutorialProtocol {
    var selectedMedia: MediaObject!
    var selectedRoom: RoomObject!
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var fullView: UIView!
    @IBOutlet weak var backgroundRadialView: RadialTutorialView!
    @IBOutlet weak var tutorialLabel: UILabel!
    @IBOutlet weak var mainBackView: UIView!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    
    var selectedVideo: VideoAsset? {
        didSet {
            
        }
    }
    var selectedImage: ImageAsset? {
        didSet {
            if self.imageView != nil {
                MainService.ImageStore(folderName: "images").getImageFromStorage(name: self.selectedImage!.name, splashImage: nil, handler: { (loaded) in
                    self.imageView.image = loaded
                })
            }
        }
    }
    
    // MARK: - View Functions
    
    var imagePickerService: MediaService.GenericPickerService!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLabelTexts()
        fullView.backgroundColor = UIColor(red: 255/255, green: 127/255, blue: 0/255, alpha: 1.0)
        self.changeBaseTintColor(UIColor.white)
    }
    
    override func viewDidLayoutSubviews() {
        
    }
    
    override func viewWillLayoutSubviews() {
        self.animateUI()
        self.appendSmallIcons()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.animateUI()
        self.appendSmallIcons()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.updateUIPositions()
        self.removeSmallIcons()
    }
    
    @IBAction func editBtnClicked(_ sender: Any) {
        let addAct = LRAlertAction(title: "media_getLibrary".localized(), style: .default) { (action) in
            self.addImage()
        }
        let addCameraAct = LRAlertAction(title: "media_capture".localized(), style: .default) { (action) in
            self.addImageFromCamera()
        }
        
        ActionSheetController.actionSheet(title: "media_Image".localized(), actions: addAct, addCameraAct)
    }
    // MARK: - UI Functions
    
    func addImage() {
        MainService.UserPermissionService.checkForPhotoLibrary { (authorized) in
            if authorized {
                self.imagePickerService = MediaService.ImagePickerService(delegate: self)
                self.imagePickerService.show()
            }
        }
    }
    
    func addImageFromCamera() {
        MainService.UserPermissionService.checkForPhotoLibrary { (authorized) in
            if authorized {
                self.imagePickerService = MediaService.CameraImagePickerService(delegate: self)
                self.imagePickerService.show()
            }
        }
    }
    
    func updateUIPositions() {
        
    }
    
    func animateUI() {
        updateUIPositions()
//        let originalTransform = tutorialImageView.transform
//        let translatedAnimation = originalTransform.translatedBy(x: 0.0, y: -150)
//        UIView.animate(withDuration: 2, delay: 1, options: .curveEaseInOut, animations: {
//            UIView.setAnimationDelegate(self)
//            self.tutorialImageView.transform = translatedAnimation
//        }) { (completed) in
//            if completed {
//                LogInfo("First Tutorial scene animation is completed.")
//            }
//        }
    }
    
    func setLabelTexts() {
        let labelText = NSMutableAttributedString(string: "newMedia_page1_title".localized(), attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 22)])
        let boldText = NSMutableAttributedString(string: "newMedia_page1_subtitle".localized(), attributes: [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 22)])
        labelText.append(boldText)
        tutorialLabel.attributedText = labelText
    }
    
    // MARK: - Small Icon Creators
    
    func appendSmallIcons() {
        let numberOfItems = Int(arc4random_uniform(10) + 3)
        for _ in 0...numberOfItems {
            let timer = Timer.scheduledTimer(timeInterval: TimeInterval((Int(arc4random_uniform(10))+1)*5), target: self, selector: #selector(setIcons), userInfo: nil, repeats: true)
            timer.fire()
        }
    }
    
    @objc func setIcons() {
        let itemImageView = UIImageView(frame: CGRect(x: -40, y: Int(arc4random_uniform(32))*10, width: 40, height: 40 ))
        itemImageView.tag = 13
        itemImageView.image = UIImage(named: smallIcons[Int(arc4random_uniform(4))])
        self.backgroundRadialView.insertSubview(itemImageView, at: Int(arc4random_uniform(UInt32(self.backgroundRadialView.subviews.count))))
        self.animateSmallIcon(icon: itemImageView)
    }
    
    func animateSmallIcon(icon: UIImageView) {
        let originalPosition = icon.transform
        let translatedPosition = originalPosition.translatedBy(x: 700, y: 0)
        UIView.animate(withDuration: TimeInterval(arc4random_uniform(20)+10), delay: TimeInterval(arc4random_uniform(2)), options: .curveEaseInOut, animations: {
            UIView.setAnimationDelegate(self)
            icon.transform = translatedPosition
        }, completion: { (completed) in
            if completed {
                self.iconObjectCompleted(icon: icon)
            }
        })
    }
    
    func iconObjectCompleted(icon: UIImageView) {
        icon.removeFromSuperview()
    }
    
    func removeSmallIcons() {
        for subview in backgroundRadialView.subviews {
            if subview.tag == 13 {
                subview.removeFromSuperview()
            }
        }
    }
    
    // MARK: - Images Array
    private(set) lazy var smallIcons: [String] = {
        return [
            "small_icon_tutorial_page1",
            "small_icon2_tutorial_page1",
            "small_icon3_tutorial_page1",
            "small_icon4_tutorial_page1"
        ]
    }()

}

extension Page1_NewMediaViewController: ImageServiceDelegate {
    func didImageChosen(_ image: UIImage?) {
        guard let image = image else { return }
        let vc = CropViewController(image: image)
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension Page1_NewMediaViewController: CropViewControllerDelegate {
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        cropViewController.navigationController?.popViewController(animated: true)
        let imgDict = ["mediaId": selectedMedia.id] as [String:String]
        let name = Utilities.encodeDictionary(imgDict)
        MainService.ImageStore(folderName: "images").saveImageToStorage(with: image, name: name!) { (error, url) in
            if let error = error {
                LogError("Couldn't save image to storage - Error: \(error)")
            }
            let imageObj = ImageAsset(id: Utilities.createNewid(), mediaId: self.selectedMedia.id, name: name!, location: url!.absoluteString, roomId: self.selectedRoom.id, creationDate: Date())
            self.selectedMedia.imageAssetId = imageObj.id
            _ = LDBService.MediaManager.sharedInstance.saveMedia(self.selectedMedia)
            _ = LDBService.AssetsManager.sharedInstance.saveAsset(imageObj as AssetObject)
            self.selectedImage = imageObj
        }
    }
}
