//
//  HomeViewController.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 1.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import Lottie
import DeepDiff

class HomeViewController: BaseUIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var updatedRooms: [RoomObject]!

    override func viewDidLoad() {
        self.tabBarController?.tabBar.isHidden = true
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.editLocalizables()
        self.tableView.register(UINib(nibName: "BaseRoomCell", bundle: nil), forCellReuseIdentifier: "homeCell")
        self.tableView.register(UINib(nibName: "RoomLastCell", bundle: nil), forCellReuseIdentifier: "emptyCell")
        self.tableView.reloadData()
        let rightBtn = UIBarButtonItem(image: UIImage(named: "icon_add"), style: .done, target: self, action: #selector(createNewRoom))
        self.navigationItem.rightBarButtonItem = rightBtn
        
        self.updatedRooms = MainService.RoomsService.sharedInstance.rooms
        self.tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        MainService.RoomsService.sharedInstance.delegate = self
        self.prepareUI()
        self.changeBaseTintColor(.white)
        //NavigationBarController.sharedInstance.viewWillAppeared(currentPos: self.tableView.contentOffset.y)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func prepareUI() {
        self.isNavigationBarTransparent = true
        self.navigationBarAppearanceLevel = -100
        self.navigationItem.largeTitleDisplayMode = .always
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    @objc func createNewRoom() {
        let newRoom = RoomObject(id: Utilities.createNewid(), name: "temp", owner: Utilities.UserInfoListHandler.sharedInstance.id!, creationDate: Date(), coverImage: nil)
        _ = LDBService.RoomsManager.sharedInstance.saveRoom(newRoom)
        let vc = UIViewController.ViewControllers.MainStoryboard.roomDetailVC()
        if vc.initialize(with: newRoom) {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func editLocalizables() {
        self.navigationItem.title = "home_title".localized()
    }

    override func didBaseTintColorChange() {
    }
    
    func createAnimation() {
        self.removeAnimation()
        let allView = UIView(frame: CGRect(x: 0, y: (Utilities.instance.deviceHeight-500)/2, width: 300, height: 500))
        allView.tag = 1293
        let label = UILabel(frame: CGRect(x: 20, y: 0, width: 300, height: 250))
        
        let text1 = NSMutableAttributedString(string: "home_noRoom_title".localized(), attributes: [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 40)])
        let text2 = NSMutableAttributedString(string: "home_noRoom_subtitle".localized(), attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 20)])
        text1.append(text2)
        label.attributedText = text1
        label.numberOfLines = 0
        label.textColor = .white
        let animationView = LOTAnimationView(name: "emptyContentAnimation")
        animationView.frame = CGRect(x: Utilities.instance.deviceWidth-300, y: 140, width: 300, height: 360)
        allView.addSubview(label)
        allView.addSubview(animationView)
        self.view.addSubview(allView)
        animationView.loopAnimation = true
        animationView.play{ (finished) in
        }
    }
    
    func removeAnimation() {
        for subview in self.view.subviews {
            if subview.tag == 1293 {
                subview.removeFromSuperview()
            }
        }
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let roomCount = self.updatedRooms?.count ?? 0
        NavigationBarController.sharedInstance.scrollViewScrolled(offset: self.tableView.contentOffset.y)
        if roomCount > 0 {
            let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 1))
            cell?.frame.size.height = 250
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 370
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let roomCount = self.updatedRooms?.count ?? 0
        if roomCount == 0 {
            self.createAnimation()
            self.tableView.isHidden = true
            self.tableView.isScrollEnabled = false
        } else {
            self.removeAnimation()
            self.tableView.isHidden = false
            self.tableView.isScrollEnabled = true
        }
        
        if section == 0 {
            return roomCount
        }
        
        if section == 1 {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            return tableView.dequeueReusableCell(withIdentifier: "homeCell", for: indexPath, with: nil)
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell", for: indexPath, with: nil)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if let cell = cell as? BaseRoomCell {
                cell.selectedRoom = self.updatedRooms?[indexPath.row]
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            
            let vc = UIViewController.ViewControllers.MainStoryboard.roomDetailVC()
            if let room = self.updatedRooms?[indexPath.row], vc.initialize(with: room) {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAct = UITableViewRowAction(style: .destructive, title: "Delete".localized()) { (action, indexPath) in
            guard let room = self.updatedRooms?[indexPath.row] else { return }
            let okAct = LRAlertAction(title: "action_ok".localized(), style: .default, completion: { (action) in
                _ = LDBService.RoomsManager.sharedInstance.deleteRoom(room: room)
                //self.updatedRooms = LDBService.RoomsManager.sharedInstance.getAllRooms()
                //self.tableView.reloadData()
            })
            let cancelAct = LRAlertAction(title: "action_cancel".localized(), style: .destructive, completion: { (action) in
            })
            ActionSheetController.alert(title: String(format: "format_home_delete_alert_title".localized(), room.name), message: "home_delete_alert_message".localized(), actions: okAct, cancelAct)
        }
        return [deleteAct]
    }
    
}

extension HomeViewController: RoomsServiceProtocol {
    
    func didRoomsUpdated() {
        let oldRooms = self.updatedRooms ?? [RoomObject]()
        self.updatedRooms = MainService.RoomsService.sharedInstance.rooms
        let diffUpdate = diff(old: oldRooms, new: self.updatedRooms)
        self.tableView.reload(changes: diffUpdate, section: 0, insertionAnimation: .automatic, deletionAnimation: .automatic, replacementAnimation: .automatic) { (completed) in
            if completed {
                LogInfo("TableView is updated")
            }
        }
    }
    
}
