//
//  ProfileViewController.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 1.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class ProfileViewController: BaseUIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    var ownRooms: [RoomObject]?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.prepareUI()
        self.tableView.register(UINib(nibName: "BaseRoomCell", bundle: nil), forCellReuseIdentifier: "homeCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if CloudService.AccountManager.instance.loginStatus == .anonym {
            let vc = UIViewController.ViewControllers.MainStoryboard.settingsVC()
            self.navigationController?.pushViewController(vc, animated: false)
        }
        super.viewWillAppear(animated)
        self.isNavigationBarTransparent = true
        self.prepareUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    func prepareUI() {
        self.isNavigationBarTransparent = true
        self.navigationBarAppearanceLevel = 100
        self.ownRooms = LDBService.RoomsManager.sharedInstance.getRoomsFromUser()
        self.tableView.reloadData()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        let settingsIcon = UIImage(named: "icon_settings")
        let settingsBtn = UIBarButtonItem(image: settingsIcon, style: .done, target: self, action: #selector(showSettings))
        self.navigationItem.rightBarButtonItem = settingsBtn
        self.blurView.alpha = 0
        NotificationCenter.default.post(name: .mainTableAppearButtons, object: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        NavigationBarController.sharedInstance.scrollViewScrolled(offset: self.tableView.contentOffset.y)
        self.blurView.alpha = Utilities.getScreenRatio(for: self.tableView.contentOffset.y, compare: Utilities.instance.deviceHeight/4)
        if self.tableView.contentOffset.y > 2 {
            NotificationCenter.default.post(name: .mainTableDisapperButtons, object: nil)
        }
        let heightCalculator = (Utilities.instance.deviceHeight-64-44+200)/2
        if self.tableView.contentOffset.y > heightCalculator {
            self.navigationItem.title = Utilities.UserInfoListHandler.sharedInstance.email ?? ""
        } else {
            self.navigationItem.title = nil
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        NotificationCenter.default.post(name: .mainTableAppearButtons, object: nil)
    }
    
    override func initialize(with object: NSObject?) -> Bool {
        return true
    }
    
    @objc func showSettings() {
        let vc = UIViewController.ViewControllers.MainStoryboard.settingsVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight {
                return Utilities.instance.deviceHeight-30-44
            } else {
                return Utilities.instance.deviceHeight-64-44
            }
        } else {
            return 360
        }
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 0 {
            return false
        }
        if indexPath.section == 1 {
            return true
        } else {
            return false
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return self.ownRooms?.count ?? 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            return tableView.dequeueReusableCell(withIdentifier: "ProfileMainTableViewCell", for: indexPath, with: nil, superViewController: self)
        } else if indexPath.section == 1{
            return tableView.dequeueReusableCell(withIdentifier: "homeCell", for: indexPath, with: nil)
        } else {
            return tableView.dequeueEmptyCell()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? BaseRoomCell {
            cell.selectedRoom = self.ownRooms?[indexPath.row]
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.section == 1 {
//            let vc = UIViewController.ViewControllers.MainStoryboard.mediaDetailVC()
//            if vc.initialize(with: selectedRoom, secondObj: self.medias?[indexPath.row]) {
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
//        }
    }
    
}
