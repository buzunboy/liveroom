//
//  SettingsTableViewController.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 22.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import MessageUI

class StandardSettingCell: UITableViewCell {
    
}

class SwitchSettingCell: UITableViewCell {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var switchBtn: UISwitch!
    
    @IBAction func switchClicked(_ sender: Any) {
        switchTarget?(self.switchBtn.isOn)
    }
    
    public var switchTarget: SwitchHandler?
    public typealias SwitchHandler = (_ state: Bool)->()
}

class BigButtonSettingCell: UITableViewCell {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var label: UILabel!
}

class SettingsTableViewController: BaseUITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.editLocalizables()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.prepareUI()
        if CloudService.AccountManager.instance.loginStatus == .anonym {
            self.navigationItem.setHidesBackButton(true, animated: false)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func prepareUI() {
        self.tableView.reloadData()
    }
    
    override func editLocalizables() {
        self.navigationItem.title = "settings_title".localized()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 1:
            return 1
        case 2:
            return 1
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath == SettingsTableViewController.SendLogIndex {
            self.sendMail(withLog: true)
        }
        if indexPath == SettingsTableViewController.NewComerTutorialIndex {
            let vc = UIViewController.ViewControllers.FirstLaunchStoryboard.mainVC()
            self.present(vc, animated: true, completion: nil)
        }
        
        if indexPath == SettingsTableViewController.LogOutIndex {
            self.logOut()
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "settings_SETTINGS".localized()
        default:
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath == SettingsTableViewController.HomeKitIndex {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SwitchSettingCell", for: indexPath) as! SwitchSettingCell
            cell.label.text = "settings_enableHomeKit".localized()
            cell.switchTarget = { (isOn) in
                Utilities.UserInfoListHandler.sharedInstance.setValue(isOn, for: "isHomeKitEnabled")
            }
            return cell
        }
        
        if indexPath == SettingsTableViewController.SendLogIndex {
            let cell = tableView.dequeueReusableCell(withIdentifier: "StandardSettingCell", for: indexPath) as! StandardSettingCell
            cell.textLabel?.text = "settings_sendLog".localized()
            return cell
        }
        
        if indexPath == SettingsTableViewController.LogOutIndex {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BigButtonSettingCell", for: indexPath) as! BigButtonSettingCell
            if CloudService.AccountManager.instance.loginStatus == .anonym {
                cell.label.text = "settings_register".localized()
                cell.mainView.backgroundColor = .orange
            } else {
                cell.label.text = "settings_logOut".localized()
                cell.mainView.backgroundColor = .red
            }
            return cell
        }
        
        if indexPath == SettingsTableViewController.NewComerTutorialIndex {
            let cell = tableView.dequeueReusableCell(withIdentifier: "StandardSettingCell", for: indexPath) as! StandardSettingCell
            cell.textLabel?.text = "settings_newComer".localized()
            return cell
        }
        
        return tableView.dequeueEmptyCell()
    }
    
    func sendMail(withLog: Bool) {
        let dataURL = LoggingInterface.sharedInstance.createZipFile()
        if MFMailComposeViewController.canSendMail() {
            let mailVC = MFMailComposeViewController()
            mailVC.mailComposeDelegate = self
            let dateStr = DateFormatter.localizedString(from: Date(), dateStyle: .short, timeStyle: .short)
            mailVC.setSubject("LiveRoom Support \(dateStr) - User: \(Utilities.UserInfoListHandler.sharedInstance.email ?? "")")
            let data = NSData(contentsOfFile: dataURL)
            mailVC.addAttachmentData(data! as Data, mimeType: "application/zip, application/octet-stream", fileName: "LiveRoom.log")
            self.present(mailVC, animated: true, completion: nil)
        }
    }
    
    func logOut() {
        CloudService.AccountManager.instance.logOut { (error) in
            
            if let error = error {
                LogError("Couldn't logged out - Error: \(error.localizedDescription)")
                return
            }
            
            LogInfo("User logged out successfuly")
        }
    }
    
    static var HomeKitIndex: IndexPath = IndexPath(row: 0, section: 0)
    static var SendLogIndex: IndexPath = IndexPath(row: 1, section: 0)
    static var NewComerTutorialIndex: IndexPath = IndexPath(row: 0, section: 1)
    static var LogOutIndex: IndexPath = IndexPath(row: 0, section: 2)
}

extension SettingsTableViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
}
