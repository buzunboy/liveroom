//
//  TabBarViewController.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 1.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.tintColor = UIColor.orange
        self.tabBar.items![0].title = "tabbar_home".localized()
        self.tabBar.items![1].title = "tabbar_profile".localized()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if CloudService.AccountManager.instance.loginStatus == .anonym {
            self.tabBar.items![1].title = "tabbar_settings".localized()
            self.tabBar.items![1].image = UIImage(named: "icon_settings")
            self.tabBar.items![1].selectedImage = UIImage(named: "icon_settings")
        } else {
            self.tabBar.items![1].title = "tabbar_profile".localized()
            self.tabBar.items![1].image = UIImage(named: "icon_profile")
            self.tabBar.items![1].selectedImage = UIImage(named: "icon_profile")
        }
    }

}
