//
//  LiveRoomViewController.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 28.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import AVKit

class LiveRoomViewController: BaseUIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    
    var selectedRoom: RoomObject!
    var loadedMedias: [MediaObject]?
    var loadedPlayer: AVPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        
        // Create a new scene
        let scene = SCNScene()
        
        // Set the scene to the view
        sceneView.scene = scene
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if #available(iOS 11.3, *) {
            var referenceImages = Set<ARReferenceImage>()
            if self.loadedMedias != nil {
                for media in self.loadedMedias! {
                    guard let id = media.imageAssetId, let imageAss = LDBService.AssetsManager.sharedInstance.getAsset(id: id), let img = MainService.ImageStore(folderName: "images").getImage(name: imageAss.name) else { return }
                    let refImg = ARReferenceImage(img.cgImage!, orientation: CGImagePropertyOrientation.up, physicalWidth: 0.05)
                    refImg.name = imageAss.mediaId
                    referenceImages.insert(refImg)
                }
            }
            
            let configuration = ARWorldTrackingConfiguration()
            configuration.detectionImages = referenceImages
            sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
        }
        // Create a session configuration
//        if #available(iOS 12.0, *) {
//            let configuration = ARImageTrackingConfiguration()
//            configuration.trackingImages = referenceImages
//            configuration.maximumNumberOfTrackedImages = (referenceImages.count > 5) ? 5 : referenceImages.count
//            sceneView.session.run(configuration)
//
//        } else {
//            // Fallback on earlier versions
//        }
        
        // Run the view's session
    }
    
    override func initialize(with object: NSObject?) -> Bool {
        if let room = object as? RoomObject {
            self.selectedRoom = room
            self.prepare()
            return true
        } else {
            return false
        }
    }
    
    func prepare() {
        self.loadedMedias = LDBService.MediaManager.sharedInstance.getMedias(forRoomId: self.selectedRoom.id)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    func createVideo(name: String) {
        self.loadedPlayer = MediaService.AVService.getPlayerController(for: name)?.player
    }

    // MARK: - ARSCNViewDelegate
    

    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
        if #available(iOS 11.3, *) {
            if let imageAnchor = anchor as? ARImageAnchor {
                if let imageName = imageAnchor.referenceImage.name {
                    if let media = LDBService.MediaManager.sharedInstance.getMedia(id: imageName), let videoId = media.videoAssetId, let videoAsset = LDBService.AssetsManager.sharedInstance.getAsset(id: videoId) {
                        let plane = SCNPlane(width: imageAnchor.referenceImage.physicalSize.width, height: imageAnchor.referenceImage.physicalSize.height)
                        self.createVideo(name: videoAsset.name)
                        plane.firstMaterial?.diffuse.contents = self.loadedPlayer
                        self.loadedPlayer?.play()
                        
                        let planeNode = SCNNode(geometry: plane)
                        planeNode.eulerAngles.x = -.pi/2
                        node.addChildNode(planeNode)
                        if Utilities.UserInfoListHandler.sharedInstance.isHomeKitEnabled {
                            if let imgId = media.imageAssetId, let imgObj = LDBService.AssetsManager.sharedInstance.getAsset(id: imgId), let img = MainService.ImageStore(folderName: "images").getImage(name: imgObj.name) {
                                MainService.HomeKit.instance.prepareLamps(with: img)
                            }
                        }
                        
                    }
                }
                
            }
        } else {
            // Fallback on earlier versions
        }
        return node
    }

    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        print("removed")
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        if #available(iOS 11.3, *) {
            if let imageAnchor = anchor as? ARImageAnchor {
                if let imageName = imageAnchor.referenceImage.name {
                    if let media = LDBService.MediaManager.sharedInstance.getMedia(id: imageName), let videoId = media.videoAssetId, let _ = LDBService.AssetsManager.sharedInstance.getAsset(id: videoId) {
                        let duration = self.loadedPlayer?.currentItem?.duration
                        let current = self.loadedPlayer?.currentTime()
                        if duration != nil && current != nil {
                            if CMTimeGetSeconds(current!) >= CMTimeGetSeconds(duration!) {
                                self.loadedPlayer?.seek(to: kCMTimeZero)
                                self.loadedPlayer?.play()
                            }
                        }
                        if Utilities.UserInfoListHandler.sharedInstance.isHomeKitEnabled {
                            if let imgId = media.imageAssetId, let imgObj = LDBService.AssetsManager.sharedInstance.getAsset(id: imgId), let img = MainService.ImageStore(folderName: "images").getImage(name: imgObj.name) {
                                MainService.HomeKit.instance.prepareLamps(with: img)
                            }
                        }
                        
                    } else {
                        print("ssds")
                    }
                } else {
                    print("asd")
                }
                
            } else {
                print("asddsd")
            }
        }
    }
    
}
