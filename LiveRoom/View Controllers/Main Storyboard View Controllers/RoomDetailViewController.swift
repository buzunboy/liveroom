//
//  RoomDetailViewController.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 28.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import ARKit

class RoomDetailViewController: BaseUIViewController {
    
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    var selectedRoom: RoomObject!
    var medias: [MediaObject]?
//    var preparedImage: UIImage? {
//        didSet {
//            if self.preparedImage != nil {
//                if self.backgroundImageView != nil {
//                    self.backgroundImageView.image = self.preparedImage!
//                }
//            }
//        }
//    }
    
    private var doneBtn: UIBarButtonItem!
    private var addMediaBtn: UIBarButtonItem!
    private var editRoomBtn: UIBarButtonItem!
    private var shareBtn: UIBarButtonItem!
    
    private var loadingIndicator: LRLoadingIndicator!
    
    private var mainCell: RoomMainTableViewCell?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.isNavigationBarTransparent = true
        self.navigationBarAppearanceLevel = 50
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.tabBarController?.tabBar.isHidden = true
        UIDevice.lockOrientation(.all)
        //self.prepareUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBar.tintColor = UIColor.orange
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        UIDevice.lockOrientation(.portrait, andRotateTo: .portrait)
        NotificationCenter.default.post(name: .mainTableDisappear, object: nil)
    }
    
    func prepareUI() {
        self.navigationItem.largeTitleDisplayMode = .never
        //self.navigationController?.navigationBar.backgroundColor = .clear
//        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.doneBtn = UIBarButtonItem(image: UIImage(named: "icon_done"), style: .done, target: self, action: #selector(doneBtnClicked))
        self.addMediaBtn = UIBarButtonItem(image: UIImage(named: "icon_add"), style: .done, target: self, action: #selector(addMediaBtnClicked))
        self.editRoomBtn = UIBarButtonItem(image: UIImage(named: "icon_settings"), style: .done, target: self, action: #selector(editRoomBtnClicked))
        self.shareBtn = UIBarButtonItem(image: UIImage(named: "icon_share"), style: .done, target: self, action: #selector(shareBtnClicked))
        
        self.prepareNavButtons()
        self.getMedias()
        self.blurView.alpha = 0
        NotificationCenter.default.post(name: .mainTableAppearButtons, object: nil)
        
        self.loadingIndicator = LRLoadingIndicator(maxRadii: 100)
        self.view.addSubview(self.loadingIndicator)
        self.loadingIndicator.center = self.view.center
        self.loadingIndicator.shouldShowBackground = true
    }
    
    private func prepareNavButtons() {
        var btnList = [UIBarButtonItem]()
        
        if self.selectedRoom.isOwner() {
            btnList.append(addMediaBtn)
            btnList.append(editRoomBtn)
        }
        
        btnList.append(shareBtn)
        self.navigationItem.rightBarButtonItems = btnList
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        NavigationBarController.sharedInstance.scrollViewScrolled(offset: self.tableView.contentOffset.y)
        
        self.blurView.alpha = Utilities.getScreenRatio(for: self.tableView.contentOffset.y, compare: Utilities.instance.deviceHeight/4) + 0.2
        if self.tableView.contentOffset.y > Utilities.roomDetailScreenOffsetValue {
            NotificationCenter.default.post(name: .mainTableDisapperButtons, object: nil)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if self.tableView.contentOffset.y <= Utilities.roomDetailScreenOffsetValue {
            NotificationCenter.default.post(name: .mainTableAppearButtons, object: nil)
        }
    }
    
    override func initialize(with object: NSObject?) -> Bool {
        guard let object = object as? RoomObject else { return false }
        self.selectedRoom = object
        return true
    }
    
    func getMedias() {
        self.medias = self.selectedRoom.medias
        if self.selectedRoom.coverImage != nil {
            
            if let img = MainService.RoomsService.roomImage(roomId: self.selectedRoom.id) {
                self.backgroundImageView.image = img
                if let color = img.getColors().primary {
                    self.changeBaseTintColor(color)
                    self.mainCell?.labelTintColor = color
                }
            }
            
            MainService.RoomsService.roomImage(roomId: self.selectedRoom.id) { (loaded) in
                guard let loaded = loaded else { return }
                DispatchQueue.main.async {
                    self.backgroundImageView.image = loaded
                    if let color = loaded.getColors().primary {
                        self.changeBaseTintColor(color)
                        self.mainCell?.labelTintColor = color
                    }
                }
            }
            
        }
        self.tableView.reloadData()
    }
    
    @objc private func addMediaBtnClicked() {
        let vc = UIViewController.ViewControllers.NewMediaTutorialStoryboard.mainVC()
        let media = MediaObject(id: Utilities.createNewid(), roomId: selectedRoom.id, videoAssetId: nil, imageAssetId: nil)
        if vc.initialize(with: selectedRoom, secondObj: media) {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc private func editRoomBtnClicked() {
        self.navigationItem.rightBarButtonItems = [doneBtn]
        NotificationCenter.default.post(name: .mainTableShowEditOptions, object: nil)
    }
    
    @objc private func doneBtnClicked() {
        self.prepareNavButtons()
        NotificationCenter.default.post(name: .mainTableHideEditOptions, object: nil)
    }
    
    @objc private func shareBtnClicked() {
        self.loadingIndicator.startAnimating()
        var username: String?
        if self.selectedRoom.isOwner() {
            username = Utilities.UserInfoListHandler.sharedInstance.email ?? ""
            self.createShareSheet(username: username)
        } else {
            CloudService.Database.instance.getUsername(id: self.selectedRoom.owner) { (error, user) in
                self.createShareSheet(username: user)
            }
        }
    }
    
    private func createShareSheet(username: String?) {
        if let link = self.selectedRoom.getShareLink() {
            let title = String(format: "format_share_room".localized(), self.selectedRoom.name, username ?? "anonym".localized(), link)
            ActionSheetController.activityController(activityItems: [title], applicationActivities: nil) {
                self.loadingIndicator.stopAnimating()
            }
        } else {
            ActionSheetController.anonymAlert()
            self.loadingIndicator.stopAnimating()
        }
    }
    
    override func linkageChanged() {
        if isLinked {
            self.prepareUI()
        }
    }
    
}


extension RoomDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight {
                return Utilities.instance.deviceHeight-30
            } else {
                return Utilities.instance.deviceHeight-64
            }
        } else {
            return 300
        }
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 0 {
            return false
        }
        if indexPath.section == 1 {
            return true
        } else {
            return false
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return (self.medias?.count != nil) ? self.medias!.count : 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RoomMainTableViewCell", for: indexPath, with: self.selectedRoom, superViewController: self)
            self.mainCell = cell as? RoomMainTableViewCell
            self.mainCell?.labelTintColor = self.baseTintColor
            return cell
        } else if indexPath.section == 1{
            return tableView.dequeueReusableCell(withIdentifier: "RoomDetailTableViewCell", for: indexPath, with: self.medias?[indexPath.row])
        } else {
            return tableView.dequeueEmptyCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            let vc = UIViewController.ViewControllers.MainStoryboard.mediaDetailVC()
            if vc.initialize(with: selectedRoom, secondObj: self.medias?[indexPath.row]) {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if indexPath.section == 1 {
            let deleteAct = UITableViewRowAction(style: .destructive, title: "Delete".localized()) { (action, indexPath) in
                guard let media = self.medias?[indexPath.row] else { return }
                let okAct = LRAlertAction(title: "action_ok".localized(), style: .default, completion: { (action) in
                    _ = LDBService.MediaManager.sharedInstance.deleteMedia(media)
                    self.getMedias()
                    self.tableView.reloadData()
                })
                let cancelAct = LRAlertAction(title: "action_cancel".localized(), style: .destructive, completion: { (action) in
                })
                ActionSheetController.alert(title: "media_sure_delete_title".localized(), message: "media_delete_alert_message".localized(), actions: okAct, cancelAct)
            }
            return [deleteAct]
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 1 { return true }
        return false
    }
    
}


extension RoomDetailViewController: StartScrollerDelegate {
    
    func didRatioUpdated(ratio: CGFloat) {
        self.blurView.alpha = ratio + 0.2
    }
    
    func didAccepted() {
        let vc = UIViewController.ViewControllers.MainStoryboard.liveRoomVC()
        if vc.initialize(with: self.selectedRoom) {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}

extension RoomDetailViewController: ImageServiceDelegate {
    
    func didImageChosen(_ image: UIImage?) {
        guard let image = image else { return }
        self.backgroundImageView.image = image
        MainService.RoomsService.saveRoomImage(image: image, room: self.selectedRoom)
    }
    
}
