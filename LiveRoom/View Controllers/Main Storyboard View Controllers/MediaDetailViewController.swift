//
//  MediaDetailViewController.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 29.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class MediaDetailViewController: BaseUIViewController {
    
    var selectedRoom: RoomObject!
    @IBOutlet weak var pictureView: UIView!
    @IBOutlet weak var pictureImageView: UIImageView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var videoImageView: UIImageView!
    var selectedMedia: MediaObject!
    var selectedVideo: VideoAsset? {
        didSet {
            self.prepareVideoView()
        }
    }
    var selectedImage: ImageAsset? {
        didSet {
            self.prepareImageView()
        }
    }
    var videoPickerService: MediaService.VideoPickerService!
    var imagePickerService: MediaService.GenericPickerService!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let addVideoBtn = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addVideo))
        self.navigationItem.rightBarButtonItem = addVideoBtn
        if self.selectedMedia.imageAssetId == nil {
            let vc = UIViewController.ViewControllers.NewMediaTutorialStoryboard.mainVC()
            (vc as! NewMediaTutorialProtocol).selectedRoom = self.selectedRoom
            (vc as! NewMediaTutorialProtocol).selectedMedia = self.selectedMedia
            for child in vc.childViewControllers {
                if let prt = child as? NewMediaTutorialProtocol {
                    prt.selectedRoom = self.selectedRoom
                    prt.selectedMedia = self.selectedMedia
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.prepareUI()
    }
    
    func prepareUI() {
        if let media = LDBService.MediaManager.sharedInstance.getMedia(id: self.selectedMedia.id) {
            self.selectedMedia = media
        }
        
        if self.selectedMedia != nil {
            
            if let videoId = self.selectedMedia.videoAssetId, let videoObj = LDBService.AssetsManager.sharedInstance.getAsset(id: videoId) {
                self.selectedVideo = videoObj.convertToVideoAsset()
            }
            if let imageId = self.selectedMedia.imageAssetId, let imageObj = LDBService.AssetsManager.sharedInstance.getAsset(id: imageId) {
                self.selectedImage = imageObj.convertToImageAsset()
            }
        }
    }
    
    func prepareVideoView() {
        if self.selectedVideo != nil {
            guard let name = self.selectedVideo!.name, let playerVC = MediaService.AVService.getPlayerController(for: name) else { return }
            if let img = MediaService.AVService.getImageFromVideo(player: playerVC.player!) {
                self.videoImageView.image = img
            }
        }
    }
    
    func prepareImageView() {
        if self.selectedImage != nil {
            MainService.ImageStore(folderName: "images").getImageFromStorage(name: self.selectedImage!.name, splashImage: nil, handler: { (loaded) in
                self.pictureImageView.image = loaded
                self.backgroundImageView.image = loaded
            })
        }
    }
    
    @objc func addVideo() {
        MainService.UserPermissionService.checkForPhotoLibrary { (authorized) in
            if authorized {
                self.videoPickerService = MediaService.VideoPickerService(delegate: self)
                self.videoPickerService.show()
            }
        }
    }
    
    func addImage() {
        MainService.UserPermissionService.checkForPhotoLibrary { (authorized) in
            if authorized {
                self.imagePickerService = MediaService.ImagePickerService(delegate: self)
                self.imagePickerService.show()
            }
        }
    }
    
    func addImageFromCamera() {
        MainService.UserPermissionService.checkForPhotoLibrary { (authorized) in
            if authorized {
                self.imagePickerService = MediaService.CameraImagePickerService(delegate: self)
                self.imagePickerService.show()
            }
        }
    }
    
    override func initialize(with object: NSObject?) -> Bool {
        return true
    }
    
    override func initialize(with firstObj: GenericObject?, secondObj: GenericObject?) -> Bool {
        if let obj = firstObj as? RoomObject, let mediaObj = secondObj as? MediaObject {
            self.selectedRoom = obj
            self.selectedMedia = mediaObj
            return true
        } else {
            return false
        }
    }
    
    @IBAction func videoPlayBtnClicked(_ sender: Any) {
        if self.selectedVideo != nil {
            let playAct = LRAlertAction(title: "media_playVideo".localized(), style: .default) { (action) in
                guard let name = self.selectedVideo?.name, let playerVC = MediaService.AVService.getPlayerController(for: name) else { return }
                playerVC.player?.play()
                self.navigationController?.pushViewController(playerVC, animated: true)
            }
            
            let changeAct = LRAlertAction(title: "media_changeVideo".localized(), style: .default) { (action) in
                self.addVideo()
            }
            ActionSheetController.actionSheet(title: "media_video".localized(), actions: playAct, changeAct)
        } else {
            self.addVideo()
        }
        
        
    }
    
    @IBAction func imageBtnClicked(_ sender: Any) {
        let vc = UIViewController.ViewControllers.MainStoryboard.imagePreviewVC()
        
        let showAct = LRAlertAction(title: "media_showImage".localized(), style: .default) { (action) in
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        let addAct = LRAlertAction(title: "media_getLibrary".localized(), style: .default) { (action) in
            self.addImage()
        }
        let addCameraAct = LRAlertAction(title: "media_capture".localized(), style: .default) { (action) in
            self.addImageFromCamera()
        }
        
        if vc.initialize(with: self.pictureImageView.image) {
            ActionSheetController.actionSheet(title: "media_Image".localized(), actions: showAct, addAct, addCameraAct)
        } else {
            ActionSheetController.actionSheet(title: "media_Image".localized(), actions: addAct, addCameraAct)
        }

    }
    
}

extension MediaDetailViewController: VideoServiceDelegate {
    func didVideoChosen(_ video: NSURL?) {
        if let videoURL = video {
            let videoDict = ["mediaId": selectedMedia.id, "id": Utilities.createNewid()] as [String:Any]
            let name = Utilities.encodeDictionary(videoDict)
            let store = MainService.VideoStorage(folderName: "videos").saveVideoToStorage(videoURL: videoURL as URL, name: name!)
            let assetObj = VideoAsset(id: Utilities.createNewid(), mediaId: self.selectedMedia.id, name: name!, location: store.absoluteString, roomId: selectedRoom.id, creationDate: Date())
            _ = LDBService.AssetsManager.sharedInstance.saveAsset(assetObj as AssetObject)
            self.selectedMedia.videoAssetId = assetObj.id
            _ = LDBService.MediaManager.sharedInstance.saveMedia(self.selectedMedia)
            self.selectedVideo = assetObj
        }
    }
}

extension MediaDetailViewController: ImageServiceDelegate {
    func didImageChosen(_ image: UIImage?) {
        guard let image = image else { return }
        let vc = CropViewController(image: image)
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MediaDetailViewController: CropViewControllerDelegate {
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        cropViewController.navigationController?.popViewController(animated: true)
        let imgDict = ["mediaId": selectedMedia.id] as [String:String]
        let name = Utilities.encodeDictionary(imgDict)
        MainService.ImageStore(folderName: "images").saveImageToStorage(with: image, name: name!) { (error, url) in
            if let error = error {
                LogError("Couldn't save image to storage - Error: \(error)")
            }
            let imageObj = ImageAsset(id: Utilities.createNewid(), mediaId: self.selectedMedia.id, name: name!, location: url!.absoluteString, roomId: self.selectedRoom.id, creationDate: Date())
            self.selectedMedia.imageAssetId = imageObj.id
            _ = LDBService.MediaManager.sharedInstance.saveMedia(self.selectedMedia)
            _ = LDBService.AssetsManager.sharedInstance.saveAsset(imageObj as AssetObject)
            self.selectedImage = imageObj
        }
    }
}
