//
//  NewMediaTutorialProtocol.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 5.08.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

protocol NewMediaTutorialProtocol: NSObjectProtocol {
    
    var selectedRoom: RoomObject! { get set }
    var selectedMedia: MediaObject! { get set }
    
}
