//
//  NavigationBarProtocol.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 11.08.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

protocol NavigationBarProtocol {
    
    var isNavigationBarTransparent: Bool { get set }
    var navigationBarAppearanceLevel: CGFloat { get set }
    
}
