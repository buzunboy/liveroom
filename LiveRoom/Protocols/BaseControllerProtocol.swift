//
//  BaseControllerProtocol.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 28.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

protocol BaseControllerProtocol {
    var isLinked: Bool { get set }
    var baseTintColor: UIColor { get set }
}

extension BaseControllerProtocol {

}
