//
//  RoomsServiceProtocol.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 12.08.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

protocol RoomsServiceProtocol {
    
    func didRoomsUpdated()
    
}
