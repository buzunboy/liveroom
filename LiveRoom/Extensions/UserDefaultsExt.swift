//
//  UserDefaultsExt.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 7.08.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

extension UserDefaults {
    
    var hasLaunchBefore: Bool {
        get {
            let retVal = self.bool(forKey: #function)
            if !retVal {
                CloudService.AccountManager.instance.logOut { (error) in
                    if let error = error {
                        LogError("Erasing old logged-in users failed - Error: \(error.localizedDescription)")
                    } else {
                        LogInfo("Old logged-in users are removed from Cloud")
                    }
                }
            }
            return retVal
        }
        set {
            self.set(newValue, forKey: #function)
            LogInfo("User Defaults value for key \(#function) is changed to \(newValue)")
        }
    }
    
}
