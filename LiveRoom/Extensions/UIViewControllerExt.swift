//
//  UIViewControllerExt.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 29.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

extension UIViewController {
    
    @objc func linked() {
        
    }

    @objc func presentWithLink(navigationController: UINavigationController?, viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        if navigationController != nil {
            self.present(navigationController!, animated: flag) {
                viewControllerToPresent.linked()
                completion?()
            }
        } else {
            self.present(viewControllerToPresent, animated: flag) {
                viewControllerToPresent.linked()
                completion?()
            }
        }
    }
    
    static func topViewController() -> UIViewController? {
        var topViewController = UIApplication.shared.keyWindow?.rootViewController
        if topViewController?.presentedViewController != nil {
            topViewController = topViewController?.presentedViewController
        } else if let nav = topViewController as? UINavigationController {
            topViewController = nav.topViewController
        } else if let tab = topViewController as? UITabBarController {
            topViewController = tab.selectedViewController
        }
        
        return topViewController
    }
    
    @objc func initialize(with object: NSObject?) -> Bool {
        fatalError("Initialize should be overridden")
    }
    
    @objc func initialize(with firstObj: GenericObject?, secondObj: GenericObject?) -> Bool {
        return false
    }
    
    @objc func editLocalizables() {
        
    }
    
    public var mainStoryboard: UIStoryboard {
        get {
            return UIStoryboard(name: "Main", bundle: nil)
        }
    }
    
    public var registrationStoryboard: UIStoryboard {
        get {
            return UIStoryboard(name: "Registration", bundle: nil)
        }
    }
    
    public var newMediaTutStoryboard: UIStoryboard {
        get {
            return UIStoryboard(name: "NewMediaTutorial", bundle: nil)
        }
    }
    
    class ViewControllers {
        
        private init() {
            
        }
        
        class MainStoryboard {
            
            private init() {
                
            }
            
            private static var storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            class func homeVC() -> UIViewController {
                return storyboard.instantiateViewController(withIdentifier: "HomeViewController")
            }
            
            class func profileVC() -> UIViewController {
                return storyboard.instantiateViewController(withIdentifier: "ProfileViewController")
            }
            
            class func tabBarVC() -> UIViewController {
                return storyboard.instantiateViewController(withIdentifier: "TabBarViewController")
            }
            
            class func liveRoomVC() -> UIViewController {
                return storyboard.instantiateViewController(withIdentifier: "LiveRoomViewController")
            }
            
            class func roomDetailVC() -> UIViewController {
                return storyboard.instantiateViewController(withIdentifier: "RoomDetailViewController")
            }
            
            class func mediaDetailVC() -> UIViewController {
                return storyboard.instantiateViewController(withIdentifier: "MediaDetailViewController")
            }
            
            class func imagePreviewVC() -> UIViewController {
                return storyboard.instantiateViewController(withIdentifier: "ImagePreviewViewController")
            }
            
            class func settingsVC() -> UIViewController {
                return storyboard.instantiateViewController(withIdentifier: "SettingsTableViewController")
            }
            
        }
        
        class RegistrationStoryboard {
            
            private init() {
                
            }
            
            private static var storyboard = UIStoryboard(name: "Registration", bundle: nil)
            
            class func loginVC() -> UIViewController {
                return storyboard.instantiateViewController(withIdentifier: "LoginViewController")
            }
            
            class func registrationVC() -> UIViewController {
                return storyboard.instantiateViewController(withIdentifier: "RegistrationViewController")
            }
            
            class func startVC() -> UIViewController {
                return storyboard.instantiateViewController(withIdentifier: "StartViewController")
            }
            
            class func initialVC() -> UIViewController {
                return storyboard.instantiateViewController(withIdentifier: "StartNavigationController")
            }
            
        }
        
        class NewMediaTutorialStoryboard {
            
            private init() {
                
            }
            
            private static var storyboard = UIStoryboard(name: "NewMediaTutorial", bundle: nil)
            
            class func mainVC() -> UIViewController {
                return storyboard.instantiateViewController(withIdentifier: "NewMediaTutorialContainerViewController")
            }
        }
        
        class FirstLaunchStoryboard {
            
            private init() {
                
            }
            
            private static var storyboard = UIStoryboard(name: "FirstLaunch", bundle: nil)
            
            class func mainVC() -> UIViewController {
                let str = storyboard.instantiateViewController(withIdentifier: "FirstLaunchContainerViewController")
                str.modalPresentationStyle = .overFullScreen
                return str
            }
            
        }
    }
    
}
