//
//  UITableViewCellExt.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 29.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

extension UITableViewCell {
    
    @objc func initialize(with object: NSObject?) {
        fatalError("Initialize should be overridden")
    }
    
    @objc func didSuperViewControllerSet(superViewController: UIViewController) {
        
    }
    
}
