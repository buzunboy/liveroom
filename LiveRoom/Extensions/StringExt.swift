//
//  StringExt.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 22.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

extension String {
    
    public func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
    
}
