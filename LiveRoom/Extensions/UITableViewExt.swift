//
//  UITableViewExt.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 29.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

extension UITableView {
    
    func dequeueReusableCell(withIdentifier: String, for indexPath: IndexPath, with object: GenericObject?, superViewController: UIViewController? = nil) -> UITableViewCell {
        let cell = self.dequeueReusableCell(withIdentifier: withIdentifier, for: indexPath)
        if let obj = object { cell.initialize(with: obj) }
        if let superVC = superViewController { cell.didSuperViewControllerSet(superViewController: superVC) }
        return cell
    }
    
    func dequeueEmptyCell() -> UITableViewCell {
        return self.dequeueReusableCell(withIdentifier: "")!
    }
    
}
