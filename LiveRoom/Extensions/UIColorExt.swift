//
//  UIColorExt.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 28.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

extension UIColor {
    
    func isBlackish() -> Bool {
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        self.getRed(&red, green: &green, blue: &blue, alpha: nil)
        return (red+green+blue >= 1.5) ? false: true
    }
    
}
