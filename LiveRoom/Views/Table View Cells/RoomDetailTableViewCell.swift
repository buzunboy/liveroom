//
//  RoomDetailTableViewCell.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 29.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class RoomDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var mainImageView: MediaImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func initialize(with object: NSObject?) {
        if let obj = object as? MediaObject {
            if obj.imageAssetId != nil {
                if let imgObj = LDBService.AssetsManager.sharedInstance.getAsset(id: obj.imageAssetId!) {
                    let img = MainService.ImageStore(folderName: "images").getImage(name: imgObj.name)
                    mainImageView.image = img
                }
            }
        }
    }
    
}
