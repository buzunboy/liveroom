//
//  RoomLastCell.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 9.08.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class RoomLastCell: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
