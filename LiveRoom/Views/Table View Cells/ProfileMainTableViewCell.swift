//
//  ProfileMainTableViewCell.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 22.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class ProfileMainTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImageView: RadialImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    private var isSeen = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.prepareUI()
        NotificationCenter.default.addObserver(self, selector: #selector(appearIcons), name: .mainTableAppearButtons, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(disappearIcons), name: .mainTableDisapperButtons, object: nil)
        self.label.text = "profile_swipeUp".localized()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func initialize(with object: NSObject?) {
        
    }
    
    func prepareUI() {
        self.usernameLabel.text = Utilities.UserInfoListHandler.sharedInstance.email ?? ""
        self.detailLabel.text = nil
        if let imageURL = Utilities.UserInfoListHandler.sharedInstance.profilePicture, imageURL != "" {
            MainService.ImageStore(folderName: "images").getImageFromStorage(imageURL: imageURL, splashImage: nil) { (loadedImage) in
                self.profileImageView.image = loadedImage
            }
        }
        
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.width / 2
        self.profileImageView.clipsToBounds = true
    }
    
    @objc func appearIcons() {
        if !isSeen {
            isSeen = true
            self.icon.alpha = 0.0
            self.label.alpha = 0.0
            self.icon.isHidden = false
            self.label.isHidden = false
            UIView.animate(withDuration: 0.5, animations: {
                self.icon.alpha = 1.0
                self.label.alpha = 1.0
            }) { (completed) in
                
            }
        }
    }
    
    @objc func disappearIcons() {
        if isSeen {
            isSeen = false
            UIView.animate(withDuration: 0.5, animations: {
                self.icon.alpha = 0.0
                self.label.alpha = 0.0
            }) { (completed) in
                if completed {
                    self.icon.isHidden = true
                    self.label.isHidden = true
                }
            }
        }
    }

}
