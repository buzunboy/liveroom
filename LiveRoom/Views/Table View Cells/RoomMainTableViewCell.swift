//
//  RoomMainTableViewCell.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 29.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class RoomMainTableViewCell: UITableViewCell {

    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var startRoomView: UIView!
    
    public var labelTintColor: UIColor = .white {
        didSet {
            self.changeLabelsColor()
        }
    }
    
    private var textField: UITextField!
    private var isEditOptionsShown = false
    private var selectedRoom: RoomObject!
    private var imagePickerService: MediaService.GenericPickerService!
    private var superViewController: UIViewController!
    private var editBtn: UIButton!
    private var startRoomVC: StartScrollerViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.prepareUI()
    }
    
    func prepareUI() {
        if self.mainLabel != nil && self.selectedRoom != nil {
            self.mainLabel.textColor = (superViewController as? BaseUIViewController)?.baseTintColor
            self.detailLabel.textColor = (superViewController as? BaseUIViewController)?.baseTintColor
            self.mainLabel.text = self.selectedRoom.name
            self.textField = UITextField(frame: self.mainLabel.frame)
            self.detailLabel.text = nil
            if (self.selectedRoom.isOwner()) {
                self.detailLabel.text = "room_detail_you".localized()
            } else {
                CloudService.Database.instance.getUsername(id: self.selectedRoom.owner) { (error, result) in
                    if let error = error {
                        LogError("Username couldn't get - Error: \(error.localizedDescription) - User Id: \(self.selectedRoom.owner)")
                        self.detailLabel.text = nil
                        return
                    }
                    self.detailLabel.text = String(format: "format_home_createdBy".localized(), result!)
                }
            }
            let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneClicked))
            let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: Utilities.instance.deviceWidth, height: 36))
            toolbar.barStyle = .default
            toolbar.setItems([doneBtn], animated: true)
            self.createChangeImageInterface()
            self.textField.borderStyle = .none
            self.textField.isHidden = true
            self.textField.inputAccessoryView = toolbar
            self.addSubview(textField)
            self.textField.font = UIFont.boldSystemFont(ofSize: 31)
            self.textField.textColor = UIColor.white
        }
    }
    
    @objc private func doneClicked() {
        self.textField.resignFirstResponder()
    }
    
    override func initialize(with object: NSObject?) {
        guard let obj = object as? RoomObject else { return }
        self.selectedRoom = obj
        NotificationCenter.default.addObserver(self, selector: #selector(appearStartRoomView), name: .mainTableAppearButtons, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(disappearStartRoomView), name: .mainTableDisapperButtons, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showEditOptions), name: .mainTableShowEditOptions, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideEditOptions), name: .mainTableHideEditOptions, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(cellWillDisappear), name: .mainTableDisappear, object: nil)
        self.prepareUI()
    }
    
    override func didSuperViewControllerSet(superViewController: UIViewController) {
        self.superViewController = superViewController
        startRoomVC = StartScrollerViewController(nibName: "StartScrollerViewController", bundle: nil)
        startRoomVC.view.frame = CGRect(x: 0, y: 0, width: self.startRoomView.frame.width, height: self.startRoomView.frame.height)
        startRoomVC.delegate = superViewController
        self.startRoomView.addSubview(startRoomVC.view)
        superViewController.addChildViewController(startRoomVC)
    }
    
    @objc private func appearStartRoomView() {
        UIView.animate(withDuration: 0.2) {
            self.startRoomView.alpha = 1
        }
    }
    
    @objc private func disappearStartRoomView() {
        UIView.animate(withDuration: 0.5) {
            self.startRoomView.alpha = 0
        }
    }
    
    @objc private func showEditOptions() {
        if !isEditOptionsShown {
            isEditOptionsShown = true
            textField.placeholder = mainLabel.text
            textField.isHidden = false
            mainLabel.isHidden = true
            self.editBtn.isHidden = false
        }
    }
    
    @objc private func hideEditOptions() {
        if isEditOptionsShown {
            isEditOptionsShown = false
            textField.isHidden = true
            mainLabel.isHidden = false
            self.textField.resignFirstResponder()
            if let text = textField.text, text.count >= 3 {
                self.selectedRoom.name = text
            }
            if LDBService.RoomsManager.sharedInstance.saveRoom(self.selectedRoom) {
                self.mainLabel.text = self.selectedRoom.name
            }
            self.editBtn.isHidden = true
        }
    }
    
    private func createChangeImageInterface() {
        editBtn = UIButton(frame: self.frame)
        editBtn.setTitle("room_clickToEdit".localized(), for: .normal)
        editBtn.setTitleColor(UIColor.white, for: .normal)
        editBtn.addTarget(self, action: #selector(openImagePicker), for: .touchUpInside)
        editBtn.isHidden = true
        self.addSubview(editBtn)
    }
    
    @objc func openImagePicker() {
        let addAct = LRAlertAction(title: "Get from library", style: .default) { (action) in
            self.addImage()
        }
        let addCameraAct = LRAlertAction(title: "Capture from camera", style: .default) { (action) in
            self.addImageFromCamera()
        }
        //self.hideEditOptions()
        ActionSheetController.actionSheet(title: "Image".localized(), actions: addAct, addCameraAct)
    }
    
    private func addImage() {
        MainService.UserPermissionService.checkForPhotoLibrary { (authorized) in
            if authorized {
                self.imagePickerService = MediaService.ImagePickerService(delegate: self.superViewController)
                self.imagePickerService.show()
            }
        }
    }
    
    private func addImageFromCamera() {
        MainService.UserPermissionService.checkForPhotoLibrary { (authorized) in
            if authorized {
                self.imagePickerService = MediaService.CameraImagePickerService(delegate: self.superViewController)
                self.imagePickerService.show()
            }
        }
    }
    
    @objc private func changeLabelsColor(notification: Notification? = nil) {
        DispatchQueue.main.async {
            self.mainLabel.textColor = self.labelTintColor
            self.detailLabel.textColor = self.labelTintColor
            self.startRoomVC.changeBaseColor(self.labelTintColor)
        }
    }
    
    @objc private func cellWillDisappear() {
        self.hideEditOptions()
    }


}
