//
//  RoomTableViewCell.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 1.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class RoomTableViewCell: UITableViewCell {

    @IBOutlet weak var backgroundImageView: MediaImageView!
    @IBOutlet weak var mainTitleLabel: UILabel!
    @IBOutlet weak var detailTitleLabel: UILabel!
    @IBOutlet weak var mediaView: MediaView!
    @IBOutlet weak var imageContainerView: UIView!
    
    private var initialPosition: CGPoint!
    
    var selectedRoom: RoomObject! {
        didSet {
            self.prepare()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundImageView.clipsToBounds = true
        self.initialPosition = self.backgroundImageView.center
        self.imageContainerView.layer.cornerRadius = 20
        self.backgroundImageView.image = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func initialize(with object: NSObject?) {
        if let obj = object as? RoomObject {
            self.selectedRoom = obj
        }
    }
    
    func prepare() {
//        let recog = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognized(gesture:)))
//        recog.delegate = self
//        self.mediaView.addGestureRecognizer(recog)
        let recog = UILongPressGestureRecognizer(target: self, action: #selector(longPressRecognized(gesture:)))
        recog.delegate = self
        recog.minimumPressDuration = 0.5
        self.mediaView.addGestureRecognizer(recog)
        self.backgroundImageView.image = nil
        self.detailTitleLabel.text = nil
        self.mainTitleLabel.text = nil
        if let room = selectedRoom {
            self.mainTitleLabel.text = room.name
            if (room.isOwner()) {
                self.detailTitleLabel.text = "room_detail_you".localized()
            } else {
                CloudService.Database.instance.getUsername(id: room.owner) { (error, result) in
                    if let error = error {
                        LogError("Username couldn't get - Error: \(error.localizedDescription) - User Id: \(self.selectedRoom.owner)")
                        self.detailTitleLabel.text = nil
                        return
                    }
                    self.detailTitleLabel.text = String(format: "format_home_createdBy".localized(), result!)
                }
            }
            MainService.RoomsService.roomImage(roomId: room.id) { (image) in
                if let image = image {
                    self.backgroundImageView.image = image
                } else {
                    self.backgroundImageView.image = UIImage(named: "default_background")
                }
            }
        }
    }
    
    @objc private func longPressRecognized(gesture: UILongPressGestureRecognizer) {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            self.mediaView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        }) { (completed) in
            
        }
        if gesture.state == .ended {
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
                self.mediaView.transform = CGAffineTransform(scaleX: 1, y: 1)
            }) { (completed) in
                
            }
        }
    }
    
    @objc func panGestureRecognized(gesture: UIPanGestureRecognizer) {
//        let isLeftDirection = (gesture.velocity(in: backgroundImageView).x < 0) ? true : false
//        if (gesture.velocity(in: self).x > 50 || gesture.velocity(in: self).x < -50) {
//            let translation = gesture.translation(in: self)
//
//            self.backgroundImageView.center = CGPoint(x: self.backgroundImageView.center.x + translation.x, y: self.backgroundImageView.center.y)
//            print(gesture.velocity(in: self))
//            if gesture.state == .ended {
//                print("ENDED ENDED ENDED")
//                UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
//                    self.backgroundImageView.center = self.initialPosition
//                }) { (completed) in }
//            }
//        }
//        gesture.setTranslation(CGPoint.zero, in: self)
    }

}
