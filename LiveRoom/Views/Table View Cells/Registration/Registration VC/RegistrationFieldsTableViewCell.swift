//
//  RegistrationFieldsTableViewCell.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 15.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class RegistrationFieldsTableViewCell: UITableViewCell {

    @IBOutlet weak var emailTF: LoginTextField!
    @IBOutlet weak var usernameTF: LoginTextField!
    @IBOutlet weak var passwordTF: LoginTextField!
    @IBOutlet weak var rePasswordTF: LoginTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func initialize(with object: NSObject?) {
        
    }
    
    

}
