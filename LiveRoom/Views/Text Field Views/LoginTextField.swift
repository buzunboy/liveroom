//
//  LoginTextField.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 15.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

@IBDesignable
class LoginTextField: UITextField {
    @IBInspectable var imageName: String = ""
    @IBInspectable var insetX: CGFloat = 0
    @IBInspectable var insetY: CGFloat = 0
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX, dy: insetY)
    }
    
    // text position
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX, dy: insetY)
    }
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        //        let image = UIImage(named: imageName)
        //        let imageView = UIImageView(frame: CGRect(x: 10, y: 15, width: 20, height: 20))
        //        imageView.image = image
        //        self.addSubview(imageView)
        // Drawing code
        self.showNormal()
    }
    
    func showError() {
        self.layer.borderColor = UIColor.red.cgColor
        self.layer.borderWidth = 2.0
    }
    
    func showPassed() {
        self.layer.borderColor = UIColor.green.cgColor
        self.layer.borderWidth = 1.0
    }
    
    func showNormal() {
        self.layer.borderColor = UIColor.darkGray.cgColor
        self.layer.borderWidth = 1.0
    }
    
    
}
