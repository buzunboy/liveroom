//
//  RadialTutorialView.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 5.08.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

@IBDesignable
class RadialTutorialView: UIView {

    @IBInspectable var viewColor: UIColor = .white { didSet { updateUI() }}
    
    override func draw(_ rect: CGRect) {
        // Drawing code
        updateUI()
    }
    
    func updateUI() {
        self.layer.cornerRadius = self.frame.width / 2
        self.backgroundColor = viewColor
        self.clipsToBounds = true
    }

}
