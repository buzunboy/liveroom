//
//  LRAlertAction.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 31.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class LRAlertAction: NSObject {
    
    typealias LRActionHandler = (_ action: UIAlertAction) -> ()
    
    public var action: UIAlertAction!
    public var completionHandler: LRActionHandler!
    public var title: String? = nil
    public var style: UIAlertActionStyle = .default
    
    init(title: String?, style: UIAlertActionStyle, completion: @escaping LRActionHandler) {
        super.init()
        self.title = title
        self.style = style
        self.completionHandler = completion
        self.action = UIAlertAction(title: title, style: style) { (actionCompleted) in
            self.completionHandler(actionCompleted)
        }
    }

}
