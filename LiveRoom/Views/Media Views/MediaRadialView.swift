//
//  MediaRadialView.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 2.07.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

@IBDesignable 
class MediaRadialView: UIView {

    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.prepare()
        }
    }
    
    override func draw(_ rect: CGRect) {
        self.prepare()
    }
    
    func prepare() {
        self.layer.cornerRadius = 20
        self.clipsToBounds = false
        self.layer.masksToBounds = true
    }

}
