//
//  CornerRadiusUIView.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 30.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

@IBDesignable
class MediaView: UIView {

    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.prepareUI()
        }
    }
    
    func prepareUI() {
        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = false
        self.backgroundColor = UIColor.clear
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 0.5
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: cornerRadius).cgPath
    }

    override func draw(_ rect: CGRect) {
        self.prepareUI()
    }


}
