//
//  AppDelegate.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 28.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import FBSDKCoreKit

var appDelegateEntity: AppDelegate!

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var orientationLock = UIInterfaceOrientationMask.all
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        LogInfo("--------------- APPLICATION STARTED ---------------")
        CloudService.AccountManager.instance.updateLastSeen()
        _ = AccountDelegate.instance
        FirebaseApp.configure()
        UIApplication.shared.isStatusBarHidden = false
        appDelegateEntity = (UIApplication.shared.delegate as! AppDelegate)
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        LogInfo("--------------- APPLICATION OPENED WITH URL ---------------")
        MainService.AppLinker(url: url, mainWindow: self.window).redirect()
        let fbHandler = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        return fbHandler
    }

    func applicationWillResignActive(_ application: UIApplication) {
        LogInfo("--------------- APPLICATION WILL RESIGN ACTIVE ---------------")
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        LogInfo("--------------- APPLICATION ENTERED BACKGROUND ---------------")
        CloudService.AccountManager.instance.updateLastSeen()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        LogInfo("--------------- APPLICATION WILL ENTER FOREGROUND ---------------")
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        LogInfo("--------------- APPLICATION BECAME ACTIVE ---------------")
    }

    func applicationWillTerminate(_ application: UIApplication) {
        LogInfo("--------------- APPLICATION WILL TERMINATE ---------------")
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return self.orientationLock
    }

    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "LocalDB")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                LogError("Unresolved error while loading Core Data - Error: \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let error = error as NSError
                LogError("Unresolved error while saving to Core Data - Error: \(error), \(error.userInfo)")
            }
        }
    }

}
