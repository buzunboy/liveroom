//
//  RoomObject.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 29.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class RoomObject: GenericObject {
    
    var name: String!
    var owner: String!
    var creationDate: Date!
    var coverImage: String?
    var shouldStoreInCloud: Bool = false {
        didSet {
            if self.shouldStoreInCloud {
                if self.saveToCloud() {
                    _ = LDBService.RoomsManager.sharedInstance.setStoreInCloud(to: self.shouldStoreInCloud, id: self.id)
                } else {
                    self.shouldStoreInCloud = false
                }
            } else {
                CloudService.Database.instance.hideRoom(self.id)
                _ = LDBService.RoomsManager.sharedInstance.setStoreInCloud(to: self.shouldStoreInCloud, id: self.id)
            }
        }
    }
    
    var medias: [MediaObject]? {
        get {
            return self.getMedias()
        }
    }
    
    init(id: String, name: String, owner: String, creationDate: Date, coverImage: String?, storeInCloud: Bool = false) {
        self.name = name
        self.owner = owner
        self.creationDate = creationDate
        self.coverImage = coverImage
        self.shouldStoreInCloud = storeInCloud
        super.init(id: id, type: .room)
    }
    
    private func getMedias() -> [MediaObject]? {
        return LDBService.MediaManager.sharedInstance.getMedias(forRoomId: id)
    }
    
    public func dictionary() -> [String:Any] {
        return [
            "id": id,
            "name": self.name,
            "owner": self.owner,
            "creationDate": self.creationDate,
            "coverImage": self.coverImage as Any,
            "storeInCloud": self.shouldStoreInCloud
        ]
    }
    
    public func isOwner() -> Bool {
        if self.owner == Utilities.UserInfoListHandler.sharedInstance.id {
            return true
        } else {
            return false
        }
    }
    
    public func getShareLink() -> String? {
        let loginStatus = CloudService.AccountManager.instance.loginStatus
        if loginStatus == .loggedIn {
            return "liveRoomApp://rooms/\(self.id!)"
        } else {
            return nil
        }
    }
    
    public func saveToCloud() -> Bool {
        let loginStatus = CloudService.AccountManager.instance.loginStatus
        if loginStatus == .loggedIn {
            CloudService.Database.instance.saveRoom(self)
            return true
        } else if loginStatus == .anonym {
            ActionSheetController.anonymAlert()
        }
        return false
    }
    
}
