//
//  MediaObject.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 30.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class MediaObject: GenericObject {
    
    var videoAssetId: String?
    var imageAssetId: String?
    var roomId: String!
    
    init(id: String, roomId: String, videoAssetId: String?, imageAssetId: String?) {
        self.imageAssetId = imageAssetId
        self.roomId = roomId
        self.videoAssetId = videoAssetId
        super.init(id: id, type: .media)
    }
    
    public func dictionary() -> [String:Any] {
        return [
            "id": id,
            "roomId": self.roomId,
            "videoId": self.videoAssetId as Any,
            "imageId": self.imageAssetId as Any
        ]
    }
    
}
