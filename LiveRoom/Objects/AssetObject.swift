//
//  AssetObject.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 29.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class AssetObject: GenericObject {
    
    var name: String!
    var location: String!
    var assetType: AssetType
    var creationDate: Date!
    var roomId: String!
    var mediaId: String!
    
    init(id: String, mediaId: String, name: String, location: String, roomId: String, assetType: AssetType, creationDate: Date) {
        self.name = name
        self.mediaId = mediaId
        self.location = location
        self.roomId = roomId
        self.assetType = assetType
        self.creationDate = creationDate
        super.init(id: id, type: .asset)
    }
    
    func convertToVideoAsset() -> VideoAsset? {
        if self.assetType == .video {
            return VideoAsset(id: id, mediaId: self.mediaId, name: self.name, location: self.location, roomId: self.roomId, creationDate: self.creationDate)
        } else {
            return nil
        }
    }
    
    func convertToImageAsset() -> ImageAsset? {
        if self.assetType == .video {
            return ImageAsset(id: id, mediaId: self.mediaId, name: self.name, location: self.location, roomId: self.roomId, creationDate: self.creationDate)
        } else {
            return nil
        }
    }
    
}
