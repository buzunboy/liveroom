//
//  VideoAsset.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 29.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

class VideoAsset: AssetObject {
    
    init(id: String, mediaId: String, name: String, location: String, roomId: String, creationDate: Date) {
        super.init(id: id, mediaId: mediaId, name: name, location: location, roomId: roomId, assetType: .video, creationDate: creationDate)
    }
    
    public func dictionary() -> [String:Any] {
        return [
            "id": id,
            "mediaId": mediaId,
            "name": name,
            "location": location,
            "roomId": roomId,
            "creationDate": creationDate
        ]
    }

}
