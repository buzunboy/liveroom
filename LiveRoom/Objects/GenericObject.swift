//
//  GenericObject.swift
//  LiveRoom
//
//  Created by Burak Uzunboy on 29.06.2018.
//  Copyright © 2018 buzunboy. All rights reserved.
//

import UIKit

protocol GenericObjectProtocol {
    var id: String! { get set }
    var type: ObjectType { get set }
}

class GenericObject: NSObject, GenericObjectProtocol {
    var id: String!
    var type: ObjectType
    
    init(id: String, type: ObjectType) {
        self.id = id
        self.type = type
    }
    
}
